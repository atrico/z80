package z80

type WordRef interface {
	Ref[Word]
	AsAddress() AddressRef
}

func NewWordRef(wd *Word) WordRef {
	return wordRef{wd}
}
func NewWordRef8(lo, hi *Byte) WordRef {
	return wordRef8{lo, hi}
}
func NewWordRefMem(mem Memory, addr Address) WordRef {
	return wordRefMem{mem, addr}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Word
// ----------------------------------------------------------------------------------------------------------------------------

type wordRef struct {
	wd *Word
}

func (r wordRef) Get() Word {
	return *r.wd
}

func (r wordRef) Set(val Word) {
	*r.wd = val
}

func (r wordRef) AsAddress() AddressRef {
	return addressRef{r}
}

func (r wordRef) String() string {
	return r.Get().String()
}

// ----------------------------------------------------------------------------------------------------------------------------
// From 2 bytes
// ----------------------------------------------------------------------------------------------------------------------------

type wordRef8 struct {
	lo *Byte
	hi *Byte
}

func (r wordRef8) Get() Word {
	return Word(int(*r.lo) + 0x100*int(*r.hi))
}

func (r wordRef8) Set(val Word) {
	*r.lo = val.Lo()
	*r.hi = val.Hi()
}

func (r wordRef8) AsAddress() AddressRef {
	return addressRef{r}
}

func (r wordRef8) String() string {
	return r.Get().String()
}

// ----------------------------------------------------------------------------------------------------------------------------
// From memory
// ----------------------------------------------------------------------------------------------------------------------------

type wordRefMem struct {
	mem  Memory
	addr Address
}

func (r wordRefMem) Get() Word {
	return r.mem.ReadWord(r.addr)
}

func (r wordRefMem) Set(val Word) {
	r.mem.WriteWord(r.addr, val)
}

func (r wordRefMem) AsAddress() AddressRef {
	return addressRef{r}
}

func (r wordRefMem) String() string {
	return r.Get().String()
}
