package z80

import (
	"bytes"
	"fmt"
	"github.com/spf13/viper"
	"io"
	"path/filepath"
	"strconv"
	"strings"
)

type MemoryAnnotation struct {
	NewSection      bool
	Label           string
	Header          []string
	Comment         string
	CommentContinue []string
	Data            int
	DataFormat      string
	NumberOffset    int
	NumberFormat    NumberFormatType
	DELETE          bool
	NONEWSECTION    bool
}

type NumberFormatType string

const (
	NumberFormat_None    NumberFormatType = ""
	NumberFormat_Char    NumberFormatType = "char"
	NumberFormat_Mask    NumberFormatType = "mask"
	NumberFormat_Address NumberFormatType = "address"
	NumberFormat_Signed  NumberFormatType = "signed"
	NumberFormat_Delete  NumberFormatType = DeleteAnnotationString
)
const DeleteAnnotationString = "-*-"
const SquashAnnotationString = "-+-"

type MemoryAnnotations interface {
	// Lookup an address and get the details
	Lookup(addr Address) (annotation MemoryAnnotation, ok bool)
	// Update an annotation with these details
	Update(addr Address, annotation MemoryAnnotation)
	// Add annotations
	Add(annotations MemoryAnnotations)
	// Add a config
	AddConfig(config AnnotationsLoader) (err error)
	// Write annotations to yaml
	Write(writer io.Writer) (err error)
	// Return annotations as map (private)
	AsMap() map[Address]MemoryAnnotation
}

type AnnotationsLoader interface {
	Load() (config *viper.Viper, err error)
}

func ByteLoader(bytes []byte, fileType string) AnnotationsLoader {
	return byteLoader{bytes, fileType}
}
func FileLoader(filePath string, fullPath bool) AnnotationsLoader {
	return fileLoader{filePath, fullPath}
}

// Configs must be yaml
func NewMemoryAnnotations(configs ...AnnotationsLoader) (annotations MemoryAnnotations, err error) {
	ma := memoryAnnotations{make(map[Address]MemoryAnnotation)}
	for _, config := range configs {
		if err = ma.AddConfig(config); err != nil {
			break
		}
	}
	return ma, err
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type memoryAnnotations struct {
	mp map[Address]MemoryAnnotation
}

func (m memoryAnnotations) Lookup(addr Address) (annotation MemoryAnnotation, ok bool) {
	annotation, ok = m.mp[addr]
	return
}

func (m memoryAnnotations) Update(addr Address, annotation MemoryAnnotation) {
	// Update non default fields
	m.mergeAnnotation(addr, annotation)
}

func (m memoryAnnotations) Add(annotations MemoryAnnotations) {
	for addr, detail := range annotations.AsMap() {
		m.mp[addr] = detail
	}
}

func (m memoryAnnotations) AddConfig(loader AnnotationsLoader) (err error) {
	var config *viper.Viper
	if config, err = loader.Load(); err == nil {
		err = m.mergeAll(config)
	}
	return
}

func (m memoryAnnotations) Write(writer io.Writer) (err error) {
	// Write in address order
	for addr := 0; err == nil && addr < 0x10000; addr++ {
		if details, ok := m.Lookup(Address(addr)); ok && !details.DELETE {
			if _, err = writer.Write([]byte(fmt.Sprintf("%04x:\n", addr))); err == nil {
				if details.NewSection {
					_, err = writer.Write([]byte("  newSection: true\n"))
				}
				if err == nil && len(details.Header) > 0 {
					_, err = writer.Write([]byte("  header:\n"))
					for _, line := range details.Header {
						_, err = writer.Write([]byte(fmt.Sprintf("    - '%s'\n", cleanYamlString(line))))
					}
				}
				if err == nil && details.Label != "" {
					_, err = writer.Write([]byte(fmt.Sprintf("  label: '%s'\n", details.Label)))
				}
				if err == nil && details.Comment != "" {
					_, err = writer.Write([]byte(fmt.Sprintf("  comment: '%s'\n", cleanYamlString(details.Comment))))
				}
				if err == nil && len(details.CommentContinue) > 0 {
					_, err = writer.Write([]byte("  commentContinue:\n"))
					for _, line := range details.CommentContinue {
						_, err = writer.Write([]byte(fmt.Sprintf("    - '%s'\n", cleanYamlString(line))))
					}
				}
				if err == nil && details.Data > 0 {
					_, err = writer.Write([]byte(fmt.Sprintf("  data: %d\n", details.Data)))
				}
				if err == nil && details.DataFormat != "" {
					_, err = writer.Write([]byte(fmt.Sprintf("  dataFormat: '%s'\n", details.DataFormat)))
				}
				if err == nil && details.NumberFormat != NumberFormat_None {
					_, err = writer.Write([]byte(fmt.Sprintf("  numberFormat: '%s'\n", details.NumberFormat)))
				}
				if err == nil && details.NumberOffset != 0 {
					_, err = writer.Write([]byte(fmt.Sprintf("  numberOffset: %d\n", details.NumberOffset)))
				}
			}
		}
	}
	return
}

func cleanYamlString(text string) string {
	// Replace single quote with two
	return strings.Replace(text, "'", "''", -1)
}

func (m memoryAnnotations) AsMap() map[Address]MemoryAnnotation {
	return m.mp
}

// ----------------------------------------------------------------------------------------------------------------------------
// Byte loader
// ----------------------------------------------------------------------------------------------------------------------------

type byteLoader struct {
	bytes    []byte
	fileType string
}

func (b byteLoader) Load() (config *viper.Viper, err error) {
	config = viper.New()
	config.SetConfigType(b.fileType)
	err = config.ReadConfig(bytes.NewBuffer(b.bytes))
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// File loader
// ----------------------------------------------------------------------------------------------------------------------------

type fileLoader struct {
	string
	fullPath bool
}

func (f fileLoader) Load() (config *viper.Viper, err error) {
	config = viper.New()
	if f.fullPath {
		config.SetConfigFile(f.string)
	} else {
		config.SetConfigName(filepath.Base(f.string))
		config.AddConfigPath(filepath.Dir(f.string))
	}
	err = config.ReadInConfig()
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Merge
// ----------------------------------------------------------------------------------------------------------------------------

func (m memoryAnnotations) mergeAll(rhs *viper.Viper) (err error) {
	var val uint64
	for addr := range rhs.AllSettings() {
		if val, err = strconv.ParseUint(addr, 16, 16); err != nil {
			return
		}
		addrDetail := MemoryAnnotation{}
		if err = rhs.UnmarshalKey(addr, &addrDetail); err != nil {
			return
		}
		m.mergeAnnotation(Address(val), addrDetail)
	}
	return
}

// Merge config into existing
// Strings == "" are ignored, == "-*-" delete existing
// Ints == 0 are ignored, == -1 set existing to 0
// String lists are appended unless set to "-*-", then removed
func (m memoryAnnotations) mergeAnnotation(address Address, rhs MemoryAnnotation) {
	if rhs.DELETE {
		delete(m.mp, address)
	} else {
		detail := m.mp[address]
		if rhs.NONEWSECTION {
			detail.NewSection = false
		} else {
			detail.NewSection = detail.NewSection || rhs.NewSection
		}
		mergeString(rhs.Label, &detail.Label)
		mergeStringList(rhs.Header, &detail.Header)
		if rhs.Comment == SquashAnnotationString {
			// Squash all comments into one line
			lines := make([]string, 0, len(detail.CommentContinue)+1)
			if detail.Comment != "" {
				lines = append(lines, detail.Comment)
			}
			for _, line := range detail.CommentContinue {
				if line != "" {
					lines = append(lines, line)
				}
			}
			detail.Comment = strings.Join(lines, " ")
			detail.CommentContinue = nil
		} else {
			mergeString(rhs.Comment, &detail.Comment)
		}
		mergeStringList(rhs.CommentContinue, &detail.CommentContinue)
		mergeInt(rhs.Data, &detail.Data)
		mergeString(rhs.DataFormat, &detail.DataFormat)
		mergeString(string(rhs.NumberFormat), (*string)(&detail.NumberFormat))
		if rhs.NumberOffset != 0 {
			detail.NumberOffset = rhs.NumberOffset
		}
		m.mp[address] = detail
	}
}

func mergeString(val string, property *string) {
	if val != "" {
		if val == DeleteAnnotationString {
			val = ""
		}
		*property = val
	}
}

func mergeInt(val int, property *int) {
	if val != 0 {
		if val == -1 {
			val = 0
		}
		*property = val
	}
}

func mergeStringList(val []string, property *[]string) {
	if len(val) > 0 {
		if val[0] == DeleteAnnotationString {
			*property = val[1:]
		} else {
			*property = append(*property, val...)
		}
	}
}
