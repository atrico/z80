package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/disassembler/symbols"
	"os"
	"slices"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
	"gitlab.com/atrico/testing/v2/random"
	"go.uber.org/goleak"
)

var rg = random.NewValueGenerator()

func TestMain(m *testing.M) {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	goleak.VerifyTestMain(m)
}

func randomByte(excluding ...z80.Byte) (ret z80.Byte) {
	ret = z80.Byte(rg.Byte())
	for slices.Contains(excluding, ret) {
		ret = z80.Byte(rg.Byte())
	}
	return
}
func randomWord(excluding ...z80.Word) (ret z80.Word) {
	ret = z80.Word(rg.IntUpto(0x10000))
	for slices.Contains(excluding, ret) {
		ret = z80.Word(rg.IntUpto(0x10000))
	}
	return
}
func randomAddress(excluding ...z80.Address) (ret z80.Address) {
	ret = z80.Address(randomWord())
	for slices.Contains(excluding, ret) {
		ret = z80.Address(randomWord())
	}
	return
}

func createRandomMemory(romSize int) (mem z80.Memory, by []z80.Byte) {
	by = createRandomBytes(0x10000)
	var err error
	if mem, err = z80.NewMemory(by[:romSize], by[romSize:]); err != nil {
		panic(err)
	}

	return
}

func createRandomBytes(count int) (by []z80.Byte) {
	by = make([]z80.Byte, count)
	for i := 0; i < count; i++ {
		by[i] = randomByte()
	}
	return
}

func createRandomWords(count int) (by []z80.Word) {
	by = make([]z80.Word, count)
	for i := 0; i < count; i++ {
		by[i] = randomWord()
	}
	return
}

// Create a memory reader with the specified bytes at location 0
func createMemoryReader(bytes []z80.Byte) z80.MemoryReaderCount {
	mem, _ := z80.NewMemory([]z80.Byte{}, make([]z80.Byte, 0x10000))
	const start = z80.Address(0)
	mem.WriteBytes(start, bytes)
	return mem.ReaderCount(start, start.Offset(len(bytes)))
}

func createDisassembler(bytes []z80.Byte, annot ...func(annot z80.MemoryAnnotations)) (dis disassembler.Disassembler, disWAA disassembler.Disassembler) {
	dataScanners := func(name string) (impl disassembler.DisassemblyScannerImpl, ok bool) {
		var fact disassembler.DisassemblyScannerImplFactory
		if fact, ok = disassembler.Z80DataScanners[name]; ok {
			impl = fact()
		}
		return
	}
	{
		reader := createMemoryReader(bytes)
		for _, f := range annot {
			f(reader.Annotations())
		}
		dis = disassembler.NewDisassembler(reader, dataScanners, nil, MockFormatter{})
	}
	{
		reader := createMemoryReader(bytes)
		for _, f := range annot {
			f(reader.Annotations())
		}
		reader.Annotations().Update(z80.Address(0), z80.MemoryAnnotation{NumberFormat: z80.NumberFormat_Address})
		disWAA = disassembler.NewDisassembler(reader, dataScanners, nil, MockFormatter{})
	}
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Mock Formatter
// ----------------------------------------------------------------------------------------------------------------------------

type MockFormatter struct{}

func (m MockFormatter) Char(val z80.Byte, configurators ...format.Configurator) string {
	//TODO implement me
	panic("implement me")
}

func (m MockFormatter) Byte(val z80.Byte, configurators ...format.Configurator) string {
	return val.String()
}

func (m MockFormatter) SymbolTable() symbols.SymbolTableBuilder {
	return MockSymbolTable{}
}

func (m MockFormatter) Highlighter() format.SyntaxHighlighter {
	return MockHighlighter{}
}

func (m MockFormatter) Word(word z80.Word, configurators ...format.Configurator) string {
	return word.String()
}
func (m MockFormatter) Address(addr z80.Address, configurators ...format.Configurator) string {
	return addr.String()
}
func (m MockFormatter) AddressRef(addr z80.Address, configurators ...format.Configurator) string {
	return fmt.Sprintf("Label_%s", addr)
}
func (m MockFormatter) AddressRefWithOffset(val z80.Address, offset int, configurators ...format.Configurator) string {
	if offset != 0 {
		//TODO implement me
		panic("implement me")
	}
	return m.AddressRef(val, configurators...)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Mock symbol table
// ----------------------------------------------------------------------------------------------------------------------------
type MockSymbolTable struct {
}

func (m MockSymbolTable) Add(addr z80.Address) (label string) {
	return ""
}

func (m MockSymbolTable) AddUsed(addr z80.Address, used z80.Address) (label string) {
	return ""

}

func (m MockSymbolTable) AddAnnotations(annotations z80.MemoryAnnotations) {

}

func (m MockSymbolTable) AddConfig(config z80.AnnotationsLoader) (err error) {
	//TODO implement me
	panic("implement me")
}

func (m MockSymbolTable) Lookup(addr z80.Address) (entry symbols.SymbolTableEntry, ok bool) {
	entry.Label = fmt.Sprintf("Label_%s", addr)
	ok = true
	return
}

func (m MockSymbolTable) Map() map[z80.Address]symbols.SymbolTableEntry {
	return make(map[z80.Address]symbols.SymbolTableEntry)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Mock highlighter
// ----------------------------------------------------------------------------------------------------------------------------
type MockHighlighter struct{}

func (m MockHighlighter) Char(original string) string {
	return original
}

func (m MockHighlighter) Error(original string) string {
	return original
}

func (m MockHighlighter) Address(original string) string {
	return original
}

func (m MockHighlighter) Word(original string) string {
	return original
}

func (m MockHighlighter) Byte(original string) string {
	return original
}

func (m MockHighlighter) String(original string) string {
	return original
}

func (m MockHighlighter) Number(original string) string {
	return original
}

func (m MockHighlighter) Variable(original string) string {
	return original
}

func (m MockHighlighter) Keyword(original string) string {
	return original
}

func (m MockHighlighter) Comment(original string) string {
	return original
}

func (m MockHighlighter) Condition(original string) string {
	return original
}

func (m MockHighlighter) Label(original string) string {
	return original
}

func (m MockHighlighter) Data(original string) string {
	return original
}

func (m MockHighlighter) CustomN(original string, num int) string {
	return original
}

func (m MockHighlighter) Custom(original string, formatter format.HighlightFunc) string {
	return original
}
