package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/interrupt"
	"gitlab.com/atrico/z80/snapshot"
	"testing"
)

// ----------------------------------------------------------------------------------------------------------------------------
// Memory
// ----------------------------------------------------------------------------------------------------------------------------

func Test_Snapshot_ReadByte(t *testing.T) {
	// Arrange
	snap, mem, _, _, _, _, _, _, _, _ := createRandomSnapshot()

	// Act & Assert
	for a := 0; a < 0x10000; a++ {
		addr := z80.Address(a)
		assert.That(t, snap.ReadByte(addr), is.EqualTo(mem.ReadByte(addr)), addr.String())
	}
}

func Test_Snapshot_ReadWord(t *testing.T) {
	// Arrange
	snap, mem, _, _, _, _, _, _, _, _ := createRandomSnapshot()

	// Act & Assert
	for a := 0; a < 0x10000; a += 2 {
		addr := z80.Address(a)
		assert.That(t, snap.ReadWord(addr), is.EqualTo(mem.ReadWord(addr)), addr.String())
	}
}

func Test_Snapshot_ReadBytes(t *testing.T) {
	// Arrange
	snap, mem, _, _, _, _, _, _, _, _ := createRandomSnapshot()
	start := z80.Address(rom - (rom / 2))
	end := z80.Address(rom + (rom / 2))

	// Act
	result := snap.ReadBytes(start, end)
	expected := mem.ReadBytes(start, end)

	// Assert
	assert.That(t, result, is.EqualTo(expected), "Bytes")
}

func Test_Snapshot_WriteByte(t *testing.T) {
	// Arrange
	snap, mem, _, _, _, _, _, _, _, _ := createRandomSnapshot()
	newBy := createRandomBytes(0x10000)

	// Act
	for i := range newBy {
		snap.WriteByte(z80.Address(i), newBy[i])
	}

	// Assert
	for a := 0; a < 0x10000; a++ {
		addr := z80.Address(a)
		exp := mem.ReadByte(addr)
		if a >= rom {
			exp = newBy[addr]
		}
		assert.That(t, snap.ReadByte(addr), is.EqualTo(exp), addr.String())
	}
}

func Test_Snapshot_WriteWord(t *testing.T) {
	// Arrange
	snap, mem, _, _, _, _, _, _, _, _ := createRandomSnapshot()
	newWd := createRandomWords(0x8000)

	// Act
	for i := range newWd {
		snap.WriteWord(z80.Address(i*2), newWd[i])
	}

	// Assert
	for a := 0; a < 0x10000; a += 2 {
		addr := z80.Address(a)
		exp := mem.ReadWord(addr)
		if a >= rom {
			exp = newWd[a/2]
		}
		assert.That(t, snap.ReadWord(addr), is.EqualTo(exp), addr.String())
	}
}

func Test_Snapshot_CloneMemory(t *testing.T) {
	// Arrange
	snap, _, _, _, _, _, _, _, _, _ := createRandomSnapshot()
	newBy := createRandomBytes(0x10000)
	origBytes := snap.ReadBytes(0, 0)

	// Act
	snap2 := snap.Clone()
	for i := range newBy {
		snap2.WriteByte(z80.Address(i), newBy[i])
	}

	// Assert
	for a := 0; a < 0x10000; a++ {
		addr := z80.Address(a)
		exp := origBytes[addr]
		if a >= rom {
			exp = newBy[addr]
		}
		assert.That(t, snap.ReadByte(addr), is.EqualTo(origBytes[addr]), fmt.Sprintf("Snap1: %s (no change)", addr))
		assert.That(t, snap2.ReadByte(addr), is.EqualTo(exp), fmt.Sprintf("Snap2: %s", addr))
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Registers
// ----------------------------------------------------------------------------------------------------------------------------

func Test_Snapshot_PcSp(t *testing.T) {
	// Arrange
	snap, _, pc, sp, _, _, _, _, _, _ := createRandomSnapshot()
	newPC := randomAddress(pc)
	newSP := randomAddress(sp)

	// Act
	snap2 := snap.Clone()
	snap2.ProgramCounter().Set(newPC)
	snap2.StackPointer().Set(newSP)

	// Assert
	assert.That(t, snap.ProgramCounter().Get(), is.EqualTo(pc), "Read PC")
	assert.That(t, snap.StackPointer().Get(), is.EqualTo(sp), "Read SP")
	assert.That(t, snap2.ProgramCounter().Get(), is.EqualTo(newPC), "Write PC")
	assert.That(t, snap2.StackPointer().Get(), is.EqualTo(newSP), "Write SP")
}

func Test_Snapshot_Interrupts(t *testing.T) {
	// Arrange
	snap, _, _, _, _, _, _, _, intMode, intEnabled := createRandomSnapshot()
	newIntMode := (intMode + 1) % 3
	newIntEnabled := !intEnabled

	// Act
	snap2 := snap.Clone()
	snap2.InterruptMode().Set(newIntMode)
	snap2.InterruptsEnabled().Set(newIntEnabled)

	// Assert
	assert.That(t, snap.InterruptMode().Get(), is.EqualTo(intMode), "Read Mode")
	assert.That(t, snap.InterruptsEnabled().Get(), is.EqualTo(intEnabled), "Read Enabled")
	assert.That(t, snap2.InterruptMode().Get(), is.EqualTo(newIntMode), "Write Mode")
	assert.That(t, snap2.InterruptsEnabled().Get(), is.EqualTo(newIntEnabled), "Write Enabled")
}

func Test_Snapshot_Registers16(t *testing.T) {
	for name, testCase := range registers16TestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase registers16TestCase) {
				// Arrange
				snap, _, _, _, reg, _, _, _, _, _ := createRandomSnapshot()
				ref := core.Must1(snap.Registers16Bit().Get, name)
				newVal := randomWord(core.Must1(snap.Registers16Bit().Get, name).Get())

				// Act
				snap2 := snap.Clone()
				ref2 := core.Must1(snap2.Registers16Bit().Get, name)
				ref2.Set(newVal)

				// Assert
				exp := testCase.getVal(reg)
				assert.That(t, ref.Get(), is.EqualTo(exp), "Read")
				assert.That(t, ref2.Get(), is.EqualTo(newVal), "Write")
			}(testCase)
		})
	}
}

func Test_Snapshot_AltRegisters16(t *testing.T) {
	for name, testCase := range registers16TestCases {
		altName := name + "'"
		t.Run(altName, func(t *testing.T) {
			func(testCase registers16TestCase) {
				// Arrange
				snap, _, _, _, _, altReg, _, _, _, _ := createRandomSnapshot()
				ref := core.Must1(snap.Registers16Bit().Get, altName)
				newVal := randomWord(ref.Get())

				// Act
				snap2 := snap.Clone()
				ref2 := core.Must1(snap2.Registers16Bit().Get, altName)
				ref2.Set(newVal)

				// Assert
				exp := testCase.getVal(altReg)
				assert.That(t, ref.Get(), is.EqualTo(exp), "Read")
				assert.That(t, ref2.Get(), is.EqualTo(newVal), "Write")
			}(testCase)
		})
	}
}

type registers16TestCase struct {
	getVal func(registers z80.GeneralRegisters) z80.Word
}

var registers16TestCases = map[string]registers16TestCase{
	"HL": {func(reg z80.GeneralRegisters) z80.Word { return reg.HL }},
	"DE": {func(reg z80.GeneralRegisters) z80.Word { return reg.DE }},
	"BC": {func(reg z80.GeneralRegisters) z80.Word { return reg.BC }},
	"AF": {func(reg z80.GeneralRegisters) z80.Word { return reg.AF }},
}

func Test_Snapshot_IndexRegisters(t *testing.T) {
	for name, testCase := range indexRegTestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase indexRegTestCase) {
				// Arrange
				snap, _, _, _, _, _, indReg, _, _, _ := createRandomSnapshot()
				ref := core.Must1(snap.Registers16Bit().Get, name)
				newVal := randomWord(ref.Get())

				// Act
				snap2 := snap.Clone()
				ref2 := core.Must1(snap2.Registers16Bit().Get, name)
				ref2.Set(newVal)

				// Assert
				exp := testCase.getVal(indReg)
				assert.That(t, ref.Get(), is.EqualTo(exp), "Read")
				assert.That(t, ref2.Get(), is.EqualTo(newVal), "Write")
			}(testCase)
		})
	}
}

type indexRegTestCase struct {
	getVal func(registers z80.IndexRegisters) z80.Word
}

var indexRegTestCases = map[string]indexRegTestCase{
	"IX": {func(reg z80.IndexRegisters) z80.Word { return reg.IX }},
	"IY": {func(reg z80.IndexRegisters) z80.Word { return reg.IY }},
}

func Test_Snapshot_Registers8(t *testing.T) {
	for name, testCase := range registers8TestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase registers8TestCase) {
				// Arrange
				snap, _, _, _, reg, _, _, _, _, _ := createRandomSnapshot()
				ref := core.Must1(snap.Registers8Bit().Get, name)
				newVal := randomByte(ref.Get())

				// Act
				snap2 := snap.Clone()
				ref2 := core.Must1(snap2.Registers8Bit().Get, name)
				ref2.Set(newVal)

				// Assert
				exp := testCase.getVal(reg)
				assert.That(t, ref.Get(), is.EqualTo(exp), "Read")
				assert.That(t, ref2.Get(), is.EqualTo(newVal), "Write")
			}(testCase)
		})
	}
}

func Test_Snapshot_AltRegisters8(t *testing.T) {
	for name, testCase := range registers8TestCases {
		altName := name + "'"
		t.Run(altName, func(t *testing.T) {
			func(testCase registers8TestCase) {
				// Arrange
				snap, _, _, _, _, altReg, _, _, _, _ := createRandomSnapshot()
				ref := core.Must1(snap.Registers8Bit().Get, altName)
				newVal := randomByte(ref.Get())

				// Act
				snap2 := snap.Clone()
				ref2 := core.Must1(snap2.Registers8Bit().Get, altName)
				ref2.Set(newVal)

				// Assert
				exp := testCase.getVal(altReg)
				assert.That(t, ref.Get(), is.EqualTo(exp), "Read")
				assert.That(t, ref2.Get(), is.EqualTo(newVal), "Write")
			}(testCase)
		})
	}
}

type registers8TestCase struct {
	getVal func(registers z80.GeneralRegisters) z80.Byte
}

var registers8TestCases = map[string]registers8TestCase{
	"H": {func(reg z80.GeneralRegisters) z80.Byte { return reg.HL.Hi() }},
	"L": {func(reg z80.GeneralRegisters) z80.Byte { return reg.HL.Lo() }},
	"D": {func(reg z80.GeneralRegisters) z80.Byte { return reg.DE.Hi() }},
	"E": {func(reg z80.GeneralRegisters) z80.Byte { return reg.DE.Lo() }},
	"B": {func(reg z80.GeneralRegisters) z80.Byte { return reg.BC.Hi() }},
	"C": {func(reg z80.GeneralRegisters) z80.Byte { return reg.BC.Lo() }},
	"A": {func(reg z80.GeneralRegisters) z80.Byte { return reg.AF.Hi() }},
	"F": {func(reg z80.GeneralRegisters) z80.Byte { return reg.AF.Lo() }},
}

func Test_Snapshot_SpecialRegisters(t *testing.T) {
	for name, testCase := range specialRegTestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase specialRegTestCase) {
				// Arrange
				snap, _, _, _, _, _, _, spcReg, _, _ := createRandomSnapshot()
				ref := core.Must1(snap.Registers8Bit().Get, name)
				newVal := randomByte(ref.Get())

				// Act
				snap2 := snap.Clone()
				ref2 := core.Must1(snap2.Registers8Bit().Get, name)
				ref2.Set(newVal)

				// Assert
				exp := testCase.getVal(spcReg)
				assert.That(t, ref.Get(), is.EqualTo(exp), "Read")
				assert.That(t, ref2.Get(), is.EqualTo(newVal), "Write")
			}(testCase)
		})
	}
}

type specialRegTestCase struct {
	getVal func(registers z80.SpecialRegisters) z80.Byte
}

var specialRegTestCases = map[string]specialRegTestCase{
	"I": {func(reg z80.SpecialRegisters) z80.Byte { return reg.I }},
	"R": {func(reg z80.SpecialRegisters) z80.Byte { return reg.R }},
}

// TODO - test read hi/lo = 16bit
// TODO - test write hi/lo = 16bit

func createRandomSnapshot() (snap snapshot.Snapshot, mem z80.Memory, pc z80.Address, sp z80.Address, registers z80.GeneralRegisters, altRegisters z80.GeneralRegisters, indexRegisters z80.IndexRegisters, specialRegisters z80.SpecialRegisters, interruptMode interrupt.Mode, interruptsEnabled bool) {
	mem, _ = createRandomMemory(0x1000)
	pc = randomAddress()
	sp = randomAddress()
	registers = createRandomRegisters()
	altRegisters = createRandomRegisters()
	indexRegisters = createRandomIndexRegisters()
	specialRegisters = createRandomSpecialRegisters()
	interruptMode = createRandomInterruptMode()
	interruptsEnabled = rg.Bool()
	snap = snapshot.NewSnapshot(mem, pc, sp, registers, altRegisters, indexRegisters, specialRegisters, interruptMode, interruptsEnabled)
	return
}

func createRandomRegisters() z80.GeneralRegisters {
	return z80.GeneralRegisters{
		HL: randomWord(),
		DE: randomWord(),
		BC: randomWord(),
		AF: randomWord(),
	}
}

func createRandomIndexRegisters() z80.IndexRegisters {
	return z80.IndexRegisters{
		IX: randomWord(),
		IY: randomWord(),
	}
}

func createRandomSpecialRegisters() z80.SpecialRegisters {
	return z80.SpecialRegisters{
		I: randomByte(),
		R: randomByte(),
	}
}

func createRandomInterruptMode() interrupt.Mode {
	return interrupt.Mode(rg.IntUpto(3))
}
