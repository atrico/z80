package registers

type Reg8 byte
type Reg16 int

const (
	B   Reg8 = iota
	C   Reg8 = iota
	D   Reg8 = iota
	E   Reg8 = iota
	H   Reg8 = iota
	L   Reg8 = iota
	A   Reg8 = iota
	F   Reg8 = iota
	IXh Reg8 = iota
	IXl Reg8 = iota
	IYh Reg8 = iota
	IYl Reg8 = iota
	I   Reg8 = iota
	R   Reg8 = iota
)

const (
	BC  Reg16 = iota
	DE  Reg16 = iota
	HL  Reg16 = iota
	AF  Reg16 = iota
	BCp Reg16 = iota
	DEp Reg16 = iota
	HLp Reg16 = iota
	AFp Reg16 = iota
	IX  Reg16 = iota
	IY  Reg16 = iota
	SP  Reg16 = iota
)
