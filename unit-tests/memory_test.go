package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"slices"
	"testing"
)

const rom int = 0x1000

func Test_Memory_ReadByte(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)

	// Act & Assert
	for a := 0; a < 0x10000; a++ {
		addr := z80.Address(a)
		assert.That(t, mem.ReadByte(addr), is.EqualTo(by[addr]), addr.String())
	}
}

func Test_Memory_ReadWord(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)

	// Act & Assert
	for a := 0; a < 0x10000; a += 2 {
		addr := z80.Address(a)
		assert.That(t, mem.ReadWord(addr), is.EqualTo(z80.Word(int(by[addr])+0x100*int(by[addr+1]))), addr.String())
	}
}

func Test_Memory_ReadWordBigEndian(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)

	// Act & Assert
	for a := 0; a < 0x10000; a += 2 {
		addr := z80.Address(a)
		assert.That(t, mem.ReadWordBigEndian(addr), is.EqualTo(z80.Word(0x100*int(by[addr])+int(by[addr+1]))), addr.String())
	}
}

func Test_Memory_ReadBytes(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	start := z80.Address(rom - (rom / 2))
	end := z80.Address(rom + (rom / 2))

	// Act
	result := mem.ReadBytes(start, end)

	// Assert
	assert.That(t, result, is.EqualTo(by[start:end]), "Bytes")
}

func Test_Memory_WriteByte(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	newBy := createRandomBytes(0x10000)

	// Act
	for i := range newBy {
		mem.WriteByte(z80.Address(i), newBy[i])
	}

	// Assert
	for a := 0; a < 0x10000; a++ {
		addr := z80.Address(a)
		exp := by[addr]
		if a >= rom {
			exp = newBy[addr]
		}
		assert.That(t, mem.ReadByte(addr), is.EqualTo(exp), addr.String())
	}
}

func Test_Memory_WriteWord(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	newWd := createRandomWords(0x8000)

	// Act
	for i := range newWd {
		mem.WriteWord(z80.Address(i*2), newWd[i])
	}

	// Assert
	for a := 0; a < 0x10000; a += 2 {
		addr := z80.Address(a)
		exp := z80.Word(int(by[addr]) + 0x100*int(by[addr+1]))
		if a >= rom {
			exp = newWd[a/2]
		}
		assert.That(t, mem.ReadWord(addr), is.EqualTo(exp), addr.String())
	}
}

func Test_Memory_WriteWordBigEndian(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	newWd := createRandomWords(0x8000)

	// Act
	for i := range newWd {
		mem.WriteWordBigEndian(z80.Address(i*2), newWd[i])
	}

	// Assert
	for a := 0; a < 0x10000; a += 2 {
		addr := z80.Address(a)
		exp := z80.Word(0x100*int(by[addr]) + int(by[addr+1]))
		if a >= rom {
			exp = newWd[a/2]
		}
		assert.That(t, mem.ReadWordBigEndian(addr), is.EqualTo(exp), addr.String())
	}
}

func Test_Memory_WriteBytes(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	const count = 0x100
	newBy := createRandomBytes(count)
	const start = z80.Address(rom - (count / 2))
	// Act
	mem.WriteBytes(start, newBy)

	// Assert
	for a := 0; a < count; a++ {
		addr := start.Offset(a)
		exp := by[addr]
		if int(addr) >= rom {
			exp = newBy[a]
		}
		assert.That(t, mem.ReadByte(addr), is.EqualTo(exp), addr.String())
	}
}

func Test_Memory_ByteRef(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	const romAddress = z80.Address(rom - 10)
	const ramAddress = z80.Address(rom + 10)
	newVal := randomByte(by[romAddress], by[ramAddress])

	// Act
	romRef := mem.GetByteRef(romAddress)
	origRom := romRef.Get()
	romRef.Set(newVal)
	ramRef := mem.GetByteRef(ramAddress)
	origRam := ramRef.Get()
	ramRef.Set(newVal)

	// Assert
	assert.That(t, origRom, is.EqualTo(by[romAddress]), "Read ROM")
	assert.That(t, romRef.Get(), is.EqualTo(by[romAddress]), "Read Ref ROM")
	assert.That(t, mem.ReadByte(romAddress), is.EqualTo(by[romAddress]), "Read mem ROM")
	assert.That(t, origRam, is.EqualTo(by[ramAddress]), "Read RAM")
	assert.That(t, ramRef.Get(), is.EqualTo(newVal), "Read Ref RAM")
	assert.That(t, mem.ReadByte(ramAddress), is.EqualTo(newVal), "Read mem RAM")
}

func Test_Memory_WordRef(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	const romAddress = z80.Address(rom - 10)
	const ramAddress = z80.Address(rom + 10)
	romWord := z80.NewWord(by[romAddress], by[romAddress+1])
	ramWord := z80.NewWord(by[ramAddress], by[ramAddress+1])
	newVal := randomWord(romWord, ramWord)

	// Act
	romRef := mem.GetWordRef(romAddress)
	origRom := romRef.Get()
	romRef.Set(newVal)
	ramRef := mem.GetWordRef(ramAddress)
	origRam := ramRef.Get()
	ramRef.Set(newVal)

	// Assert
	assert.That(t, origRom, is.EqualTo(romWord), "Read ROM")
	assert.That(t, romRef.Get(), is.EqualTo(romWord), "Read Ref ROM")
	assert.That(t, mem.ReadWord(romAddress), is.EqualTo(romWord), "Read mem ROM")
	assert.That(t, origRam, is.EqualTo(ramWord), "Read RAM")
	assert.That(t, ramRef.Get(), is.EqualTo(newVal), "Read Ref RAM")
	assert.That(t, mem.ReadWord(ramAddress), is.EqualTo(newVal), "Read mem RAM")
}

func Test_Memory_AddressRef(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	const romAddress = z80.Address(rom - 10)
	const ramAddress = z80.Address(rom + 10)
	romVal := z80.NewWord(by[romAddress], by[romAddress+1]).AsAddress()
	ramVal := z80.NewWord(by[ramAddress], by[ramAddress+1]).AsAddress()
	newVal := randomWord(romVal.AsWord(), ramVal.AsWord()).AsAddress()

	// Act
	romRef := mem.GetAddressRef(romAddress)
	origRom := romRef.Get()
	romRef.Set(newVal)
	ramRef := mem.GetAddressRef(ramAddress)
	origRam := ramRef.Get()
	ramRef.Set(newVal)

	// Assert
	assert.That(t, origRom, is.EqualTo(romVal), "Read ROM")
	assert.That(t, romRef.Get(), is.EqualTo(romVal), "Read Ref ROM")
	assert.That(t, mem.ReadWord(romAddress).AsAddress(), is.EqualTo(romVal), "Read mem ROM")
	assert.That(t, origRam, is.EqualTo(ramVal), "Read RAM")
	assert.That(t, ramRef.Get(), is.EqualTo(newVal), "Read Ref RAM")
	assert.That(t, mem.ReadWord(ramAddress).AsAddress(), is.EqualTo(newVal), "Read mem RAM")
}

func Test_Memory_BlockRef(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	const length = 20
	const startAddress = z80.Address(rom - length/2)
	newVals := createRandomBytes(length)
	memOrig := slices.Clone(by[startAddress : startAddress+length])
	exp := slices.Clone(append(by[startAddress:startAddress+length/2], newVals[length/2:]...))

	// Act
	ref := mem.GetBlockRef(startAddress, length)
	orig := slices.Clone(ref.Get())
	ref.Set(newVals)

	// Assert
	assert.That(t, orig, is.EqualTo(memOrig), "Read")
	assert.That(t, ref.Get(), is.EqualTo(exp), "Write")
	assert.That(t, mem.ReadBytes(startAddress, startAddress+length), is.EqualTo(exp), "Mem")
}

func Test_Memory_Clone(t *testing.T) {
	// Arrange
	mem, by := createRandomMemory(rom)
	newBy := createRandomBytes(0x10000)

	// Act
	mem2 := mem.Clone()
	for i := range newBy {
		mem2.WriteByte(z80.Address(i), newBy[i])
	}

	// Assert
	for a := 0; a < 0x10000; a++ {
		addr := z80.Address(a)
		exp := by[addr]
		if a >= rom {
			exp = newBy[addr]
		}
		assert.That(t, mem.ReadByte(addr), is.EqualTo(by[addr]), fmt.Sprintf("Mem1: %s (no change)", addr))
		assert.That(t, mem2.ReadByte(addr), is.EqualTo(exp), fmt.Sprintf("Mem2: %s", addr))
	}
}
