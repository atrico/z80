package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/disassembler/symbols"
	"strings"
	"testing"
)

func Test_Formatter_Dec(t *testing.T) {
	// Arrange
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} })

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultDefault := formatter.Word(word)
			resultDec := formatter.Word(word, format.Dec)
			resultDecC := formatter.Word(word, format.NumberBase(format.Base_Dec))

			// Assert
			assert.That(t, resultDefault, is.EqualTo(fmt.Sprintf("%04x", int(word))), "Default")
			assert.That(t, resultDec, is.EqualTo(fmt.Sprintf("%d", int(word))), "Dec")
			assert.That(t, resultDecC, is.EqualTo(fmt.Sprintf("%d", int(word))), "DecC")
		})
	}
}

func Test_Formatter_Hex(t *testing.T) {
	// Arrange
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} }, format.AddDefaultConfig("", format.Dec))

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultDefault := formatter.Word(word)
			resultHex := formatter.Word(word, format.Hex)
			resultHexC := formatter.Word(word, format.NumberBase(format.Base_Hex))

			// Assert
			assert.That(t, resultDefault, is.EqualTo(fmt.Sprintf("%d", int(word))), "Default")
			assert.That(t, resultHex, is.EqualTo(fmt.Sprintf("%04x", int(word))), "Hex")
			assert.That(t, resultHexC, is.EqualTo(fmt.Sprintf("%04x", int(word))), "HexC")
		})
	}
}

func Test_Formatter_FixedWidth(t *testing.T) {
	// Arrange
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} })

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultDefaultHex := formatter.Word(word)
			resultFixedHex := formatter.Word(word, format.FixedWidth)
			resultDefaultDec := formatter.Word(word, format.Dec)
			resultFixedDec := formatter.Word(word, format.Dec, format.FixedWidth)

			// Assert
			assert.That(t, resultDefaultHex, is.EqualTo(fmt.Sprintf("%04x", int(word))), "DefaultHex")
			assert.That(t, resultFixedHex, is.EqualTo(fmt.Sprintf("%04x", int(word))), "FixedHex")
			assert.That(t, resultDefaultDec, is.EqualTo(fmt.Sprintf("%d", int(word))), "DefaultDec")
			assert.That(t, resultFixedDec, is.EqualTo(fmt.Sprintf("%5d", int(word))), "FixedDec")
		})
	}
}

func Test_Formatter_NoFixedWidth(t *testing.T) {
	// Arrange
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} }, format.AddDefaultConfig("", format.FixedWidth))

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultDefaultHex := formatter.Word(word)
			resultNoFixedHex := formatter.Word(word, format.NoFixedWidth)
			resultDefaultDec := formatter.Word(word, format.Dec)
			resultNoFixedDec := formatter.Word(word, format.Dec, format.NoFixedWidth)

			// Assert
			assert.That(t, resultDefaultHex, is.EqualTo(fmt.Sprintf("%04x", int(word))), "DefaultHex")
			assert.That(t, resultNoFixedHex, is.EqualTo(fmt.Sprintf("%x", int(word))), "NoFixedHex")
			assert.That(t, resultDefaultDec, is.EqualTo(fmt.Sprintf("%5d", int(word))), "DefaultDec")
			assert.That(t, resultNoFixedDec, is.EqualTo(fmt.Sprintf("%d", int(word))), "NoFixedDec")
		})
	}
}

func Test_Formatter_LeadingZero(t *testing.T) {
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} }, format.AddDefaultConfig("", format.LeadingZeroCustom(format.LeadingZero_None)))

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultDefaultHex := formatter.Word(word)
			resultLeadingHex := formatter.Word(word, format.LeadingZero)
			resultDefaultDec := formatter.Word(word, format.Dec)
			resultLeadingDec := formatter.Word(word, format.Dec, format.LeadingZero)

			// Assert
			assert.That(t, resultDefaultHex, is.EqualTo(fmt.Sprintf("%4x", int(word))), "DefaultHex")
			assert.That(t, resultLeadingHex, is.EqualTo(fmt.Sprintf("%04x", int(word))), "LeadingHex")
			assert.That(t, resultDefaultDec, is.EqualTo(fmt.Sprintf("%0d", int(word))), "DefaultDec")
			assert.That(t, resultLeadingDec, is.EqualTo(fmt.Sprintf("%0d", int(word))), "LeadingDec")
		})
	}
}

func Test_Formatter_Prefix(t *testing.T) {
	// Arrange
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} }, format.AddDefaultConfig("", format.Prefix("Default_")))

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultDefault := formatter.Word(word)
			resultPrefix := formatter.Word(word, format.Prefix("Prefix:"))

			// Assert
			assert.That(t, resultDefault, is.EqualTo(fmt.Sprintf("Default_%04x", int(word))), "Default")
			assert.That(t, resultPrefix, is.EqualTo(fmt.Sprintf("Prefix:%04x", int(word))), "Prefix")
		})
	}
}

func Test_Formatter_AsLabel(t *testing.T) {
	// Arrange
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} })

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultHex := formatter.Word(word, format.AsLabel)
			resultDec := formatter.Word(word, format.AsLabel, format.Dec)

			// Assert
			assert.That(t, resultHex, is.EqualTo(fmt.Sprintf("L_%04x", int(word))), "Hex")
			assert.That(t, resultDec, is.EqualTo(fmt.Sprintf("L_%05d", int(word))), "Dec")
		})
	}
}

func Test_Formatter_AsLabelCustom(t *testing.T) {
	// Arrange
	formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} })

	for _, word := range words() {
		t.Run(word.String(), func(t *testing.T) {
			// Act
			resultHex := formatter.Word(word, format.AsLabelCustom("L:"))
			resultDec := formatter.Word(word, format.AsLabelCustom("L:"), format.Dec)

			// Assert
			assert.That(t, resultHex, is.EqualTo(fmt.Sprintf("L:%04x", int(word))), "Hex")
			assert.That(t, resultDec, is.EqualTo(fmt.Sprintf("L:%05d", int(word))), "Dec")
		})
	}
}

var charFormatter = func(by z80.Byte) (ch string, ok bool) { return "char", true }

func Test_FormatterOffset(t *testing.T) {
	for name, testCase := range offsetTestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase offsetTestCase) {
				// Arrange
				formatter := format.NewFormatter(MockHighlighter{}, charFormatter, func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder { return MockSymbolTable{} })
				word := randomWord()
				offset := rg.IntBetween(-0xff, 0x100)
				t.Log(fmt.Sprintf("word=%d(%04x), offset=%d(%02x)", int(word), int(word), offset, offset))

				// Act
				result := formatter.Word(word, append(testCase.configurators, format.Offset(offset))...)
				t.Log(result)

				// Assert
				wordStr := formatter.Word(z80.Word(int(word)+offset), testCase.configurators...)
				offsetStr := offsetString(formatter, -offset, testCase.configurators...)
				expected := fmt.Sprintf("%s%s", wordStr, offsetStr)
				assert.That(t, result, is.EqualTo(expected), "")
			}(testCase)
		})
	}
}

type offsetTestCase struct {
	configurators []format.Configurator
}

var offsetTestCases = map[string]offsetTestCase{
	"DecByte":        offTest(format.Dec),
	"HexByte":        offTest(format.Hex),
	"DecWord":        offTest(format.Dec),
	"HexWord":        offTest(format.Hex),
	"DecWordFixed":   offTest(format.Dec, format.FixedWidth),
	"HexWordLeading": offTest(format.Hex, format.LeadingZero),
	"Prefix":         offTest(format.Dec, format.Prefix("Pre_")),
	"Label":          offTest(format.Hex, format.AsLabel),
}

func offTest(configurators ...format.Configurator) offsetTestCase {
	return offsetTestCase{configurators}
}

func words() (word []z80.Word) {
	word = make([]z80.Word, 4)
	for i, mask := range []int{0xffff, 0xfff, 0xff, 0xf} {
		word[i] = z80.Word(rg.IntUpto(0x10000) & mask)
	}
	return
}

func offsetString(formatter format.Formatter, offset int, configurators ...format.Configurator) string {
	text := strings.Builder{}
	if offset < 0 {
		text.WriteString("-")
	} else {
		text.WriteString("+")
	}
	offsetAbs := absInt(offset)
	if offsetAbs > 255 {
		text.WriteString(formatter.Word(z80.Word(offsetAbs), append(configurators, format.Prefix(""))...))
	} else {
		text.WriteString(formatter.Byte(z80.Byte(offsetAbs), append(configurators, format.Prefix(""))...))
	}
	return text.String()
}

func absInt(val int) int {
	if val < 0 {
		return -val
	}
	return val
}
