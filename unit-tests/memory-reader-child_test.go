package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"testing"
)

type ParentFactory func(mem z80.Memory, start, end z80.Address) z80.MemoryReader

func Test_MemoryReader_Child(t *testing.T) {
	parents := map[string]ParentFactory{
		"Reader":      func(mem z80.Memory, start, end z80.Address) z80.MemoryReader { return mem.Reader(start) },
		"ReaderCount": func(mem z80.Memory, start, end z80.Address) z80.MemoryReader { return mem.ReaderCount(start, end) },
		"Reader_Child": func(mem z80.Memory, start, end z80.Address) z80.MemoryReader {
			return mem.Reader(start).ChildReader(int(end - start))
		},
		"ReaderCount_Child": func(mem z80.Memory, start, end z80.Address) z80.MemoryReader {
			return mem.ReaderCount(start, end).ChildReader(int(end - start))
		},
	}
	for name, fact := range parents {
		t.Run(name, func(t *testing.T) {
			t.Run("PeekByte", func(t *testing.T) { test_MemoryReader_Child_PeekByte(t, fact) })
			t.Run("ReadByte", func(t *testing.T) { test_MemoryReader_Child_ReadByte(t, fact) })
			t.Run("PeekWord", func(t *testing.T) { test_MemoryReader_Child_PeekWord(t, fact) })
			t.Run("PeekWordBigEndian", func(t *testing.T) { test_MemoryReader_Child_PeekWordBigEndian(t, fact) })
			t.Run("ReadWord", func(t *testing.T) { test_MemoryReader_Child_ReadWord(t, fact) })
			t.Run("ReadWordBigEndian", func(t *testing.T) { test_MemoryReader_Child_ReadWordBigEndian(t, fact) })
			t.Run("PeekAddress", func(t *testing.T) { test_MemoryReader_Child_PeekAddress(t, fact) })
			t.Run("PeekAddressBigEndian", func(t *testing.T) { test_MemoryReader_Child_PeekAddressBigEndian(t, fact) })
			t.Run("ReadAddress", func(t *testing.T) { test_MemoryReader_Child_ReadAddress(t, fact) })
			t.Run("ReadAddressBigEndian", func(t *testing.T) { test_MemoryReader_Child_ReadAddressBigEndian(t, fact) })
			t.Run("ReadBlockWithCount", func(t *testing.T) { test_MemoryReader_Child_ReadBlockWithCount(t, fact) })
			t.Run("ReadBytes", func(t *testing.T) { test_MemoryReader_Child_ReadBytes(t, fact) })
			t.Run("ReadWords", func(t *testing.T) { test_MemoryReader_Child_ReadWords(t, fact) })
			t.Run("ReadAllBytes", func(t *testing.T) { test_MemoryReader_Child_ReadAllBytes(t, fact) })
			t.Run("ReadAllWords", func(t *testing.T) { test_MemoryReader_Child_ReadAllWords(t, fact) })
			t.Run("ResetByteBuffer", func(t *testing.T) { test_MemoryReader_Child_ResetByteBuffer(t, fact) })
			t.Run("SkipBytes", func(t *testing.T) { test_MemoryReader_Child_SkipBytes(t, fact) })
			t.Run("PeekByteAt", func(t *testing.T) { test_MemoryReader_Child_PeekByteAt(t, fact) })
			t.Run("PeekWordAt", func(t *testing.T) { test_MemoryReader_Child_PeekWordAt(t, fact) })
			t.Run("PeekWordBigEndianAt", func(t *testing.T) { test_MemoryReader_Child_PeekWordBigEndianAt(t, fact) })
			t.Run("PeekAddressAt", func(t *testing.T) { test_MemoryReader_Child_PeekAddressAt(t, fact) })
			t.Run("PeekAddressBigEndianAt", func(t *testing.T) { test_MemoryReader_Child_PeekAddressBigEndianAt(t, fact) })
			t.Run("Reset", func(t *testing.T) { test_MemoryReader_Child_Reset(t, fact) })
		})
	}
}

func test_MemoryReader_Child_PeekByte(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.PeekByte(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_ReadByte(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func test_MemoryReader_Child_PeekWord(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWord(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_PeekWordBigEndian(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordBigEndian(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_ReadWord(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Word], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWord(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_ReadWordBigEndian(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Word], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWordBigEndian(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_PeekAddress(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddress(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_PeekAddressBigEndian(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressBigEndian(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_ReadAddress(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Address], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.ReadAddress(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_ReadAddressBigEndian(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Address], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.ReadAddressBigEndian(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_ReadBlockWithCount(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	const blockCount = 6
	mem.WriteWord(start, z80.Word(blockCount))
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	result := reader.ReadBlockWithCount()
	final := reader.CurrentAddress()

	// Assert
	assert.That(t, len(result), is.EqualTo(blockCount), "Size of block")
	for i, by := range result {
		assert.That(t, by, is.EqualTo(mem.ReadByte(start.Offset(2+i))), fmt.Sprintf("Read %d", i))
	}
	assert.That(t, final, is.EqualTo(start.Offset(2+blockCount)), "Final address")
	expBuffer := mem.ReadBytes(start, start.Offset(2+blockCount))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_ReadBytes(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	const blockCount = count / 2
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[[]z80.Byte], 0, count/blockCount)
	for reader.More() {
		results = append(results, readResult[[]z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadBytes(blockCount),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/blockCount), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.ListEqualTo(mem.ReadBytes(expAddr, expAddr.Offset(blockCount))), fmt.Sprintf("Read %d: value", i))
		expAddr += blockCount
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func test_MemoryReader_Child_ReadWords(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 16
	end := start.Offset(count * 2)
	const blockCount = count / 4
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[[]z80.Word], 0, count/blockCount/2)
	for reader.More() {
		results = append(results, readResult[[]z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWords(blockCount),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/blockCount/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.ListEqualTo(memReadWords(mem, expAddr, blockCount)), fmt.Sprintf("Read %d: value", i))
		expAddr += blockCount * 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func test_MemoryReader_Child_ReadAllBytes(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	result := readResult[[]z80.Byte]{
		reader.CurrentAddress(),
		reader.ReadAllBytes(),
	}

	// Assert
	assert.That(t, result.addr, is.EqualTo(start), "Read: address")
	assert.That(t, result.value, is.ListEqualTo(mem.ReadBytes(start, start.Offset(count))), "Read: value")
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func test_MemoryReader_Child_ReadAllWords(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 16
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	result := readResult[[]z80.Word]{
		reader.CurrentAddress(),
		reader.ReadAllWords(),
	}

	// Assert
	assert.That(t, result.addr, is.EqualTo(start), "Read: address")
	assert.That(t, result.value, is.ListEqualTo(memReadWords(mem, start, count/2)), "Read: value")
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func test_MemoryReader_Child_ResetByteBuffer(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
		if len(results) == count/2 {
			reader.ResetByteBuffer()
		}
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start.Offset(count/2), start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func test_MemoryReader_Child_SkipBytes(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Byte], 0, count/2)
	reader.SkipBytes(count / 2)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start + count/2
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start.Offset(count/2), start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func test_MemoryReader_Child_PeekByteAt(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.PeekByteAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_PeekWordAt(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_PeekWordBigEndianAt(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordBigEndianAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_PeekAddressAt(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr.Offset(i)).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_PeekAddressBigEndianAt(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	reader := parent.ChildReader(count)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressBigEndianAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr.Offset(i)).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func test_MemoryReader_Child_Reset(t *testing.T, parentFactory ParentFactory) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count * 2)
	parent := parentFactory(mem, start, end)
	parent.SkipBytes(2) // Skip a couple (to test reset goes back to start of child)
	reader := parent.ChildReader(count)

	// Act
	// Read some
	reader.ReadWord()
	reader.ReadWord()
	reader.ReadWord()
	reader.Reset()
	results := make([]readResult[z80.Byte], 0, count)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	realStart := start.Offset(2)
	expAddr := realStart
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(realStart, realStart.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}
