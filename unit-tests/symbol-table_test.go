package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/symbols"
	"testing"
)

func Test_SymbolTable_New(t *testing.T) {
	// Arrange
	st := symbols.NewSymbolTable(labelFormatter)

	// Act
	_, ok := st.Lookup(z80.Address(0))
	mp := st.Map()

	// Assert
	assert.That(t, ok, is.False, "lookup")
	assert.That(t, len(mp), is.EqualTo(0), "map")
}

func Test_SymbolTable_AddNoAnnotation(t *testing.T) {
	// Arrange
	addr := randomAddress()
	st := symbols.NewSymbolTable(labelFormatter)

	// Act
	st.Add(addr)
	_, ok := st.Lookup(addr)
	mp := st.Map()

	// Assert
	assert.That(t, ok, is.False, "lookup")
	assert.That(t, len(mp), is.EqualTo(0), "map")
}

func Test_SymbolTable_AddAnnotation(t *testing.T) {
	// Arrange
	addr := randomAddress()
	label := rg.Identifier()
	annotations, _ := z80.NewMemoryAnnotations()
	annotations.Update(addr, z80.MemoryAnnotation{Label: label})
	st := symbols.NewSymbolTable(labelFormatter)
	st.AddAnnotations(annotations)

	// Act
	st.Add(addr)

	// Assert
	assertSymbolTable(t, st, symbols.SymbolTableEntry{Address: addr, Label: label})
}

func Test_SymbolTable_AddUsedNoAnnotation(t *testing.T) {
	// Arrange
	addr := randomAddress()
	used1 := randomAddress()
	used2 := randomAddress()
	st := symbols.NewSymbolTable(labelFormatter)

	// Act
	st.AddUsed(addr, used1)
	st.AddUsed(addr, used2)

	// Assert
	assertSymbolTable(t, st, symbols.SymbolTableEntry{Address: addr, Label: labelFormatter(addr), Used: []z80.Address{used1, used2}})
}

func Test_SymbolTable_AddUsedAnnotation(t *testing.T) {
	// Arrange
	addr := randomAddress()
	label := rg.Identifier()
	used1 := randomAddress()
	used2 := randomAddress()
	annotations, _ := z80.NewMemoryAnnotations()
	annotations.Update(addr, z80.MemoryAnnotation{Label: label})
	st := symbols.NewSymbolTable(labelFormatter)
	st.AddAnnotations(annotations)

	// Act
	st.AddUsed(addr, used1)
	st.AddUsed(addr, used2)

	// Assert
	assertSymbolTable(t, st, symbols.SymbolTableEntry{Address: addr, Label: label, Used: []z80.Address{used1, used2}})
}

func assertSymbolTable(t *testing.T, st symbols.SymbolTable, entries ...symbols.SymbolTableEntry) {
	mp := st.Map()
	assert.That(t, len(mp), is.EqualTo(len(entries)), "map length")
	for _, entry := range entries {
		lu, ok := st.Lookup(entry.Address)
		assert.That(t, ok, is.True, "lookup ok: %s", entry.Address)
		assert.That(t, lu, is.DeepEqualTo(entry), "lookup entry: %s", entry.Address)
		assert.That(t, mp[entry.Address], is.DeepEqualTo(entry), "map entry: %s", entry.Address)
	}
}

func labelFormatter(address z80.Address) string {
	return fmt.Sprintf("L_%04x", address)
}
