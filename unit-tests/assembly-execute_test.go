package unit_tests

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/executor"
	"gitlab.com/atrico/z80/snapshot"
	"gitlab.com/atrico/z80/unit-tests/flags"
	"gitlab.com/atrico/z80/unit-tests/registers"
	"slices"
	"strconv"
	"strings"
	"testing"
)

func Test_AssemblyExecute(t *testing.T) {
	for _, testCase := range asmExecuteTestCases {
		for _, bytes := range testCase.bytes {
			// Get instruction display
			snap := createBlankSnapshot()
			for _, mod := range testCase.setup {
				mod(snap, testCase.rnd)
			}
			// Write "program"
			pc := snap.ProgramCounter().Get()
			for o, by := range bytes {
				snap.WriteByte(pc.Offset(o), by)
			}
			// Get instruction for test name
			instr, _ := executor.GetNextInstruction(snap)
			t.Run(fmt.Sprintf("%s___%s", instr.Display(MockFormatter{}), formatBytes(bytes)), func(t *testing.T) {
				func(testCase asmExecuteTestCase, snap snapshot.Snapshot, rnd rands, bytes []z80.Byte) {
					// Arrange
					t.Log(rnd)
					// Setup expected
					expected := snap.Clone()
					// Increment R
					incReg8(registers.R)(expected)
					// If prefix, inc again
					prefix := []z80.Byte{0xdd, 0xfd, 0xcb, 0xed}
					if slices.Contains(prefix, bytes[0]) {
						incReg8(registers.R)(expected)
					}
					// Increment PC
					incPC(len(bytes))(expected)
					// Finally run modifiers
					for _, mod := range testCase.expected {
						mod(expected, rnd)
					}

					// Act
					executor.ExecuteCode(snap)

					// Assert
					assertSnapshot(t, snap, expected)

				}(testCase, snap, testCase.rnd, bytes)
			})
		}
	}
}

type asmExecuteTestCase struct {
	rnd             rands
	bytes           [][]z80.Byte
	setup, expected []testSnapshotModifier
}

var asmExecuteTestCases []asmExecuteTestCase

func init() {
	//// Nop
	//addTest(tb().SETUP().EXPECTED().BYTES("00"))
	//// LD 8 bit
	//addLd8bit()
	//// LD 16 bit
	//addLd16bit()
	//// Push and Pop
	//pushPop()
	//// Exchange
	//exchange()
	// Block operations
	//blockLd()
	blockCp()
}

func addLd8bit() {
	// Temp vars
	var val, off z80.Byte
	var addr z80.Address
	// std reg, reg
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("40").BYTES("dd40").BYTES("fd40")) // ld b,b
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("41").BYTES("dd41").BYTES("fd41")) // ld b,c
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("42").BYTES("dd42").BYTES("fd42")) // ld b,d
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("43").BYTES("dd43").BYTES("fd43")) // ld b,e
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.H, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("44"))                             // ld b,h
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.L, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("45"))                             // ld b,l
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("47").BYTES("dd47").BYTES("fd47")) // ld b,a
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("48").BYTES("dd48").BYTES("fd48")) // ld c,b
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("49").BYTES("dd49").BYTES("fd49")) // ld c,c
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("4a").BYTES("dd4a").BYTES("fd4a")) // ld c,d
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("4b").BYTES("dd4b").BYTES("fd4b")) // ld c,e
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.H, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("4c"))                             // ld c,h
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.L, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("4d"))                             // ld c,l
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("4f").BYTES("dd4f").BYTES("fd4f")) // ld c,a
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("50").BYTES("dd50").BYTES("fd50")) // ld d,b
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("51").BYTES("dd51").BYTES("fd51")) // ld d,c
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("52").BYTES("dd52").BYTES("fd52")) // ld d,d
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("53").BYTES("dd53").BYTES("fd53")) // ld d,e
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.H, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("54"))                             // ld d,h
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.L, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("55"))                             // ld d,l
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("57").BYTES("dd57").BYTES("fd57")) // ld d,a
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("58").BYTES("dd58").BYTES("fd58")) // ld e,b
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("59").BYTES("dd59").BYTES("fd59")) // ld e,c
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("5a").BYTES("dd5a").BYTES("fd5a")) // ld e,d
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("5b").BYTES("dd5b").BYTES("fd5b")) // ld e,e
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.H, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("5c"))                             // ld e,h
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.L, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("5d"))                             // ld e,l
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("5f").BYTES("dd5f").BYTES("fd5f")) // ld e,a
	addTest(tb().SETUP(reg8Rnd(registers.H, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("60"))                             // ld h,b
	addTest(tb().SETUP(reg8Rnd(registers.H, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("61"))                             // ld h,c
	addTest(tb().SETUP(reg8Rnd(registers.H, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("62"))                             // ld h,d
	addTest(tb().SETUP(reg8Rnd(registers.H, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("63"))                             // ld h,e
	addTest(tb().SETUP(reg8Rnd(registers.H, 0), reg8Rnd(registers.H, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("64"))                             // ld h,h
	addTest(tb().SETUP(reg8Rnd(registers.H, 0), reg8Rnd(registers.L, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("65"))                             // ld h,l
	addTest(tb().SETUP(reg8Rnd(registers.H, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("67"))                             // ld h,a
	addTest(tb().SETUP(reg8Rnd(registers.L, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("68"))                             // ld l,b
	addTest(tb().SETUP(reg8Rnd(registers.L, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("69"))                             // ld l,c
	addTest(tb().SETUP(reg8Rnd(registers.L, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("6a"))                             // ld l,d
	addTest(tb().SETUP(reg8Rnd(registers.L, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("6b"))                             // ld l,e
	addTest(tb().SETUP(reg8Rnd(registers.L, 0), reg8Rnd(registers.H, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("6c"))                             // ld l,h
	addTest(tb().SETUP(reg8Rnd(registers.L, 0), reg8Rnd(registers.L, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("6d"))                             // ld l,l
	addTest(tb().SETUP(reg8Rnd(registers.L, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("6f"))                             // ld l,a
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("78").BYTES("dd78").BYTES("fd78")) // ld a,b
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("79").BYTES("dd79").BYTES("fd79")) // ld a,c
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("7a").BYTES("dd7a").BYTES("fd7a")) // ld a,d
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("7b").BYTES("dd7b").BYTES("fd7b")) // ld a,e
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.H, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("7c"))                             // ld a,h
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.L, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("7d"))                             // ld a,l
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("7f").BYTES("dd7f").BYTES("fd7f")) // ld a,a
	// reg, ixh/l
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.IXh, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("dd44"))     // ld b,ixh
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.IXl, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("dd45"))     // ld b,ixl
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.IXh, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("dd4c"))     // ld c,ixh
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.IXl, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("dd4d"))     // ld c,ixl
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.IXh, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("dd54"))     // ld d,ixh
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.IXl, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("dd55"))     // ld d,ixl
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.IXh, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("dd5c"))     // ld e,ixh
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.IXl, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("dd5d"))     // ld e,ixl
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd60"))   // ld ixh,b
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd61"))   // ld ixh,c
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd62"))   // ld ixh,d
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd63"))   // ld ixh,e
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0), reg8Rnd(registers.IXh, 1)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd64")) // ld ixh,ixh
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0), reg8Rnd(registers.IXl, 1)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd65")) // ld ixh,ixl
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd67"))   // ld ixh,a
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd68"))   // ld ixl,b
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd69"))   // ld ixl,c
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd6a"))   // ld ixl,d
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd6b"))   // ld ixl,e
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0), reg8Rnd(registers.IXh, 1)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd6c")) // ld ixl,ixh
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0), reg8Rnd(registers.IXl, 1)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd6d")) // ld ixl,ixl
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd6f"))   // ld ixl,a
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.IXh, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("dd7c"))     // ld a,ixh
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.IXl, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("dd7d"))     // ld a,ixl
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.IYh, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("fd44"))     // ld b,iyh
	addTest(tb().SETUP(reg8Rnd(registers.B, 0), reg8Rnd(registers.IYl, 1)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("fd45"))     // ld b,iyl
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.IYh, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("fd4c"))     // ld c,iyh
	addTest(tb().SETUP(reg8Rnd(registers.C, 0), reg8Rnd(registers.IYl, 1)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("fd4d"))     // ld c,iyl
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.IYh, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("fd54"))     // ld d,iyh
	addTest(tb().SETUP(reg8Rnd(registers.D, 0), reg8Rnd(registers.IYl, 1)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("fd55"))     // ld d,iyl
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.IYh, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("fd5c"))     // ld e,iyh
	addTest(tb().SETUP(reg8Rnd(registers.E, 0), reg8Rnd(registers.IYl, 1)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("fd5d"))     // ld e,iyl
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd60"))   // ld iyh,b
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd61"))   // ld iyh,c
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd62"))   // ld iyh,d
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd63"))   // ld iyh,e
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0), reg8Rnd(registers.IYh, 1)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd64")) // ld iyh,iyh
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0), reg8Rnd(registers.IYl, 1)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd65")) // ld iyh,iyl
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd67"))   // ld iyh,a
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0), reg8Rnd(registers.B, 1)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd68"))   // ld iyl,b
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0), reg8Rnd(registers.C, 1)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd69"))   // ld iyl,c
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0), reg8Rnd(registers.D, 1)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd6a"))   // ld iyl,d
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0), reg8Rnd(registers.E, 1)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd6b"))   // ld iyl,e
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0), reg8Rnd(registers.IYh, 1)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd6c")) // ld iyl,iyh
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0), reg8Rnd(registers.IYl, 1)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd6d")) // ld iyl,iyl
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd6f"))   // ld iyl,a
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.IYh, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("fd7c"))     // ld a,iyh
	addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Rnd(registers.IYl, 1)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("fd7d"))     // ld a,iyl
	// reg, literal
	addTest(tb().SETUP(reg8Rnd(registers.B, 0)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("06n1").BYTES("dd06n1").BYTES("fd06n1")) // ld b,NN
	addTest(tb().SETUP(reg8Rnd(registers.C, 0)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("0en1").BYTES("dd0en1").BYTES("fd0en1")) // ld c,NN
	addTest(tb().SETUP(reg8Rnd(registers.D, 0)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("16n1").BYTES("dd16n1").BYTES("fd16n1")) // ld d,NN
	addTest(tb().SETUP(reg8Rnd(registers.E, 0)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("1en1").BYTES("dd1en1").BYTES("fd1en1")) // ld e,NN
	addTest(tb().SETUP(reg8Rnd(registers.H, 0)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("26n1"))                                 // ld h,NN
	addTest(tb().SETUP(reg8Rnd(registers.L, 0)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("2en1"))                                 // ld l,NN
	addTest(tb().SETUP(reg8Rnd(registers.A, 0)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("3en1").BYTES("dd3en1").BYTES("fd3en1")) // ld a,NN
	addTest(tb().SETUP(reg8Rnd(registers.IXh, 0)).EXPECTED(reg8Rnd(registers.IXh, 1)).BYTES("dd26n1"))                           // ld ixh,NN
	addTest(tb().SETUP(reg8Rnd(registers.IXl, 0)).EXPECTED(reg8Rnd(registers.IXl, 1)).BYTES("dd2en1"))                           // ld ixl,NN
	addTest(tb().SETUP(reg8Rnd(registers.IYh, 0)).EXPECTED(reg8Rnd(registers.IYh, 1)).BYTES("fd26n1"))                           // ld iyh,NN
	addTest(tb().SETUP(reg8Rnd(registers.IYl, 0)).EXPECTED(reg8Rnd(registers.IYl, 1)).BYTES("fd2en1"))                           // ld iyl,NN
	// reg, (hl)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 1), reg8Rnd(registers.B, 0)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("46")) // ld b,(hl)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 1), reg8Rnd(registers.C, 0)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("4e")) // ld c,(hl)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 1), reg8Rnd(registers.D, 0)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("56")) // ld d,(hl)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 1), reg8Rnd(registers.E, 0)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("5e")) // ld e,(hl)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 1)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("66"))                          // ld h,(hl)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 1)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("6e"))                          // ld l,(hl)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 1), reg8Rnd(registers.A, 0)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("7e")) // ld a,(hl)
	// (hl), reg
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0), reg8Rnd(registers.B, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("70")) // ld (hl),b
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0), reg8Rnd(registers.C, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("71")) // ld (hl),c
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0), reg8Rnd(registers.D, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("72")) // ld (hl),d
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0), reg8Rnd(registers.E, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("73")) // ld (hl),e
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0)).EXPECTED(memByteLit(addr, addr.AsWord().Hi())).BYTES("74"))         // ld (hl),h
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0)).EXPECTED(memByteLit(addr, addr.AsWord().Lo())).BYTES("75"))         // ld (hl),l
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0), reg8Rnd(registers.A, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("77")) // ld (hl),a
	// (hl/ix/iy), lit
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.HL, addr.AsWord()), memByteRnd(addr, 0)).EXPECTED(memByteRnd(addr, 1)).BYTES("36n1"))                                             // ld (hl),NN
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd36n2n1")) // ld (ix+NN),NN
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd36n2n1")) // ld (iy+NN),NN
	// reg, (ix/y+dd)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.B, 0)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("dd46n2")) // ld b,(ix+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.C, 0)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("dd4en2")) // ld c,(ix+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.D, 0)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("dd56n2")) // ld d,(ix+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.E, 0)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("dd5en2")) // ld e,(ix+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.H, 0)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("dd66n2")) // ld h,(ix+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.L, 0)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("dd6en2")) // ld l,(ix+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.A, 0)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("dd7en2")) // ld a,(ix+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.B, 0)).EXPECTED(reg8Rnd(registers.B, 1)).BYTES("fd46n2")) // ld b,(iy+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.C, 0)).EXPECTED(reg8Rnd(registers.C, 1)).BYTES("fd4en2")) // ld c,(iy+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.D, 0)).EXPECTED(reg8Rnd(registers.D, 1)).BYTES("fd56n2")) // ld d,(iy+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.E, 0)).EXPECTED(reg8Rnd(registers.E, 1)).BYTES("fd5en2")) // ld e,(iy+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.H, 0)).EXPECTED(reg8Rnd(registers.H, 1)).BYTES("fd66n2")) // ld h,(iy+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.L, 0)).EXPECTED(reg8Rnd(registers.L, 1)).BYTES("fd6en2")) // ld l,(iy+NN)
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 1), reg8Rnd(registers.A, 0)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("fd7en2")) // ld a,(iy+NN)
	// (ix/y+dd),reg,
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.B, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd70n2")) // ld (ix+NN),b
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.C, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd71n2")) // ld (ix+NN),c
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.D, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd72n2")) // ld (ix+NN),d
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.E, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd73n2")) // ld (ix+NN),e
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.H, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd74n2")) // ld (ix+NN),h
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.L, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd75n2")) // ld (ix+NN),l
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IX, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.A, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("dd77n2")) // ld (ix+NN),a
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.B, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd70n2")) // ld (iy+NN),b
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.C, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd71n2")) // ld (iy+NN),c
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.D, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd72n2")) // ld (iy+NN),d
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.E, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd73n2")) // ld (iy+NN),e
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.H, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd74n2")) // ld (iy+NN),h
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.L, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd75n2")) // ld (iy+NN),l
	addTest(tb().Get8(2, &off).GetAddr(0, &addr).SETUP(reg16Lit(registers.IY, addr.AsWord()), memByteRnd(addr.OffsetB(off), 0), reg8Rnd(registers.A, 1)).EXPECTED(memByteRnd(addr.OffsetB(off), 1)).BYTES("fd77n2")) // ld (iy+NN),a
	// A, (bc/de/lit)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.BC, addr.AsWord()), memByteRnd(addr, 1), reg8Rnd(registers.A, 0)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("0a").BYTES("dd0a").BYTES("fd0a")) // ld a,(bc)
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.DE, addr.AsWord()), memByteRnd(addr, 1), reg8Rnd(registers.A, 0)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("1a").BYTES("dd1a").BYTES("fd1a")) // ld a,(de)
	addTest(tb().GetAddr(0, &addr).SETUP(memByteRnd(addr, 1), reg8Rnd(registers.A, 0)).EXPECTED(reg8Rnd(registers.A, 1)).BYTES("3aw0--").BYTES("dd3aw0--").BYTES("fd3aw0--"))                            // ld a,(NNNN)
	// (bc/de/lit), A
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.BC, addr.AsWord()), memByteRnd(addr, 0), reg8Rnd(registers.A, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("02").BYTES("dd02").BYTES("fd02")) // ld (bc),a
	addTest(tb().GetAddr(0, &addr).SETUP(reg16Lit(registers.DE, addr.AsWord()), memByteRnd(addr, 0), reg8Rnd(registers.A, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("12").BYTES("dd12").BYTES("fd12")) // ld (de),a
	addTest(tb().GetAddr(0, &addr).SETUP(memByteRnd(addr, 0), reg8Rnd(registers.A, 1)).EXPECTED(memByteRnd(addr, 1)).BYTES("32w0--").BYTES("dd32w0--").BYTES("fd32w0--"))                            // ld (NNNN),a
	// I and R
	addTest(tb().SETUP(reg8Rnd(registers.I, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.I, 1)).BYTES("ed47")) // ld i,a
	addTest(tb().SETUP(reg8Rnd(registers.R, 0), reg8Rnd(registers.A, 1)).EXPECTED(reg8Rnd(registers.R, 1)).BYTES("ed4f")) // ld r,a
	// Flags settings
	// ld a,i & ld a,r affect flags based on value assigned, test all combinations
	var irMask byte = 0xfe
	var flgs theFlags
	irLd := map[z80.Byte]theFlags{
		0x00: {n: false, b3: false, h: false, b5: false, z: true, s: false}, // zero
		0x7f: {n: false, b3: true, h: false, b5: true, z: false, s: false},  // pos
		0x80: {n: false, b3: false, h: false, b5: false, z: false, s: true}, // neg
	}
	for _, ei := range []bool{true, false} {
		for val, flgs = range irLd {
			flgs.pv = ei
			addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Lit(registers.I, val), enableInterruptsT(ei), invertFlags(irMask, flgs)).EXPECTED(reg8Lit(registers.A, val), setupFlags(irMask, flgs)).BYTES("ed57"))   // ld a,i
			addTest(tb().SETUP(reg8Rnd(registers.A, 0), reg8Lit(registers.R, val-2), enableInterruptsT(ei), invertFlags(irMask, flgs)).EXPECTED(reg8Lit(registers.A, val), setupFlags(irMask, flgs)).BYTES("ed5f")) // ld a,r - r is increased by 1 before assignment
		}
	}
}

func addLd16bit() {
	var addr z80.Address
	// reg,lit
	addTest(tb().SETUP(reg16Rnd(registers.BC, 0)).EXPECTED(reg16Rnd(registers.BC, 1)).BYTES("01w1--").BYTES("dd01w1--").BYTES("fd01w1--")) // ld bc,NNNN
	addTest(tb().SETUP(reg16Rnd(registers.DE, 0)).EXPECTED(reg16Rnd(registers.DE, 1)).BYTES("11w1--").BYTES("dd11w1--").BYTES("fd11w1--")) // ld de,NNNN
	addTest(tb().SETUP(reg16Rnd(registers.HL, 0)).EXPECTED(reg16Rnd(registers.HL, 1)).BYTES("21w1--"))                                     // ld hl,NNNN
	addTest(tb().SETUP(reg16Rnd(registers.SP, 0)).EXPECTED(reg16Rnd(registers.SP, 1)).BYTES("31w1--").BYTES("dd31w1--").BYTES("fd31w1--")) // ld sp,NNNN
	addTest(tb().SETUP(reg16Rnd(registers.IX, 0)).EXPECTED(reg16Rnd(registers.IX, 1)).BYTES("dd21w1--"))                                   // ld ix,NNNN
	addTest(tb().SETUP(reg16Rnd(registers.IY, 0)).EXPECTED(reg16Rnd(registers.IY, 1)).BYTES("fd21w1--"))                                   // ld iy,NNNN
	// reg,(lit)
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Rnd(registers.HL, 0), memWordRnd(addr, 1)).EXPECTED(reg16Rnd(registers.HL, 1)).BYTES("2aw2--"))   // ld hl,(NNNN)
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Rnd(registers.IX, 0), memWordRnd(addr, 1)).EXPECTED(reg16Rnd(registers.IX, 1)).BYTES("dd2aw2--")) // ld ix,(NNNN)
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Rnd(registers.IY, 0), memWordRnd(addr, 1)).EXPECTED(reg16Rnd(registers.IY, 1)).BYTES("fd2aw2--")) // ld iy,(NNNN)
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Rnd(registers.BC, 0), memWordRnd(addr, 1)).EXPECTED(reg16Rnd(registers.BC, 1)).BYTES("ed4bw2--")) // ld bc,(NNNN)
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Rnd(registers.DE, 0), memWordRnd(addr, 1)).EXPECTED(reg16Rnd(registers.DE, 1)).BYTES("ed5bw2--")) // ld de,(NNNN)
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Rnd(registers.SP, 0), memWordRnd(addr, 1)).EXPECTED(reg16Rnd(registers.SP, 1)).BYTES("ed7bw2--")) // ld sp,(NNNN)
	// (lit),reg
	addTest(tb().GetAddr(2, &addr).SETUP(memWordRnd(addr, 0), reg16Rnd(registers.HL, 1)).EXPECTED(memWordRnd(addr, 1)).BYTES("22w2--"))   // ld (NNNN),hl
	addTest(tb().GetAddr(2, &addr).SETUP(memWordRnd(addr, 0), reg16Rnd(registers.IX, 1)).EXPECTED(memWordRnd(addr, 1)).BYTES("dd22w2--")) // ld (NNNN),ix
	addTest(tb().GetAddr(2, &addr).SETUP(memWordRnd(addr, 0), reg16Rnd(registers.IY, 1)).EXPECTED(memWordRnd(addr, 1)).BYTES("fd22w2--")) // ld (NNNN),iy
	addTest(tb().GetAddr(2, &addr).SETUP(memWordRnd(addr, 0), reg16Rnd(registers.BC, 1)).EXPECTED(memWordRnd(addr, 1)).BYTES("ed43w2--")) // ld (NNNN),bc
	addTest(tb().GetAddr(2, &addr).SETUP(memWordRnd(addr, 0), reg16Rnd(registers.DE, 1)).EXPECTED(memWordRnd(addr, 1)).BYTES("ed53w2--")) // ld (NNNN),de
	addTest(tb().GetAddr(2, &addr).SETUP(memWordRnd(addr, 0), reg16Rnd(registers.SP, 1)).EXPECTED(memWordRnd(addr, 1)).BYTES("ed73w2--")) // ld (NNNN),sp
	// sp,reg
	addTest(tb().SETUP(reg16Rnd(registers.SP, 0), reg16Rnd(registers.HL, 1)).EXPECTED(reg16Rnd(registers.SP, 1)).BYTES("f9"))   // ld sp,hl
	addTest(tb().SETUP(reg16Rnd(registers.SP, 0), reg16Rnd(registers.IX, 1)).EXPECTED(reg16Rnd(registers.SP, 1)).BYTES("ddf9")) // ld sp,ix
	addTest(tb().SETUP(reg16Rnd(registers.SP, 0), reg16Rnd(registers.IY, 1)).EXPECTED(reg16Rnd(registers.SP, 1)).BYTES("fdf9")) // ld sp,iy
}

func pushPop() {
	var addr z80.Address
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr.Offset(-2), 0), reg16Rnd(registers.BC, 1)).EXPECTED(reg16Lit(registers.SP, addr.Offset(-2).AsWord()), memWordRnd(addr.Offset(-2), 1)).BYTES("c5").BYTES("ddc5").BYTES("fdc5")) // push bc
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr.Offset(-2), 0), reg16Rnd(registers.DE, 1)).EXPECTED(reg16Lit(registers.SP, addr.Offset(-2).AsWord()), memWordRnd(addr.Offset(-2), 1)).BYTES("d5").BYTES("ddd5").BYTES("fdd5")) // push de
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr.Offset(-2), 0), reg16Rnd(registers.HL, 1)).EXPECTED(reg16Lit(registers.SP, addr.Offset(-2).AsWord()), memWordRnd(addr.Offset(-2), 1)).BYTES("e5"))                             // push hl
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr.Offset(-2), 0), reg16Rnd(registers.AF, 1)).EXPECTED(reg16Lit(registers.SP, addr.Offset(-2).AsWord()), memWordRnd(addr.Offset(-2), 1)).BYTES("f5").BYTES("ddf5").BYTES("fdf5")) // push af
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr.Offset(-2), 0), reg16Rnd(registers.IX, 1)).EXPECTED(reg16Lit(registers.SP, addr.Offset(-2).AsWord()), memWordRnd(addr.Offset(-2), 1)).BYTES("dde5"))                           // push ix
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr.Offset(-2), 0), reg16Rnd(registers.IY, 1)).EXPECTED(reg16Lit(registers.SP, addr.Offset(-2).AsWord()), memWordRnd(addr.Offset(-2), 1)).BYTES("fde5"))                           // push iy
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 1), reg16Rnd(registers.BC, 0)).EXPECTED(reg16Rnd(registers.BC, 1), reg16Lit(registers.SP, addr.Offset(2).AsWord())).BYTES("c1").BYTES("ddc1").BYTES("fdc1"))                  // pop bc
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 1), reg16Rnd(registers.BC, 0)).EXPECTED(reg16Rnd(registers.DE, 1), reg16Lit(registers.SP, addr.Offset(2).AsWord())).BYTES("d1").BYTES("ddd1").BYTES("fdd1"))                  // pop de                                                                                                                                                                          // pop de
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 1), reg16Rnd(registers.BC, 0)).EXPECTED(reg16Rnd(registers.HL, 1), reg16Lit(registers.SP, addr.Offset(2).AsWord())).BYTES("e1"))                                              // pop hl                                                                                                                                                                           // pop hl
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 1), reg16Rnd(registers.BC, 0)).EXPECTED(reg16Rnd(registers.AF, 1), reg16Lit(registers.SP, addr.Offset(2).AsWord())).BYTES("f1").BYTES("ddf1").BYTES("fdf1"))                  // pop af                                                                                                                                                                              // pop af
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 1), reg16Rnd(registers.BC, 0)).EXPECTED(reg16Rnd(registers.IX, 1), reg16Lit(registers.SP, addr.Offset(2).AsWord())).BYTES("dde1"))                                            // pop ix                                                                                                                                                         // pop ix
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 1), reg16Rnd(registers.BC, 0)).EXPECTED(reg16Rnd(registers.IY, 1), reg16Lit(registers.SP, addr.Offset(2).AsWord())).BYTES("fde1"))                                            // pop iy                                                                                                                                                // pop iy
}

func exchange() {
	var addr z80.Address
	addTest(tb().SETUP(reg16Rnd(registers.AF, 0), reg16Rnd(registers.AFp, 1)).EXPECTED(reg16Rnd(registers.AF, 1), reg16Rnd(registers.AFp, 0)).BYTES("08").BYTES("dd08").BYTES("fd08"))                  // ex af,af'
	addTest(tb().SETUP(reg16Rnd(registers.DE, 0), reg16Rnd(registers.HL, 1)).EXPECTED(reg16Rnd(registers.DE, 1), reg16Rnd(registers.HL, 0)).BYTES("eb").BYTES("ddeb").BYTES("fdeb"))                    // ex de,hl
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 0), reg16Rnd(registers.HL, 1)).EXPECTED(memWordRnd(addr, 1), reg16Rnd(registers.HL, 0)).BYTES("e3"))   // ex (sp),hl
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 0), reg16Rnd(registers.IX, 1)).EXPECTED(memWordRnd(addr, 1), reg16Rnd(registers.IX, 0)).BYTES("dde3")) // ex (sp),ix
	addTest(tb().GetAddr(2, &addr).SETUP(reg16Lit(registers.SP, addr.AsWord()), memWordRnd(addr, 0), reg16Rnd(registers.IY, 1)).EXPECTED(memWordRnd(addr, 1), reg16Rnd(registers.IY, 0)).BYTES("fde3")) // ex (sp),iy
	addTest(tb().
		SETUP(reg16Rnd(registers.BC, 0), reg16Rnd(registers.DE, 1), reg16Rnd(registers.HL, 2), reg16Rnd(registers.BCp, 3), reg16Rnd(registers.DEp, 4), reg16Rnd(registers.HLp, 5)).
		EXPECTED(reg16Rnd(registers.BC, 3), reg16Rnd(registers.DE, 4), reg16Rnd(registers.HL, 5), reg16Rnd(registers.BCp, 0), reg16Rnd(registers.DEp, 1), reg16Rnd(registers.HLp, 2)).
		BYTES("d9").BYTES("ddd9").BYTES("fdd9")) // exx
}

func blockLd() {
	type bytes struct {
		a      z80.Byte
		hl, de z80.Word
		bc     z80.Word
		bytes  []z80.Byte
		flgs   theFlags
	}
	// Flags settings
	// ldi,ldir,ldd,lddr affect flags based on A and last transferred byte, test all combinations
	var mask byte = 0x3e
	cases := []bytes{
		{a: 0x00, hl: 0x1000, de: 0x2000, bc: 0x0002, bytes: []z80.Byte{0x01, 0x02}, flgs: theFlags{b5: false, b3: false, pv: true}},
		{a: 0x00, hl: 0x1000, de: 0x2000, bc: 0x0001, bytes: []z80.Byte{0x01, 0x02}, flgs: theFlags{b5: false, b3: false, pv: false}},
		{a: 0x01, hl: 0x1000, de: 0x2000, bc: 0x0001, bytes: []z80.Byte{0x09, 0x02}, flgs: theFlags{b5: true, b3: true, pv: false}},
		{a: 0x08, hl: 0x1000, de: 0x2000, bc: 0x0001, bytes: []z80.Byte{0x02, 0x02}, flgs: theFlags{b5: true, b3: true, pv: false}},
	}
	for _, cse := range cases {
		addTest(tb().
			SETUP(reg16Lit(registers.HL, cse.hl), reg16Lit(registers.DE, cse.de), reg16Lit(registers.BC, cse.bc), reg8Lit(registers.A, cse.a), memWrite(cse.hl.AsAddress(), cse.bytes), memRandomise(cse.de.AsAddress(), cse.bytes), invertFlags(mask, cse.flgs)).
			EXPECTED(reg16Lit(registers.HL, cse.hl+1), reg16Lit(registers.DE, cse.de+1), reg16Lit(registers.BC, cse.bc-1), memByteLit(cse.de.AsAddress(), cse.bytes[0]), setupFlags(mask, cse.flgs)).
			BYTES("eda0")) // ldi
		addTest(tb().
			SETUP(reg16Lit(registers.HL, cse.hl), reg16Lit(registers.DE, cse.de), reg16Lit(registers.BC, cse.bc), reg8Lit(registers.A, cse.a), memWrite(cse.hl.AsAddress(), cse.bytes), memRandomise(cse.de.AsAddress(), cse.bytes), invertFlags(mask, cse.flgs)).
			EXPECTED(reg16Lit(registers.HL, cse.hl-1), reg16Lit(registers.DE, cse.de-1), reg16Lit(registers.BC, cse.bc-1), memByteLit(cse.de.AsAddress(), cse.bytes[0]), setupFlags(mask, cse.flgs)).
			BYTES("eda8")) // ldd
	}
	cases = []bytes{
		{a: 0x00, hl: 0x1000, de: 0x2000, bc: 0x0002, bytes: []z80.Byte{0x02, 0x01}, flgs: theFlags{b5: false, b3: false}},
		{a: 0x00, hl: 0x1000, de: 0x2000, bc: 0x0001, bytes: []z80.Byte{0x01}, flgs: theFlags{b5: false, b3: false}},
		{a: 0x01, hl: 0x1000, de: 0x2000, bc: 0x0003, bytes: []z80.Byte{0x12, 0xff, 0x09}, flgs: theFlags{b5: true, b3: true}},
		{a: 0x08, hl: 0x1000, de: 0x2000, bc: 0x0003, bytes: []z80.Byte{0xfe, 0x36, 0x02}, flgs: theFlags{b5: true, b3: true}},
	}
	for _, cse := range cases {
		addTest(tb().
			SETUP(reg16Lit(registers.HL, cse.hl), reg16Lit(registers.DE, cse.de), reg16Lit(registers.BC, cse.bc), reg8Lit(registers.A, cse.a), memWrite(cse.hl.AsAddress(), cse.bytes), memRandomise(cse.de.AsAddress(), cse.bytes), invertFlags(mask, cse.flgs)).
			EXPECTED(reg16Lit(registers.HL, cse.hl+cse.bc), reg16Lit(registers.DE, cse.de+cse.bc), reg16Lit(registers.BC, 0), memWrite(cse.de.AsAddress(), cse.bytes), setupFlags(mask, cse.flgs)).
			BYTES("edb0")) // ldir
		addTest(tb().
			SETUP(reg16Lit(registers.HL, cse.hl), reg16Lit(registers.DE, cse.de), reg16Lit(registers.BC, cse.bc), reg8Lit(registers.A, cse.a), memWriteRev(cse.hl.AsAddress(), cse.bytes), memRandomiseRev(cse.de.AsAddress(), cse.bytes), invertFlags(mask, cse.flgs)).
			EXPECTED(reg16Lit(registers.HL, cse.hl-cse.bc), reg16Lit(registers.DE, cse.de-cse.bc), reg16Lit(registers.BC, 0), memWriteRev(cse.de.AsAddress(), cse.bytes), setupFlags(mask, cse.flgs)).
			BYTES("edb8")) // lddr
	}
}

func blockCp() {
	//dsmTest([]z80.Byte{0xed, 0xa1}, "cpi"),
	//	dsmTest([]z80.Byte{0xed, 0xb1}, "cpir"),
	//	dsmTest([]z80.Byte{0xed, 0xa9}, "cpd"),
	//	dsmTest([]z80.Byte{0xed, 0xb9}, "cpdr"),
}

func addTest(t *asmExecuteTestCase) {
	asmExecuteTestCases = append(asmExecuteTestCases, asmExecuteTestCase{t.rnd, t.bytes, t.setup, t.expected})
}

func tb() *asmExecuteTestCase {
	return &asmExecuteTestCase{rnd: randoms()}
}

func (t *asmExecuteTestCase) SETUP(mods ...testSnapshotModifier) *asmExecuteTestCase {
	t.setup = mods
	return t
}
func (t *asmExecuteTestCase) EXPECTED(mods ...testSnapshotModifier) *asmExecuteTestCase {
	t.expected = mods
	return t
}
func (t *asmExecuteTestCase) BYTES(bytes string) *asmExecuteTestCase {
	// Replace rnd references
	more := true
	for more {
		i := strings.Index(bytes, "n")
		if more = i != -1; more {
			idx := core.Must1(strconv.Atoi, string(bytes[i+1]))
			bytes = strings.Replace(bytes, bytes[i:i+2], t.rnd.by[idx].String(), 1)
		}
	}
	more = true
	for more {
		i := strings.Index(bytes, "w")
		if more = i != -1; more {
			idx := core.Must1(strconv.Atoi, string(bytes[i+1]))
			bytes = strings.Replace(bytes, bytes[i:i+2], t.rnd.wd[idx].Lo().String(), 1)
			bytes = strings.Replace(bytes, bytes[i+2:i+4], t.rnd.wd[idx].Hi().String(), 1)
		}
	}
	// Convert hex string to bytes
	byteSlice := core.Must1(hex.DecodeString, bytes)
	t.bytes = append(t.bytes, z80.ToZ80(byteSlice))
	return t
}

// Get a random value
func (t *asmExecuteTestCase) Get8(i int, val *z80.Byte) *asmExecuteTestCase {
	*val = t.rnd.by[i]
	return t
}
func (t *asmExecuteTestCase) Get16(i int, val *z80.Word) *asmExecuteTestCase {
	*val = t.rnd.wd[i]
	return t
}
func (t *asmExecuteTestCase) GetAddr(i int, val *z80.Address) *asmExecuteTestCase {
	*val = t.rnd.wd[i].AsAddress()
	return t
}

func formatBytes(bytes []z80.Byte) string {
	text := strings.Builder{}
	for _, by := range bytes {
		text.WriteString(by.String())
	}
	return text.String()
}

// Random values needed for test
type rands struct {
	by []z80.Byte
	wd []z80.Word
}

func randoms() (rnd rands) {
	for i := 0; i < 6; i++ {
		rnd.by = append(rnd.by, randomByte(rnd.by...))
		rnd.wd = append(rnd.wd, randomWord(rnd.wd...))
	}
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Random Snapshot modifiers
// ----------------------------------------------------------------------------------------------------------------------------
type testSnapshotModifier func(snap snapshot.Snapshot, rnd rands)

func (r rands) String() string {
	var items []string
	for i, by := range r.by {
		items = append(items, fmt.Sprintf("B%d=%s", i, by.String()))
	}
	for i, wd := range r.wd {
		items = append(items, fmt.Sprintf("W%d=%s", i, wd.String()))
	}
	return strings.Join(items, ",")
}

// Set reg to rand val (at index)
func reg8Rnd(r registers.Reg8, idx int) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		setReg8(r, rnd.by[idx])(snap)
	}
}

// Set reg to literal value
func reg8Lit(r registers.Reg8, val z80.Byte) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		setReg8(r, val)(snap)
	}
}

func reg16Rnd(r registers.Reg16, idx int) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		setReg16(r, rnd.wd[idx])(snap)
	}
}
func reg16Lit(r registers.Reg16, val z80.Word) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		setReg16(r, val)(snap)
	}
}

// Set memory location to rand val (at index)
func memByteRnd(addr z80.Address, idx int) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		writeMem(addr, rnd.by[idx])(snap)
	}
}

// Set memory location to literal value
func memByteLit(addr z80.Address, val z80.Byte) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		writeMem(addr, val)(snap)
	}
}

func memWordRnd(addr z80.Address, idx int) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		writeMem(addr, rnd.wd[idx].Lo())(snap)
		writeMem(addr+1, rnd.wd[idx].Hi())(snap)
	}
}

func memWrite(addr z80.Address, bytes []z80.Byte) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		for i, by := range bytes {
			writeMem(addr.Offset(i), by)(snap)
		}
	}
}
func memRandomise(addr z80.Address, bytes []z80.Byte) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		for i := range bytes {
			writeMem(addr.Offset(i), randomByte())(snap)
		}
	}
}
func memWriteRev(addr z80.Address, bytes []z80.Byte) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		for i, by := range bytes {
			writeMem(addr.Offset(-i), by)(snap)
		}
	}
}
func memRandomiseRev(addr z80.Address, bytes []z80.Byte) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		for i := range bytes {
			writeMem(addr.Offset(-i), randomByte())(snap)
		}
	}
}

// Set flags
type theFlags struct {
	c, n, pv, b3, h, b5, z, s bool
}

func setupFlags(mask byte, f theFlags) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		if mask&0x01 != 0 {
			setFlag(flags.C, f.c)(snap)
		}
		if mask&0x02 != 0 {
			setFlag(flags.N, f.n)(snap)
		}
		if mask&0x04 != 0 {
			setFlag(flags.PV, f.pv)(snap)
		}
		if mask&0x08 != 0 {
			setFlag(flags.B3, f.b3)(snap)
		}
		if mask&0x10 != 0 {
			setFlag(flags.H, f.h)(snap)
		}
		if mask&0x20 != 0 {
			setFlag(flags.B5, f.b5)(snap)
		}
		if mask&0x40 != 0 {
			setFlag(flags.Z, f.z)(snap)
		}
		if mask&0x80 != 0 {
			setFlag(flags.S, f.s)(snap)
		}
	}
}
func invertFlags(mask byte, f theFlags) testSnapshotModifier {
	var newFlags theFlags
	newFlags.c = !f.c
	newFlags.n = !f.n
	newFlags.pv = !f.pv
	newFlags.b3 = !f.b3
	newFlags.h = !f.h
	newFlags.b5 = !f.b5
	newFlags.z = !f.z
	newFlags.s = !f.s
	return setupFlags(mask, newFlags)
}
func enableInterruptsT(val bool) testSnapshotModifier {
	return func(snap snapshot.Snapshot, rnd rands) {
		enableInterrupts(val)(snap)
	}
}
