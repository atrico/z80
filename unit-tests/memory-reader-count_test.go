package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"testing"
)

func Test_MemoryReader_Count_PeekByte(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.PeekByte(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_ReadByte(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_PeekWord(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWord(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_PeekWordBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordBigEndian(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_ReadWord(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Word], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWord(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_ReadWordBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Word], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWordBigEndian(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_PeekAddress(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddress(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_PeekAddressBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressBigEndian(),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_ReadAddress(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Address], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.ReadAddress(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_ReadAddressBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Address], 0, count/2)
	for reader.More() {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.ReadAddressBigEndian(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_ReadBlockWithCount(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	const blockCount = 6
	mem.WriteWord(start, z80.Word(blockCount))
	reader := mem.ReaderCount(start, end)

	// Act
	result := reader.ReadBlockWithCount()
	final := reader.CurrentAddress()

	// Assert
	assert.That(t, len(result), is.EqualTo(blockCount), "Size of block")
	for i, by := range result {
		assert.That(t, by, is.EqualTo(mem.ReadByte(start.Offset(2+i))), fmt.Sprintf("Read %d", i))
	}
	assert.That(t, final, is.EqualTo(start.Offset(2+blockCount)), "Final address")
	expBuffer := mem.ReadBytes(start, start.Offset(2+blockCount))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_ReadBytes(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	const blockCount = count / 2
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[[]z80.Byte], 0, count/blockCount)
	for reader.More() {
		results = append(results, readResult[[]z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadBytes(blockCount),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/blockCount), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.ListEqualTo(mem.ReadBytes(expAddr, expAddr.Offset(blockCount))), fmt.Sprintf("Read %d: value", i))
		expAddr += blockCount
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_ReadWords(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 16
	end := start.Offset(count)
	const blockCount = count / 4
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[[]z80.Word], 0, count/blockCount/2)
	for reader.More() {
		results = append(results, readResult[[]z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWords(blockCount),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/blockCount/2), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.ListEqualTo(memReadWords(mem, expAddr, blockCount)), fmt.Sprintf("Read %d: value", i))
		expAddr += blockCount * 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_ReadAllBytes(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	result := readResult[[]z80.Byte]{
		reader.CurrentAddress(),
		reader.ReadAllBytes(),
	}

	// Assert
	assert.That(t, result.addr, is.EqualTo(start), "Read: address")
	assert.That(t, result.value, is.ListEqualTo(mem.ReadBytes(start, start.Offset(count))), "Read: value")
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_ReadAllWords(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 16
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	result := readResult[[]z80.Word]{
		reader.CurrentAddress(),
		reader.ReadAllWords(),
	}

	// Assert
	assert.That(t, result.addr, is.EqualTo(start), "Read: address")
	assert.That(t, result.value, is.ListEqualTo(memReadWords(mem, start, count/2)), "Read: value")
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_ResetByteBuffer(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
		if len(results) == count/2 {
			reader.ResetByteBuffer()
		}
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start.Offset(count/2), start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_SkipBytes(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Byte], 0, count/2)
	reader.SkipBytes(count / 2)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count/2), "Number of reads")
	expAddr := start + count/2
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start.Offset(count/2), start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_Count_PeekByteAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Byte], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.PeekByteAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_PeekWordAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_PeekWordBigEndianAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Word], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordBigEndianAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_PeekAddressAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr.Offset(i)).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_PeekAddressBigEndianAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	results := make([]readResult[z80.Address], 0, count)
	for i := 0; i < count; i++ {
		results = append(results, readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressBigEndianAt(start.Offset(i)),
		})
	}

	// Assert
	assert.That(t, reader.More(), is.True, "Still more")
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr.Offset(i)).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_Count_Reset(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	end := start.Offset(count)
	reader := mem.ReaderCount(start, end)

	// Act
	// Read some
	reader.ReadWord()
	reader.ReadWord()
	reader.ReadWord()
	reader.Reset()
	results := make([]readResult[z80.Byte], 0, count)
	for reader.More() {
		results = append(results, readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		})
	}

	// Assert
	assert.That(t, len(results), is.EqualTo(count), "Number of reads")
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}
