package unit_tests

import (
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/interrupt"
	"gitlab.com/atrico/z80/snapshot"
	"gitlab.com/atrico/z80/unit-tests/flags"
	"gitlab.com/atrico/z80/unit-tests/registers"
	"testing"
)

func createBlankSnapshot(modifiers ...snapshotModifier) (snap snapshot.Snapshot) {
	mem, _ := z80.NewMemory(make([]z80.Byte, 0), make([]z80.Byte, 0x10000)) // All RAM
	snap = snapshot.NewSnapshot(
		mem,
		z80.Address(0x8000), // PC
		z80.Address(0xc000), // SP
		z80.GeneralRegisters{},
		z80.GeneralRegisters{},
		z80.IndexRegisters{},
		z80.SpecialRegisters{},
		interrupt.IM0,
		false, // Interrupts disabled
	)
	for _, mod := range modifiers {
		mod(snap)
	}
	return
}

func assertSnapshot(t *testing.T, snap snapshot.Snapshot, expected snapshot.Snapshot) {
	// PC
	assert.That(t, snap.ProgramCounter().Get(), is.EqualTo(expected.ProgramCounter().Get()), "PC")
	// Regs
	{
		// Accumulator
		assert.That(t, snap.Registers8Bit().A.Get(), is.EqualTo(expected.Registers8Bit().A.Get()), "A")
		// 16 bits
		assert.That(t, snap.Registers16Bit().BC.Get(), is.EqualTo(expected.Registers16Bit().BC.Get()), "BC")
		assert.That(t, snap.Registers16Bit().DE.Get(), is.EqualTo(expected.Registers16Bit().DE.Get()), "DE")
		assert.That(t, snap.Registers16Bit().HL.Get(), is.EqualTo(expected.Registers16Bit().HL.Get()), "HL")
		// Flags
		assert.That(t, snap.Flags().C.Get(), is.EqualTo(expected.Flags().C.Get()), "Flag: C")
		assert.That(t, snap.Flags().N.Get(), is.EqualTo(expected.Flags().N.Get()), "Flag: N")
		assert.That(t, snap.Flags().PV.Get(), is.EqualTo(expected.Flags().PV.Get()), "Flag: PV")
		assert.That(t, snap.Flags().B3.Get(), is.EqualTo(expected.Flags().B3.Get()), "Flag: B3")
		assert.That(t, snap.Flags().H.Get(), is.EqualTo(expected.Flags().H.Get()), "Flag: H")
		assert.That(t, snap.Flags().B5.Get(), is.EqualTo(expected.Flags().B5.Get()), "Flag: B5")
		assert.That(t, snap.Flags().Z.Get(), is.EqualTo(expected.Flags().Z.Get()), "Flag: Z")
		assert.That(t, snap.Flags().S.Get(), is.EqualTo(expected.Flags().S.Get()), "Flag: S")

	}
	// SP
	assert.That(t, snap.StackPointer().Get(), is.EqualTo(expected.StackPointer().Get()), "SP")
	// Index
	assert.That(t, snap.Registers16Bit().IX.Get(), is.EqualTo(expected.Registers16Bit().IX.Get()), "IX")
	assert.That(t, snap.Registers16Bit().IY.Get(), is.EqualTo(expected.Registers16Bit().IY.Get()), "IY")
	// Alt regs
	{
		// Accumulator
		assert.That(t, snap.Registers8Bit().Ap.Get(), is.EqualTo(expected.Registers8Bit().Ap.Get()), "A'")
		// 16 bits
		assert.That(t, snap.Registers16Bit().BCp.Get(), is.EqualTo(expected.Registers16Bit().BCp.Get()), "BC'")
		assert.That(t, snap.Registers16Bit().DEp.Get(), is.EqualTo(expected.Registers16Bit().DEp.Get()), "DE'")
		assert.That(t, snap.Registers16Bit().HLp.Get(), is.EqualTo(expected.Registers16Bit().HLp.Get()), "HL'")
		// F
		assert.That(t, snap.Registers8Bit().Fp.Get(), is.EqualTo(expected.Registers8Bit().Fp.Get()), "F'")
	}
	// Special
	assert.That(t, snap.Registers8Bit().I.Get(), is.EqualTo(expected.Registers8Bit().I.Get()), "I")
	assert.That(t, snap.Registers8Bit().R.Get(), is.EqualTo(expected.Registers8Bit().R.Get()), "R")
	// Interrupts
	assert.That(t, snap.InterruptMode().Get(), is.EqualTo(expected.InterruptMode().Get()), "Interrupt mode")
	assert.That(t, snap.InterruptsEnabled().Get(), is.EqualTo(expected.InterruptsEnabled().Get()), "Interrupt enabled")
	// Memory
	for i := 0; i < 0x10000; i++ {
		addr := z80.Address(i)
		assert.That(t, snap.ReadByte(addr), is.EqualTo(expected.ReadByte(addr)), "Memory: %s", addr.String())
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Snapshot modifiers
// ----------------------------------------------------------------------------------------------------------------------------
type snapshotModifier func(snap snapshot.Snapshot)

func _getReg8Ref(snap snapshot.Snapshot, r registers.Reg8) z80.ByteRef {
	switch r {
	case registers.B:
		return snap.Registers8Bit().B
	case registers.C:
		return snap.Registers8Bit().C
	case registers.D:
		return snap.Registers8Bit().D
	case registers.E:
		return snap.Registers8Bit().E
	case registers.H:
		return snap.Registers8Bit().H
	case registers.L:
		return snap.Registers8Bit().L
	case registers.A:
		return snap.Registers8Bit().A
	case registers.F:
		return snap.Registers8Bit().F
	case registers.IXh:
		return snap.Registers8Bit().IXh
	case registers.IXl:
		return snap.Registers8Bit().IXl
	case registers.IYh:
		return snap.Registers8Bit().IYh
	case registers.IYl:
		return snap.Registers8Bit().IYl
	case registers.I:
		return snap.Registers8Bit().I
	case registers.R:
		return snap.Registers8Bit().R
	}
	panic("unrecognised reg")
}

func setReg8(r registers.Reg8, val z80.Byte) snapshotModifier {
	return func(snap snapshot.Snapshot) {
		ref := _getReg8Ref(snap, r)
		ref.Set(val)
	}
}

func incReg8(r registers.Reg8) snapshotModifier {
	return func(snap snapshot.Snapshot) {
		ref := _getReg8Ref(snap, r)
		ref.Set((ref.Get() + 1) & 0xff)
	}
}

func setReg16(r registers.Reg16, val z80.Word) snapshotModifier {
	return func(snap snapshot.Snapshot) {
		var ref z80.WordRef
		switch r {
		case registers.BC:
			ref = snap.Registers16Bit().BC
		case registers.DE:
			ref = snap.Registers16Bit().DE
		case registers.HL:
			ref = snap.Registers16Bit().HL
		case registers.AF:
			ref = snap.Registers16Bit().AF
		case registers.BCp:
			ref = snap.Registers16Bit().BCp
		case registers.DEp:
			ref = snap.Registers16Bit().DEp
		case registers.HLp:
			ref = snap.Registers16Bit().HLp
		case registers.AFp:
			ref = snap.Registers16Bit().AFp
		case registers.IX:
			ref = snap.Registers16Bit().IX
		case registers.IY:
			ref = snap.Registers16Bit().IY
		case registers.SP:
			ref = snap.StackPointer().AsWord()
		}
		ref.Set(val)
	}
}

func writeMem(addr z80.Address, values ...z80.Byte) snapshotModifier {
	return func(snap snapshot.Snapshot) {
		for o, val := range values {
			snap.WriteByte(addr.Offset(o), val)
		}
	}
}

func incPC(delta int) snapshotModifier {
	return func(snap snapshot.Snapshot) {
		ref := snap.ProgramCounter()
		ref.Set(ref.Get().Offset(delta))
	}
}

func setFlag(f flags.Flag, val bool) snapshotModifier {
	return func(snap snapshot.Snapshot) {
		ref := _getFlagRef(snap, f)
		ref.Set(val)
	}
}

func _getFlagRef(snap snapshot.Snapshot, f flags.Flag) z80.BitRef {
	switch f {
	case flags.C:
		return snap.Flags().C
	case flags.N:
		return snap.Flags().N
	case flags.PV:
		return snap.Flags().PV
	case flags.B3:
		return snap.Flags().B3
	case flags.H:
		return snap.Flags().H
	case flags.B5:
		return snap.Flags().B5
	case flags.Z:
		return snap.Flags().Z
	case flags.S:
		return snap.Flags().S
	}
	panic("unrecognised flag")
}

func enableInterrupts(val bool) snapshotModifier {
	return func(snap snapshot.Snapshot) {
		ref := snap.InterruptsEnabled()
		ref.Set(val)
	}
}
