package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"testing"
)

type readResult[T any] struct {
	addr  z80.Address
	value T
}

func Test_MemoryReader_PeekByte(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Byte], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.PeekByte(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_ReadByte(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Byte], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_PeekWord(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Word], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWord(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_PeekWordBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Word], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordBigEndian(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr)), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_ReadWord(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Word], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWord(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count*2))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_ReadWordBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Word], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWordBigEndian(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count*2))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_PeekAddress(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Address], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddress(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_PeekAddressBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Address], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressBigEndian(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_ReadAddress(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Address], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.ReadAddress(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count*2))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_ReadAddressBigEndian(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Address], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.ReadAddressBigEndian(),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr).AsAddress()), fmt.Sprintf("Read %d: value", i))
		expAddr += 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count*2))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_ReadBlockWithCount(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const blockCount = 6
	mem.WriteWord(start, z80.Word(blockCount))
	reader := mem.Reader(start)

	// Act
	result := reader.ReadBlockWithCount()
	final := reader.CurrentAddress()

	// Assert
	assert.That(t, len(result), is.EqualTo(blockCount), "Size of block")
	for i, by := range result {
		assert.That(t, by, is.EqualTo(mem.ReadByte(start.Offset(2+i))), fmt.Sprintf("Read %d", i))
	}
	assert.That(t, final, is.EqualTo(start.Offset(2+blockCount)), "Final address")
	expBuffer := mem.ReadBytes(start, start.Offset(2+blockCount))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_ReadBytes(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	const blockCount = count / 2
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[[]z80.Byte], count/blockCount)
	for i := 0; i < count/blockCount; i++ {
		results[i] = readResult[[]z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadBytes(blockCount),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.ListEqualTo(mem.ReadBytes(expAddr, expAddr.Offset(blockCount))), fmt.Sprintf("Read %d: value", i))
		expAddr += blockCount
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_ReadWords(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 16
	const blockCount = count / 4
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[[]z80.Word], count/blockCount/2)
	for i := 0; i < count/blockCount/2; i++ {
		results[i] = readResult[[]z80.Word]{
			reader.CurrentAddress(),
			reader.ReadWords(blockCount),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.ListEqualTo(memReadWords(mem, expAddr, blockCount)), fmt.Sprintf("Read %d: value", i))
		expAddr += blockCount * 2
	}
	expBuffer := mem.ReadBytes(start, start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_ResetByteBuffer(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Byte], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		}
		if i == count/2 {
			reader.ResetByteBuffer()
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start.Offset(count/2+1), start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_SkipBytes(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Byte], count/2)
	reader.SkipBytes(count / 2)
	for i := 0; i < count/2; i++ {
		results[i] = readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.ReadByte(),
		}
	}

	// Assert
	expAddr := start + count/2
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr)), fmt.Sprintf("Read %d: value", i))
		expAddr++
	}
	expBuffer := mem.ReadBytes(start.Offset(count/2), start.Offset(count))
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer")
}

func Test_MemoryReader_PeekByteAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Byte], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Byte]{
			reader.CurrentAddress(),
			reader.PeekByteAt(start.Offset(i)),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadByte(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_PeekWordAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Word], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordAt(start.Offset(i)),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_PeekWordBigEndianAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Word], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Word]{
			reader.CurrentAddress(),
			reader.PeekWordBigEndianAt(start.Offset(i)),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr.Offset(i))), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_PeekAddressAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Address], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressAt(start.Offset(i)),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWord(expAddr.Offset(i)).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func Test_MemoryReader_PeekAddressBigEndianAt(t *testing.T) {
	// Arrange
	mem, _ := createRandomMemory(rom)
	start := z80.Address(0x2000)
	const count = 10
	reader := mem.Reader(start)

	// Act
	results := make([]readResult[z80.Address], count)
	for i := 0; i < count; i++ {
		results[i] = readResult[z80.Address]{
			reader.CurrentAddress(),
			reader.PeekAddressBigEndianAt(start.Offset(i)),
		}
	}

	// Assert
	expAddr := start
	for i, result := range results {
		assert.That(t, result.addr, is.EqualTo(expAddr), fmt.Sprintf("Read %d: address", i))
		assert.That(t, result.value, is.EqualTo(mem.ReadWordBigEndian(expAddr.Offset(i)).AsAddress()), fmt.Sprintf("Read %d: value", i))
	}
	var expBuffer []z80.Byte
	assert.That(t, reader.ByteBuffer(), is.ListEqualTo(expBuffer), "Byte buffer should be empty")
}

func memReadWords(mem z80.Memory, addr z80.Address, count int) (ret []z80.Word) {
	for i := 0; i < count; i++ {
		ret = append(ret, mem.ReadWord(addr.Offset(i*2)))
	}
	return
}
