package flags

type Flag byte

const (
	C  Flag = iota // Carry (bit 0)
	N  Flag = iota // Add/Subtract (bit 1)
	PV Flag = iota // Parity/Overflow (bit 2)
	B3 Flag = iota // bit 3 of the result (bit 3)
	H  Flag = iota // Half carry (bit 4)
	B5 Flag = iota // bit 5 of the result (bit 5)
	Z  Flag = iota // Zero (bit 6)
	S  Flag = iota // Sign (bit 7)
)
