package unit_tests

import (
	"fmt"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler"
	"strings"
	"testing"
)

func Test_Disassembler_Data(t *testing.T) {
	for name, testCase := range asmDataTestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase assemblyCalcTestCase) {
				// Arrange
				dis := make([]disassembler.Disassembler, 2)
				dis[0], dis[1] = createDisassembler(testCase.bytes, func(annot z80.MemoryAnnotations) {
					annot.Update(z80.Address(0), z80.MemoryAnnotation{Data: len(testCase.bytes), DataFormat: testCase.format})
				})

				// Act
				asm := make([][]disassembler.DisassemblerLine, 2)
				for i := 0; i < 2; i++ {
					for dis[i].Scan(false) {
						asm[i] = append(asm[i], dis[i].Current())
						t.Log(dis[i].Current().Bytes, dis[i].Current().Op.Display(MockFormatter{}))
					}
				}

				// Assert
				for i := 0; i < 2; i++ {
					for l, r := range testCase.result {
						assert.That(t, asm[i][l].Op.Display(MockFormatter{}), is.EqualTo(r.line), "instruction: %d,%d", i, l)
						assert.That(t, asm[i][l].Bytes, is.EqualTo(r.bytes), "bytes: %d,%d", i, l)

					}
				}
			}(testCase)
		})
	}
}

type assemblyCalcTestCase struct {
	bytes  []z80.Byte
	format string
	result []asmTCRes
}

type asmTCRes struct {
	bytes []z80.Byte
	line  string
}

var asmDataTestCases = map[string]assemblyCalcTestCase{}

func makeAsmDataTC(bytes []z80.Byte, format string, results ...asmTCRes) assemblyCalcTestCase {
	return assemblyCalcTestCase{bytes, format, results}
}

func init() {
	// Bytes
	bytes, expected := randomBytes(10)
	asmDataTestCases["Bytes"] = makeAsmDataTC(bytes, "bytes", asmTCRes{bytes, fmt.Sprintf("DATA: %s", expected)})
	// Words
	bytes, expected = randomWords(5)
	asmDataTestCases["Words"] = makeAsmDataTC(bytes, "words", asmTCRes{bytes, fmt.Sprintf("DATA: %s", expected)})
}

func randomBytes(count int) (ret []z80.Byte, str string) {
	ret = make([]z80.Byte, count)
	retS := make([]string, count)
	for i := 0; i < count; i++ {
		ret[i] = z80.Byte(rg.Byte())
		retS[i] = ret[i].String()
	}
	str = strings.Join(retS, ",")
	return
}

func randomWords(count int) (ret []z80.Byte, str string) {
	ret = make([]z80.Byte, count*2)
	retS := make([]string, count)
	for i := 0; i < count; i++ {
		word := z80.Word(rg.IntUpto(0x10000))
		ret[2*i] = word.Lo()
		ret[2*i+1] = word.Hi()
		retS[i] = word.String()
	}
	str = strings.Join(retS, ",")
	return
}
