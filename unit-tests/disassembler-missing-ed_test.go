package unit_tests

import (
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"slices"
	"testing"
)

func Test_Disassembler_MissingED(t *testing.T) {
	for _, testCase := range assemblyMissingEdTestCases {
		t.Run(testCase.String(), func(t *testing.T) {
			func(testCase z80.Byte) {
				// Arrange
				disassembler, disassemblerWAA := createDisassembler(append([]z80.Byte{0xed}, testCase))

				// Act
				assert.That(t, disassembler.Scan(false), is.True, "Scan1")
				assembly1 := disassembler.Current()
				assert.That(t, disassembler.Scan(false), is.True, "Scan2")
				assembly2 := disassembler.Current()
				t.Log(assembly1.Bytes, assembly1.Op.Display(MockFormatter{}))
				t.Log(assembly2.Bytes, assembly2.Op.Display(MockFormatter{}))
				assert.That(t, disassemblerWAA.Scan(false), is.True, "ScanWAA1")
				assemblyWAA1 := disassemblerWAA.Current()
				assert.That(t, disassemblerWAA.Scan(false), is.True, "ScanWAA2")
				assemblyWAA2 := disassemblerWAA.Current()
				t.Log(assemblyWAA1.Bytes, assemblyWAA1.Op.Display(MockFormatter{}))
				t.Log(assemblyWAA2.Bytes, assemblyWAA2.Op.Display(MockFormatter{}))

				// Assert
				assert.That(t, assembly1.Op.Display(MockFormatter{}), is.EqualTo("nop"), "instruction1")
				assert.That(t, assembly1.Bytes, is.EqualTo([]z80.Byte{0xed}), "bytes1")
				assert.That(t, assembly2.Op.Display(MockFormatter{}), is.EqualTo("nop"), "instruction2")
				assert.That(t, assembly2.Bytes, is.EqualTo([]z80.Byte{testCase}), "bytes2")
				assert.That(t, assemblyWAA1.Op.Display(MockFormatter{}), is.EqualTo("nop"), "instructionWAA1")
				assert.That(t, assemblyWAA1.Bytes, is.EqualTo([]z80.Byte{0xed}), "bytesWAA1")
				assert.That(t, assemblyWAA2.Op.Display(MockFormatter{}), is.EqualTo("nop"), "instructionWAA2")
				assert.That(t, assemblyWAA2.Bytes, is.EqualTo([]z80.Byte{testCase}), "bytesWAA2")
			}(testCase)
		})
	}
}

var assemblyMissingEdTestCases []z80.Byte

func init() {
	assemblyMissingEdTestCases = make([]z80.Byte, 0, 256)
	for i := 0; i < 0x100; i++ {
		by := z80.Byte(i)
		if !slices.Contains(edInstructions, by) && by != 0xed { // Double ED different case
			assemblyMissingEdTestCases = append(assemblyMissingEdTestCases, by)
		}
	}
}

var edInstructions = []z80.Byte{
	0x40,
	0x41,
	0x42,
	0x43,
	0x44,
	0x45,
	0x46,
	0x47,
	0x48,
	0x49,
	0x4a,
	0x4b,
	0x4c,
	0x4d,
	0x4e,
	0x4f,
	0x50,
	0x51,
	0x52,
	0x53,
	0x54,
	0x55,
	0x56,
	0x57,
	0x58,
	0x59,
	0x5a,
	0x5b,
	0x5c,
	0x5d,
	0x5e,
	0x5f,
	0x60,
	0x61,
	0x62,
	0x64,
	0x65,
	0x66,
	0x67,
	0x68,
	0x69,
	0x6a,
	0x6c,
	0x6d,
	0x6e,
	0x6f,
	0x72,
	0x73,
	0x74,
	0x75,
	0x76,
	0x78,
	0x79,
	0x7a,
	0x7b,
	0x7c,
	0x7d,
	0x7e,
	0xa0,
	0xa1,
	0xa2,
	0xa3,
	0xa8,
	0xa9,
	0xaa,
	0xab,
	0xb0,
	0xb1,
	0xb2,
	0xb3,
	0xb8,
	0xb9,
	0xba,
	0xbb,
}
