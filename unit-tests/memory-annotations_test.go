package unit_tests

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"gitlab.com/atrico/z80"
	"strings"
	"testing"
)

func Test_MemoryAnnotations_Empty(t *testing.T) {
	// Act
	annot, err := z80.NewMemoryAnnotations()

	// Assert
	assert.That[any](t, err, is.Nil, "err")
	_, ok := annot.Lookup(z80.Address(0))
	assert.That(t, ok, is.False, "lookup ok")
}

func Test_MemoryAnnotations_New(t *testing.T) {
	// Arrange
	addr1 := z80.Address(rg.IntUpto(0x10000))
	detail1 := createDetail()
	testConfig1 := createConfig(addr1, detail1)
	t.Log("\n", testConfig1)
	addr2 := z80.Address(rg.IntUpto(0x10000))
	detail2 := createDetail()
	testConfig2 := createConfig(addr2, detail2)
	t.Log("\n", testConfig2)

	// Act
	annot, err := z80.NewMemoryAnnotations(testConfig1, testConfig2)

	// Assert
	assert.That[any](t, err, is.Nil, "err")
	result1, ok1 := annot.Lookup(addr1)
	assert.That(t, ok1, is.True, "ok 1")
	assert.That(t, result1, is.DeepEqualTo(detail1), "lookup 1")
	result2, ok2 := annot.Lookup(addr2)
	assert.That(t, ok2, is.True, "ok 2")
	assert.That(t, result2, is.DeepEqualTo(detail2), "lookup 2")
}

func Test_MemoryAnnotations_AddConfig(t *testing.T) {
	// Arrange
	addr1 := z80.Address(rg.IntUpto(0x10000))
	detail1 := createDetail()
	testConfig1 := createConfig(addr1, detail1)
	t.Log("\n", testConfig1)
	addr2 := z80.Address(rg.IntUpto(0x10000))
	detail2 := createDetail()
	testConfig2 := createConfig(addr2, detail2)
	t.Log("\n", testConfig2)

	// Act
	annot, errNew := z80.NewMemoryAnnotations()
	errAdd1 := annot.AddConfig(testConfig1)
	errAdd2 := annot.AddConfig(testConfig2)

	// Assert
	assert.That[any](t, errNew, is.Nil, "err new")
	assert.That[any](t, errAdd1, is.Nil, "err add 1")
	assert.That[any](t, errAdd2, is.Nil, "err add 2")
	result1, ok1 := annot.Lookup(addr1)
	assert.That(t, ok1, is.True, "ok 1")
	assert.That(t, result1, is.DeepEqualTo(detail1), "lookup 1")
	result2, ok2 := annot.Lookup(addr2)
	assert.That(t, ok2, is.True, "ok 2")
	assert.That(t, result2, is.DeepEqualTo(detail2), "lookup 2")
}

func Test_MemoryAnnotations_UpdateNew(t *testing.T) {
	// Arrange
	addr := z80.Address(rg.IntUpto(0x10000))
	detail := createDetail()
	t.Log("\n", createConfig(addr, detail))

	// Act
	annot, errNew := z80.NewMemoryAnnotations()
	annot.Update(addr, detail)

	// Assert
	assert.That[any](t, errNew, is.Nil, "err")
	result, ok := annot.Lookup(addr)
	assert.That(t, ok, is.True, "ok")
	assert.That(t, result, is.DeepEqualTo(detail), "lookup")
}

func Test_MemoryAnnotations_UpdateExisting(t *testing.T) {
	// Arrange
	addr := z80.Address(rg.IntUpto(0x10000))
	detail1 := createDetail()
	testConfig := createConfig(addr, detail1)
	t.Log("\n", testConfig)
	detail2 := z80.MemoryAnnotation{
		Label:        "Label",
		Header:       []string{"one", "two"},
		NumberFormat: z80.NumberFormat_Address,
	}
	t.Log("\n", createConfig(addr, detail2))

	// Act
	annot, errNew := z80.NewMemoryAnnotations(testConfig)
	annot.Update(addr, detail2)

	// Assert
	assert.That[any](t, errNew, is.Nil, "err new")
	result, ok := annot.Lookup(addr)
	assert.That(t, ok, is.True, "ok")
	// The same
	assert.That(t, result.NewSection, is.EqualTo(detail1.NewSection), "NewSection")
	assert.That(t, result.Data, is.EqualTo(detail1.Data), "data")
	assert.That(t, result.DataFormat, is.EqualTo(detail1.DataFormat), "dataFormat")
	// Changed
	assert.That(t, result.Label, is.EqualTo(detail2.Label), "label")
	assert.That(t, result.NumberFormat, is.EqualTo(detail2.NumberFormat), "words")
	// Appended
	assert.That(t, result.Header, is.EqualTo(append(detail1.Header, detail2.Header...)), "Header")
}

func createDetail() (detail z80.MemoryAnnotation) {
	detail.Label = rg.Identifier()
	detail.NewSection = rg.Bool()
	for i := 0; i < rg.IntUpto(5); i++ {
		detail.Header = append(detail.Header, rg.Identifier())
	}
	detail.Data = rg.IntUpto(5)
	detail.DataFormat = rg.Identifier()
	if rg.Bool() {
		detail.NumberFormat = z80.NumberFormat_Address
	}
	return
}

func createConfig(addr z80.Address, config z80.MemoryAnnotation) testConfigImpl {
	yaml := strings.Builder{}
	yaml.WriteString(fmt.Sprintf("%s:\n", addr))
	// Update non default fields
	if config.NewSection {
		yaml.WriteString("  newSection: true\n")
	}
	if len(config.Header) > 0 {
		yaml.WriteString("  header:\n")
		for _, line := range config.Header {
			yaml.WriteString(fmt.Sprintf("    - '%s'\n", line))
		}
	}
	if config.Label != "" {
		yaml.WriteString(fmt.Sprintf("  label: '%s'\n", config.Label))
	}
	if config.Data > 0 {
		yaml.WriteString(fmt.Sprintf("  data: %d\n", config.Data))
	}
	if config.DataFormat != "" {
		yaml.WriteString(fmt.Sprintf("  dataFormat: '%s'\n", config.DataFormat))
	}
	if config.NumberFormat != z80.NumberFormat_None {
		yaml.WriteString(fmt.Sprintf("  numberFormat: %s\n", config.NumberFormat))
	}
	return testConfigImpl{[]byte(yaml.String())}
}

type testConfigImpl struct {
	bytes []byte
}

func (t testConfigImpl) String() string {
	return string(t.bytes)
}

func (t testConfigImpl) Load() (config *viper.Viper, err error) {
	return z80.ByteLoader(t.bytes, "yaml").Load()
}
