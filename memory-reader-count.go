package z80

//goland:noinspection GoStandardMethods
type MemoryReaderCount interface {
	MemoryReader
	// Any more data to read?
	More() bool
	// Read all remaining bytes
	ReadAllBytes() []Byte
	// Read all remaining bytes as words
	ReadAllWords() []Word
	// Reset the reader back to its original address/count
	Reset()
}

func (m *memory) ReaderCount(start, end Address) MemoryReaderCount {
	count := int(end - start)
	if count <= 0 {
		count += 0x10000
	}
	return &memoryReaderCount{
		base:       m.Reader(start),
		count:      count,
		startAddr:  start,
		startCount: count,
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type memoryReaderCount struct {
	base       MemoryReader
	count      int
	startAddr  Address
	startCount int
}

func (r *memoryReaderCount) CurrentAddress() Address {
	return r.base.CurrentAddress()
}

func (r *memoryReaderCount) Annotations() MemoryAnnotations {
	return r.base.Annotations()
}

func (r *memoryReaderCount) PeekByte() Byte {
	return r.base.PeekByte()
}

func (r *memoryReaderCount) PeekWord() Word {
	return r.base.PeekWord()
}
func (r *memoryReaderCount) PeekWordBigEndian() Word {
	return r.base.PeekWordBigEndian()
}

func (r *memoryReaderCount) PeekAddress() Address {
	return r.base.PeekAddress()
}
func (r *memoryReaderCount) PeekAddressBigEndian() Address {
	return r.base.PeekAddressBigEndian()
}

//goland:noinspection GoStandardMethods
func (r *memoryReaderCount) ReadByte() (by Byte) {
	by = r.base.ReadByte()
	r.count--
	return
}

func (r *memoryReaderCount) ReadWord() Word {
	return Word(r.ReadByte()) + 0x100*Word(r.ReadByte())
}
func (r *memoryReaderCount) ReadWordBigEndian() Word {
	return 0x100*Word(r.ReadByte()) + Word(r.ReadByte())
}

func (r *memoryReaderCount) ReadAddress() Address {
	return Address(r.ReadWord())
}
func (r *memoryReaderCount) ReadAddressBigEndian() Address {
	return Address(r.ReadWordBigEndian())
}

func (r *memoryReaderCount) ReadBytes(count int) []Byte {
	r.count -= count
	return r.base.ReadBytes(count)
}

func (r *memoryReaderCount) ReadWords(count int) []Word {
	r.count -= count * 2
	return r.base.ReadWords(count)
}

func (r *memoryReaderCount) ReadBlockWithCount() []Byte {
	count := int(r.ReadWord())
	return r.ReadBytes(count)
}

func (r *memoryReaderCount) ByteBuffer() []Byte {
	return r.base.ByteBuffer()
}

func (r *memoryReaderCount) ResetByteBuffer() {
	r.base.ResetByteBuffer()
}

func (r *memoryReaderCount) PeekByteAt(address Address) Byte {
	return r.base.PeekByteAt(address)
}

func (r *memoryReaderCount) PeekWordAt(address Address) Word {
	return r.base.PeekWordAt(address)
}
func (r *memoryReaderCount) PeekWordBigEndianAt(address Address) Word {
	return r.base.PeekWordBigEndianAt(address)
}

func (r *memoryReaderCount) PeekAddressAt(address Address) Address {
	return r.base.PeekAddressAt(address)
}
func (r *memoryReaderCount) PeekAddressBigEndianAt(address Address) Address {
	return r.base.PeekAddressBigEndianAt(address)
}

func (r *memoryReaderCount) ResetAddress(addr Address) {
	r.base.ResetAddress(addr)
}

func (r *memoryReaderCount) More() bool {
	return r.count > 0
}

func (r *memoryReaderCount) ReadAllBytes() []Byte {
	return r.ReadBytes(r.count)
}

func (r *memoryReaderCount) ReadAllWords() []Word {
	return r.ReadWords(r.count / 2)
}

func (r *memoryReaderCount) SkipBytes(delta int) {
	r.base.SkipBytes(delta)
	r.count -= delta
}

func (r *memoryReaderCount) Reset() {
	r.base.ResetAddress(r.startAddr)
	r.count = r.startCount
}

func (r *memoryReaderCount) ChildReader(count int) (reader MemoryReaderCount) {
	// Truncate count if not enough
	if count > r.count {
		count = r.count
	}
	reader = &memoryReaderChild{
		MemoryReaderCount: r,
		count:             count,
		startAddr:         r.CurrentAddress(),
		startCount:        count,
	}
	return
}

func (r *memoryReaderCount) Rewind(count int) {
	r.base.Rewind(count)
	r.count += count
}
