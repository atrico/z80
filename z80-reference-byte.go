package z80

type ByteRef interface {
	Ref[Byte]
}

type HiLo bool

const (
	HiByte HiLo = true
	LoByte HiLo = false
)

func NewByteRef(by *Byte) ByteRef {
	return byteRef{by}
}

func NewByteRef16(wd *Word, hiLo HiLo) ByteRef {
	return byteRef16{wd, bool(hiLo)}
}

func NewByteRefMem(mem Memory, addr Address) ByteRef {
	return byteRefMem{mem, addr}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Byte
// ----------------------------------------------------------------------------------------------------------------------------

type byteRef struct {
	by *Byte
}

func (r byteRef) Get() Byte {
	return *r.by
}

func (r byteRef) Set(val Byte) {
	*r.by = val
}

func (r byteRef) String() string {
	return r.Get().String()
}

// ----------------------------------------------------------------------------------------------------------------------------
// Part of word
// ----------------------------------------------------------------------------------------------------------------------------

type byteRef16 struct {
	wd *Word
	hi bool
}

func (r byteRef16) Get() Byte {
	if r.hi {
		return Byte(*r.wd >> 8)
	}
	return Byte(*r.wd & 0xff)
}

func (r byteRef16) Set(val Byte) {
	prev := *r.wd
	if r.hi {
		prev = (prev & 0xff) + (Word(val) << 8)
	} else {
		prev = (prev & 0xff00) + Word(val)
	}
	*r.wd = prev
}

func (r byteRef16) String() string {
	return r.Get().String()
}

// ----------------------------------------------------------------------------------------------------------------------------
// From memory
// ----------------------------------------------------------------------------------------------------------------------------

type byteRefMem struct {
	mem  Memory
	addr Address
}

func (r byteRefMem) Get() Byte {
	return r.mem.ReadByte(r.addr)
}

func (r byteRefMem) Set(val Byte) {
	r.mem.WriteByte(r.addr, val)
}

func (r byteRefMem) String() string {
	return r.Get().String()
}
