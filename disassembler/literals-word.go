package disassembler

import (
	"fmt"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type numWord struct {
	word   z80.Word
	format z80.NumberFormatType
	offset int
}

func (p *numWord) ReferencesAddress() (refs []z80.Address) {
	if p.format == z80.NumberFormat_Address {
		refs = []z80.Address{p.word.AsAddress().Offset(p.offset)}
	}
	return
}

func (p *numWord) Display(formatter format.Formatter) (text string) {
	off := format.Offset(p.offset)
	switch p.format {
	case z80.NumberFormat_Address:
		text = formatter.AddressRef(p.word.AsAddress(), off)
	case z80.NumberFormat_Mask:
		text = formatter.Highlighter().Number(fmt.Sprintf("%016b", int(p.word)))
	case z80.NumberFormat_Signed:
		val := p.word.TwosComplement()
		sgn := ""
		if val >= 0 {
			sgn = "+"
		}
		text = formatter.Highlighter().Number(fmt.Sprintf("%s%d", sgn, val))
	default:
		text = formatter.Word(p.word, off)
	}
	return
}

func literalWord() opPartT[z80.Word] { return &numWord{} }

func (p *numWord) Read(reader z80.MemoryReaderCount) {
	p.word = reader.ReadWord()
}

func (p *numWord) Get(snapshot.Snapshot) z80.Word {
	return p.word
}

func (p *numWord) Set(z80.Word, snapshot.Snapshot) {
	panic(changeLiteralError)
}

func (p *numWord) ModifyParameter(format z80.NumberFormatType, offset int) {
	p.format = format
	p.offset = offset
}
