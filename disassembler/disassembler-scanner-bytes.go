package disassembler

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/errors"

	"strings"
)

// ----------------------------------------------------------------------------------------------------------------------------
// Bytes
// ----------------------------------------------------------------------------------------------------------------------------
const maxWidth = 32

// True = data in words, False = data in bytes
type disassemblerScannerBytes struct {
	asWords      bool
	unused       bool
	data         []z80.Byte
	startAddress z80.Address
	started      bool
}

func (d *disassemblerScannerBytes) Read(reader z80.MemoryReaderCount) {
	panic(errors.CannotExecuteData)
}

func (d *disassemblerScannerBytes) ChildPreCheck(reader z80.MemoryReaderCount, details z80.MemoryAnnotation) (child DisassemblyScanner) {
	// No children
	return
}

func (d *disassemblerScannerBytes) ScanImpl(reader z80.MemoryReaderCount, details z80.MemoryAnnotation, symbolsOnly bool) (op DisassemblerOp, child DisassemblyScanner, ok bool) {
	// Store start address
	if !d.started {
		d.startAddress = reader.CurrentAddress()
		d.started = true
	}
	d.data = nil
	// Split lines
	for reader.More() && len(d.data) < maxWidth {
		d.data = append(d.data, reader.ReadByte())
		ok = true
	}
	op = d
	return
}

func (d *disassemblerScannerBytes) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (d *disassemblerScannerBytes) Display(formatter format.Formatter) string {
	text := strings.Builder{}
	var tag string
	if d.unused {
		tag = "UNUSED: "
	} else {
		tag = "DATA: "
	}
	text.WriteString(formatter.Highlighter().Data(tag))
	if d.asWords {
		text.WriteString(wordsToCommaSepString(d.data, formatter.Highlighter(), d.startAddress))
	} else {
		text.WriteString(bytesToCommaSepString(d.data, formatter.Highlighter()))
	}
	return text.String()
}

func bytesToCommaSepString(buffer []z80.Byte, highlight format.SyntaxHighlighter) string {
	str := make([]string, len(buffer))
	for i, v := range buffer {
		str[i] = highlight.Number(v.String())
	}
	return strings.Join(str, ",")
}

func wordsToCommaSepString(buffer []z80.Byte, highlight format.SyntaxHighlighter, addr z80.Address) string {
	str := make([]string, 0, len(buffer))
	var lo, hi z80.Byte
	for i := 0; i < len(buffer); i += 2 {
		lo = buffer[i]
		if i < len(buffer)-1 {
			hi = buffer[i+1]
		} else {
			hi = 0
			log.Warn().Msg(fmt.Sprintf("Data truncated: Defined as words but odd number of bytes: %s", addr))
		}
		str = append(str, highlight.Number(z80.NewWord(lo, hi).String()))
	}
	return strings.Join(str, ",")
}
