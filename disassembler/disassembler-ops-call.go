package disassembler

import (
	"fmt"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
	"strings"
)

func Call(param1 opPartAddr) func() DisassemblerInstructionOpCode {
	return CallC(condition_None, param1)
}
func CallC(cond opPartBit, param2 opPartAddr) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("call", cond, param2) }
}
func Rst(param1 byte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return rst(param1) }
}

func Ret(cond opPartBit) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("ret", cond) }
}

// ----------------------------------------------------------------------------------------------------------------------------
// RST
// ----------------------------------------------------------------------------------------------------------------------------

type rst byte

func (o rst) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

func (r rst) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (r rst) Display(formatter format.Formatter) string {
	text := strings.Builder{}
	text.WriteString(formatter.Highlighter().Keyword("rst"))
	text.WriteString(" ")
	text.WriteString(formatter.Highlighter().Number(fmt.Sprintf("%02x", byte(r))))
	return text.String()
}

func (r rst) Read(reader z80.MemoryReaderCount) {
	// Nothing to read
}

func (r rst) ModifyParameter(z80.NumberFormatType, int) {
	// No change
}
