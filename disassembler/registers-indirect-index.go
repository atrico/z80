package disassembler

import (
	"fmt"
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

func IndirectIX() opPartT[z80.Byte] {
	return &indirectIndex{reg: "ix"}
}
func IndirectIY() opPartT[z80.Byte] {
	return &indirectIndex{reg: "iy"}
}
func IndirectIX_old() opPartByte {
	return &indirectIndex{reg: "ix"}
}
func IndirectIY_old() opPartByte {
	return &indirectIndex{reg: "iy"}
}

type indirectIndex struct {
	reg    string
	offset z80.Byte
}

func (p *indirectIndex) FormatWordAsAddress() {
	// Nothing to convert
}

func (p *indirectIndex) ReferencesAddress() (refs []z80.Address) {
	// No references
	return
}

func (p *indirectIndex) Display(formatter format.Formatter) string {
	reg := formatter.Highlighter().Variable(p.reg)
	offset := formatter.Highlighter().Number(p.offset.String())
	return fmt.Sprintf("(%s+%s)", reg, offset)
}

func (p *indirectIndex) Read(reader z80.MemoryReaderCount) {
	p.offset = reader.ReadByte()
}

func (p *indirectIndex) Get(snap snapshot.Snapshot) z80.Byte {
	// Get address
	addr := core.Must1(snap.Registers16Bit().Get, p.reg).Get().AsAddress().Offset(int(p.offset))
	// Get byte at this address
	return snap.ReadByte(addr)
}

func (p *indirectIndex) Set(val z80.Byte, snap snapshot.Snapshot) {
	// Get address
	addr := core.Must1(snap.Registers16Bit().Get, p.reg).Get().AsAddress().Offset(int(p.offset))
	// Write byte to this address
	snap.WriteByte(addr, val)
}

func (p *indirectIndex) ModifyParameter(z80.NumberFormatType, int) {
	// Nothing to do
}
