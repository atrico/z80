package symbols

import (
	"gitlab.com/atrico/z80"
)

type SymbolTable interface {
	// Lookup an address
	Lookup(addr z80.Address) (entry SymbolTableEntry, ok bool)
	// Map of all symbols
	Map() map[z80.Address]SymbolTableEntry
}

type SymbolTableEntry struct {
	Address z80.Address
	Label   string
	Used    []z80.Address
}

type SymbolTableBuilder interface {
	SymbolTable
	// Add a new address (if it has an annotation)
	Add(addr z80.Address) (label string)
	// Add a new referenced that's used, create label if not already labelled
	AddUsed(addr z80.Address, used z80.Address) (label string)
	// Add annotations
	AddAnnotations(annotations z80.MemoryAnnotations)
	// Add a config from yaml file
	AddConfig(config z80.AnnotationsLoader) (err error)
}

type LabelFormatter func(addr z80.Address) string

func NewSymbolTable(formatLabel LabelFormatter) SymbolTableBuilder {
	return NewSymbolTableInit(formatLabel)
}
func NewSymbolTableInit(formatLabel LabelFormatter, configs ...z80.AnnotationsLoader) SymbolTableBuilder {
	annotations, _ := z80.NewMemoryAnnotations(configs...)
	return &symbolTableImpl{annotations, make(map[z80.Address]SymbolTableEntry), formatLabel}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type symbolTableImpl struct {
	annotations z80.MemoryAnnotations
	symbols     map[z80.Address]SymbolTableEntry
	formatLabel LabelFormatter
}

func (s *symbolTableImpl) Lookup(addr z80.Address) (label SymbolTableEntry, ok bool) {
	label, ok = s.symbols[addr]
	return
}

func (s *symbolTableImpl) Map() map[z80.Address]SymbolTableEntry {
	return s.symbols
}

func (s *symbolTableImpl) Add(addr z80.Address) (label string) {
	if detail, ok := s.annotations.Lookup(addr); ok && detail.Label != "" {
		s.symbols[addr] = SymbolTableEntry{
			Address: addr,
			Label:   detail.Label,
		}
		label = detail.Label
	}
	return
}

func (s *symbolTableImpl) AddUsed(addr z80.Address, used z80.Address) (label string) {
	var entry SymbolTableEntry
	var ok bool
	// Find existing entry
	if entry, ok = s.symbols[addr]; !ok {
		// Lookup annotation
		var detail z80.MemoryAnnotation
		if detail, ok = s.annotations.Lookup(addr); ok && detail.Label != "" {
			label = detail.Label
		} else {
			label = s.createLabel(addr)
			ok = true
		}
		entry = SymbolTableEntry{
			Address: addr,
			Label:   label,
		}
	}
	// Add used reference
	entry.Used = append(entry.Used, used)
	s.symbols[addr] = entry
	return
}

func (s *symbolTableImpl) AddAnnotations(annotations z80.MemoryAnnotations) {
	s.annotations.Add(annotations)
}

func (s *symbolTableImpl) AddConfig(config z80.AnnotationsLoader) (err error) {
	return s.annotations.AddConfig(config)
}

func (s *symbolTableImpl) createLabel(addr z80.Address) string {
	return s.formatLabel(addr)
}
