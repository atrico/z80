package disassembler

import "gitlab.com/atrico/z80"

func incrementRef16(ref z80.WordRef) {
	modifyRef16(ref, 1)
}
func decrementRef16(ref z80.WordRef) {
	modifyRef16(ref, -1)
}
func modifyRef16(ref z80.WordRef, delta int) {
	ref.Set(ref.Get() + z80.Word(delta))
}
