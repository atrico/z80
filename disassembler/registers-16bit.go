package disassembler

import (
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type reg16 string

func (p reg16) FormatWordAsAddress() {
	// Nothing to convert
}

func (p reg16) ReferencesAddress() []z80.Address {
	return nil
}

func (p reg16) Display(formatter format.Formatter) string {
	return formatter.Highlighter().Variable(string(p))
}

func BC_old() opPartWord  { return _bc }
func DE_old() opPartWord  { return _de }
func HL_old() opPartWord  { return _hl }
func AF_old() opPartWord  { return _af }
func AFp_old() opPartWord { return _afp }
func SP_old() opPartWord  { return _sp }
func IX_old() opPartWord  { return _ix }
func IY_old() opPartWord  { return _iy }

func BC() opPartT[z80.Word]  { return _bc }
func DE() opPartT[z80.Word]  { return _de }
func HL() opPartT[z80.Word]  { return _hl }
func AF() opPartT[z80.Word]  { return _af }
func AFp() opPartT[z80.Word] { return _afp }
func SP() opPartT[z80.Word]  { return _sp }
func IX() opPartT[z80.Word]  { return _ix }
func IY() opPartT[z80.Word]  { return _iy }

var _bc = reg16("bc")
var _de = reg16("de")
var _hl = reg16("hl")
var _af = reg16("af")
var _afp = reg16("af'")
var _sp = reg16("sp")
var _ix = reg16("ix")
var _iy = reg16("iy")

func (p reg16) Read(_ z80.MemoryReaderCount) {
	// Nothing to do
}

func (p reg16) Get(snap snapshot.Snapshot) z80.Word {
	ref := core.Must1(snap.Registers16Bit().Get, string(p))
	return ref.Get()
}

func (p reg16) Set(val z80.Word, snap snapshot.Snapshot) {
	ref := core.Must1(snap.Registers16Bit().Get, string(p))
	ref.Set(val)
}

func (p reg16) ModifyParameter(z80.NumberFormatType, int) {
	// Nothing to do
}
