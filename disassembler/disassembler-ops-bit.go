package disassembler

func Bit(b int, param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("bit", bitVal(b), param1) }
}

func Res(b int, param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("res", bitVal(b), param1) }
}

func Set(b int, param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("set", bitVal(b), param1) }
}
