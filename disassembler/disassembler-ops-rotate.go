package disassembler

import "gitlab.com/atrico/z80/snapshot"

func Rlc(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("rlc", param1) }
}
func Rrc(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("rrc", param1) }
}
func Rl(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("rl", param1) }
}
func Rr(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("rr", param1) }
}

func Rlca() DisassemblerInstructionOpCode {
	return rlca{newOp0("rlca")}
}
func Rrca() DisassemblerInstructionOpCode {
	return rrca{newOp0("rrca")}
}
func Rla() DisassemblerInstructionOpCode {
	return rla{newOp0("rla")}
}
func Rra() DisassemblerInstructionOpCode {
	return rra{newOp0("rra")}
}

func Rld() DisassemblerInstructionOpCode {
	return rld{newOp0("rld")}
}
func Rrd() DisassemblerInstructionOpCode {
	return rrd{newOp0("rrd")}
}

// ----------------------------------------------------------------------------------------------------------------------------
// rlca
// ----------------------------------------------------------------------------------------------------------------------------

type rlca struct {
	op0
}

func (o rlca) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// rrca
// ----------------------------------------------------------------------------------------------------------------------------

type rrca struct {
	op0
}

func (o rrca) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// rla
// ----------------------------------------------------------------------------------------------------------------------------

type rla struct {
	op0
}

func (o rla) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// rra
// ----------------------------------------------------------------------------------------------------------------------------

type rra struct {
	op0
}

func (o rra) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// rld
// ----------------------------------------------------------------------------------------------------------------------------

type rld struct {
	op0
}

func (o rld) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// rrd
// ----------------------------------------------------------------------------------------------------------------------------

type rrd struct {
	op0
}

func (o rrd) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}
