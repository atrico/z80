package disassembler

func Dec8(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("dec", param1) }
}

func Dec16(param1 opPartWord) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("dec", param1) }
}
