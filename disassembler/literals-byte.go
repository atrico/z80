package disassembler

import (
	"fmt"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type numByte struct {
	byte   z80.Byte
	format z80.NumberFormatType
	offset int
}

func (p *numByte) FormatWordAsAddress() {
	// Nothing to convert

}

func (p *numByte) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (p *numByte) Display(formatter format.Formatter) (text string) {
	off := format.Offset(p.offset)
	switch p.format {
	case z80.NumberFormat_Char:
		text = formatter.Char(p.byte, off)
	case z80.NumberFormat_Mask:
		text = formatter.Highlighter().Number(fmt.Sprintf("%08b", int(p.byte)))
	case z80.NumberFormat_Signed:
		val := p.byte.TwosComplement()
		sgn := ""
		if val >= 0 {
			sgn = "+"
		}
		text = formatter.Highlighter().Number(fmt.Sprintf("%s%d", sgn, val))
	default:
		text = formatter.Byte(p.byte, off)
	}
	return
}

func literalByte() opPartT[z80.Byte]     { return &numByte{} }
func literalByteMask() opPartT[z80.Byte] { return &numByte{format: z80.NumberFormat_Mask} }
func literal_Byte_old() opPartByte       { return &numByte{} }

func (p *numByte) Read(reader z80.MemoryReaderCount) {
	p.byte = reader.ReadByte()
}

func (r *numByte) Get(snapshot.Snapshot) z80.Byte {
	return r.byte
}

func (r *numByte) Set(z80.Byte, snapshot.Snapshot) {
	panic(changeLiteralError)
}

func (p *numByte) ModifyParameter(format z80.NumberFormatType, offset int) {
	p.format = format
	p.offset = offset
}
