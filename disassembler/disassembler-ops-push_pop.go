package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/disassembler/symbols"
	"gitlab.com/atrico/z80/snapshot"
	"strings"
)

func Push(param1 opPartTF[z80.Word]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &push{newOp1[z80.Word]("push", param1)} }
}

func Pop(param1 opPartTF[z80.Word]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &pop{newOp1[z80.Word]("pop", param1)} }
}

// ----------------------------------------------------------------------------------------------------------------------------
// Push
// ----------------------------------------------------------------------------------------------------------------------------
type push struct {
	op1[z80.Word]
}

func (o *push) Execute(snap snapshot.Snapshot) {
	// Get value
	val := o.param1.Get(snap)
	// Get sp
	sp := snap.StackPointer().Get()
	// Decrease SP and push value
	sp = sp.Offset(-2)
	snap.StackPointer().Set(sp)
	snap.WriteWord(sp, val)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Pop
// ----------------------------------------------------------------------------------------------------------------------------

type pop struct {
	op1[z80.Word]
}

func (o *pop) Execute(snap snapshot.Snapshot) {
	// Get sp
	sp := snap.StackPointer().Get()
	// Read value and increase SP
	val := snap.ReadWord(sp)
	sp = sp.Offset(2)
	snap.StackPointer().Set(sp)
	// Set reg
	o.param1.Set(val, snap)
}

// ----------------------------------------------------------------------------------------------------------------------------
// common
// ----------------------------------------------------------------------------------------------------------------------------
func displayPushPop(name string, reg opPartWord, symbols symbols.SymbolTable, highlight format.SyntaxHighlighter, formatter format.Formatter) string {
	text := strings.Builder{}
	text.WriteString(highlight.Keyword(name))
	param := reg.Display(formatter)
	text.WriteString(" ")
	text.WriteString(param)
	return text.String()
}
