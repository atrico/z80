package disassembler

import "gitlab.com/atrico/z80/snapshot"

func Nop() DisassemblerInstructionOpCode {
	return nop{newOp0("nop")}
}
func Daa() DisassemblerInstructionOpCode {
	return daa{newOp0("daa")}
}
func Cpl() DisassemblerInstructionOpCode {
	return cpl{newOp0("cpl")}
}
func Scf() DisassemblerInstructionOpCode {
	return scf{newOp0("scf")}
}
func Ccf() DisassemblerInstructionOpCode {
	return ccf{newOp0("ccf")}
}
func Halt() DisassemblerInstructionOpCode {
	return halt{newOp0("halt")}
}
func Neg() DisassemblerInstructionOpCode {
	return neg{newOp0("neg")}
}

// ----------------------------------------------------------------------------------------------------------------------------
// nop
// ----------------------------------------------------------------------------------------------------------------------------

type nop struct {
	op0
}

func (o nop) Execute(snap snapshot.Snapshot) {
	// Do nothing!
}

// ----------------------------------------------------------------------------------------------------------------------------
// daa
// ----------------------------------------------------------------------------------------------------------------------------

type daa struct {
	op0
}

func (o daa) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// cpl
// ----------------------------------------------------------------------------------------------------------------------------

type cpl struct {
	op0
}

func (o cpl) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// scf
// ----------------------------------------------------------------------------------------------------------------------------

type scf struct {
	op0
}

func (o scf) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// ccf
// ----------------------------------------------------------------------------------------------------------------------------

type ccf struct {
	op0
}

func (o ccf) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// halt
// ----------------------------------------------------------------------------------------------------------------------------

type halt struct {
	op0
}

func (o halt) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// neg
// ----------------------------------------------------------------------------------------------------------------------------

type neg struct {
	op0
}

func (o neg) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}
