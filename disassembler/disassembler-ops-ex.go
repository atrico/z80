package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
	"strings"
)

func Ex(param1, param2 opPartTF[z80.Word]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &ex{newOp2("ex", param1, param2)} }
}

func Exx() DisassemblerInstructionOpCode {
	return exx{op0{"exx"}}
}

// ----------------------------------------------------------------------------------------------------------------------------
// ex
// ----------------------------------------------------------------------------------------------------------------------------
type ex struct {
	op2[z80.Word]
}

func (o *ex) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (o *ex) Display(formatter format.Formatter) string {
	text := strings.Builder{}
	text.WriteString(formatter.Highlighter().Keyword("ex"))
	text.WriteString(" ")
	text.WriteString(o.param1.Display(formatter))
	text.WriteString(",")
	text.WriteString(o.param2.Display(formatter))
	return text.String()
}

func (o *ex) Read(reader z80.MemoryReaderCount) {
	o.param1.Read(reader)
	o.param2.Read(reader)
}

func (o *ex) ModifyParameter(z80.NumberFormatType, int) {
}

func (o *ex) Execute(snap snapshot.Snapshot) {
	tmp := o.param1.Get(snap)
	o.param1.Set(o.param2.Get(snap), snap)
	o.param2.Set(tmp, snap)
}

// ----------------------------------------------------------------------------------------------------------------------------
// exx
// ----------------------------------------------------------------------------------------------------------------------------

type exx struct {
	op0
}

func (o exx) Execute(snap snapshot.Snapshot) {
	// Swap BC,DE & HL
	swapRef(snap.Registers16Bit().BC, snap.Registers16Bit().BCp)
	swapRef(snap.Registers16Bit().DE, snap.Registers16Bit().DEp)
	swapRef(snap.Registers16Bit().HL, snap.Registers16Bit().HLp)
}

// ----------------------------------------------------------------------------------------------------------------------------
// common
// ----------------------------------------------------------------------------------------------------------------------------

func swapRef(lhs, rhs z80.WordRef) {
	tmp := lhs.Get()
	lhs.Set(rhs.Get())
	rhs.Set(tmp)
}
