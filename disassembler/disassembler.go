package disassembler

import (
	"gitlab.com/atrico/console/ansi"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/disassembler/symbols"
	"strings"
)

type Disassembler interface {
	Scan(symbolsOnly bool) bool
	Current() DisassemblerLine
	SymbolTable() symbols.SymbolTable
	// Max widths of columns
	ColumnWidths() Columns
}

type Columns struct {
	Label       int
	Instruction int
	Comment     int
}

type DisassemblerLine struct {
	// Metadata
	NewSection     bool
	HeaderComments []string
	// Line
	Address  z80.Address
	Bytes    []z80.Byte
	Op       DisassemblerOp
	Comments []string
}

type DisassemblerOp interface {
	// Op uses address(es) (which should be converted to label)
	ReferencesAddress() []z80.Address
	// Format this op for display (including labels and syntax highlight)
	Display(formatter format.Formatter) string
}

type PostOpChildCheck func(op DisassemblerOp, reader z80.MemoryReaderCount, details z80.MemoryAnnotation) (child DisassemblyScanner)

func NewDisassembler(reader z80.MemoryReaderCount, dataScanners DisassemblerScannerByName, postCheck []PostOpChildCheck, format format.Formatter) Disassembler {
	postCheck = append([]PostOpChildCheck{GetMissingEdPostCheck()}, postCheck...)
	return &disassembler{reader: reader, start: reader.CurrentAddress(), scanner: NewDisassemblyInstructionScanner(reader, dataScanners, postCheck), format: format}
}

func NewDisassemblyInstructionScanner(reader z80.MemoryReaderCount, dataScanners DisassemblerScannerByName, postCheck []PostOpChildCheck) DisassemblyScanner {
	return NewDisassemblyScanner(reader, &disassemblerScannerInstruction{dataScanners: dataScanners, postFormatters: postCheck})
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type disassembler struct {
	reader       z80.MemoryReaderCount
	start        z80.Address
	currentLine  DisassemblerLine
	symbolTable  symbols.SymbolTableBuilder
	scanner      DisassemblyScanner
	format       format.Formatter
	columnWidths Columns
}

func (d *disassembler) Scan(symbolsOnly bool) (more bool) {
	if more = d.reader.More(); more {
		// Get metadata
		details, _ := d.reader.Annotations().Lookup(d.reader.CurrentAddress())
		// Scan next
		if more = d.scanner.Scan(details, symbolsOnly); more {
			d.currentLine = d.scanner.Current()
		}
	}
	return
}

func (d *disassembler) Current() DisassemblerLine {
	return d.currentLine
}

func (d *disassembler) SymbolTable() symbols.SymbolTable {
	if d.symbolTable == nil {
		d.symbolTable = d.format.SymbolTable()
		d.symbolTable.AddAnnotations(d.reader.Annotations())
		// Calculate table with first pass
		for d.Scan(true) {
			// Line addresses that have annotations
			label := d.symbolTable.Add(d.Current().Address)
			d.columnWidths.UpdateLabel(label)
			// Words used as addresses
			for _, addr := range d.Current().Op.ReferencesAddress() {
				label = d.symbolTable.AddUsed(addr, d.Current().Address)
				d.columnWidths.UpdateLabel(label)
			}
			// TODO - intelligent analysis of addresses (hl used in (hl) etc)
			// Update max column widths (for formatting)
			d.columnWidths.UpdateInstruction(d.Current().Op.Display(d.format))
			for _, cmt := range d.Current().Comments {
				d.columnWidths.UpdateComment(cmt)
			}
		}
	}
	d.reader.Reset()
	return d.symbolTable
}

func (d *disassembler) ColumnWidths() Columns {
	// Requires symbol table to have run
	d.SymbolTable()
	return d.columnWidths
}

func (c *Columns) UpdateLabel(label string) {
	c._updateWidth(label, &c.Label)
}
func (c *Columns) UpdateInstruction(instr string) {
	for _, part := range strings.Split(instr, "\n") {
		c._updateWidth(part, &c.Instruction)
	}
}
func (c *Columns) UpdateComment(comment string) {
	c._updateWidth(comment, &c.Comment)
}
func (c *Columns) _updateWidth(str string, width *int) {
	ln := ansi.StrLen(str)
	if ln > *width {
		*width = ln
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Mock highlighter - symbol table doesn't need highlighting
// ----------------------------------------------------------------------------------------------------------------------------
type symTabMockHighlighter struct {
}

func (s symTabMockHighlighter) Address(original string) string {
	return original
}

func (s symTabMockHighlighter) Word(original string) string {
	return original
}

func (s symTabMockHighlighter) Byte(original string) string {
	return original
}

func (s symTabMockHighlighter) String(original string) string {
	return original
}

func (s symTabMockHighlighter) Number(original string) string {
	return original
}

func (s symTabMockHighlighter) Variable(original string) string {
	return original
}

func (s symTabMockHighlighter) Keyword(original string) string {
	return original
}

func (s symTabMockHighlighter) Comment(original string) string {
	return original
}

func (s symTabMockHighlighter) Condition(original string) string {
	return original
}

func (s symTabMockHighlighter) Label(original string) string {
	return original
}

func (s symTabMockHighlighter) Data(original string) string {
	return original
}

func (s symTabMockHighlighter) CustomN(original string, num int) string {
	return original
}

func (s symTabMockHighlighter) Custom(original string, formatter format.HighlightFunc) string {
	return original
}

// ----------------------------------------------------------------------------------------------------------------------------
// Single NOP scanner
// This will read a single byte and return NOP (for missing ED opCodes
// ----------------------------------------------------------------------------------------------------------------------------

func GetMissingEdPostCheck() PostOpChildCheck {
	return func(op DisassemblerOp, reader z80.MemoryReaderCount, details z80.MemoryAnnotation) (child DisassemblyScanner) {
		// Check for missing ED - Nasty hack
		if op == NOP && reader.ByteBuffer()[0] == 0xed {
			// Rewind 1 byte - this will be NOP next time
			reader.Rewind(1)
			// Temp scanner to read this as nop whatever it was
			child = NewDisassemblyScanner(reader.ChildReader(1), &singleNopScanner{})
		}
		return
	}
}

type singleNopScanner struct {
}

func (d *singleNopScanner) ChildPreCheck(reader z80.MemoryReaderCount, details z80.MemoryAnnotation) (child DisassemblyScanner) {
	// No children
	return
}

func (d *singleNopScanner) ScanImpl(reader z80.MemoryReaderCount, details z80.MemoryAnnotation, symbolsOnly bool) (op DisassemblerOp, child DisassemblyScanner, ok bool) {
	// Discard byte
	reader.ReadByte()
	// Always return NOP
	op = Nop()
	ok = true
	return
}
