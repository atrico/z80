package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
	"strings"
)

type op1[T any] struct {
	op0
	param1 opPartT[T]
}

func newOp1[T any](name string, param1 opPartTF[T]) op1[T] {
	return op1[T]{op0{name}, param1()}
}

func (o *op1[T]) ModifyParameter(format z80.NumberFormatType, data int) {
	o.param1.ModifyParameter(format, data)
}

func (o *op1[T]) ReferencesAddress() (refs []z80.Address) {
	refs = append(refs, o.param1.ReferencesAddress()...)
	return
}

func (o *op1[T]) Display(formatter format.Formatter) string {
	text := strings.Builder{}
	text.WriteString(o.op0.Display(formatter))
	text.WriteString(" ")
	paramFormatter := formatter
	text.WriteString(o.param1.Display(paramFormatter))
	return text.String()
}

func (o *op1[T]) Read(reader z80.MemoryReaderCount) {
	o.param1.Read(reader)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Old
// ----------------------------------------------------------------------------------------------------------------------------

func newOp1_old(name string, param1 opPart) *op1_old {
	return &op1_old{name, param1}
}

type op1_old struct {
	name   string
	param1 opPart
}

// TODO - remove this (all ops should have their own type)
func (o *op1_old) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

func (o *op1_old) ModifyParameter(z80.NumberFormatType, int) {
}

func (o *op1_old) ReferencesAddress() (refs []z80.Address) {
	refs = append(refs, o.param1.ReferencesAddress()...)
	return
}

func (o *op1_old) Display(formatter format.Formatter) string {
	text := strings.Builder{}
	text.WriteString(formatter.Highlighter().Keyword(o.name))
	param := o.param1.Display(formatter)
	if param != "" {
		if !strings.Contains(o.name, " ") {
			text.WriteString(" ")
		}
		text.WriteString(param)
	}
	return text.String()
}

func (o *op1_old) Read(reader z80.MemoryReaderCount) {
	o.param1.Read(reader)
}
