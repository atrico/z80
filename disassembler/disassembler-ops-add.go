package disassembler

func Add8(param1, param2 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("add", param1, param2) }
}

func Adc8(param1, param2 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("adc", param1, param2) }
}

func Add16(param1, param2 opPartWord) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("add", param1, param2) }
}

func Adc16(param1, param2 opPartWord) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("adc", param1, param2) }
}
