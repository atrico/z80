package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
	"strings"
)

type op2[T any] struct {
	op1[T]
	param2 opPartT[T]
}

func newOp2[T any](name string, param1, param2 opPartTF[T]) op2[T] {
	return op2[T]{newOp1(name, param1), param2()}
}

func (o *op2[T]) ModifyParameter(format z80.NumberFormatType, data int) {
	o.op1.ModifyParameter(format, data)
	o.param2.ModifyParameter(format, data)
}
func (o *op2[T]) ReferencesAddress() (refs []z80.Address) {
	refs = append(refs, o.op1.ReferencesAddress()...)
	refs = append(refs, o.param2.ReferencesAddress()...)
	return
}

func (o *op2[T]) Display(formatter format.Formatter) string {
	text := strings.Builder{}
	text.WriteString(o.op1.Display(formatter))
	text.WriteString(",")
	paramFormatter := formatter
	text.WriteString(o.param2.Display(paramFormatter))
	return text.String()
}

func (o *op2[T]) Read(reader z80.MemoryReaderCount) {
	o.op1.Read(reader)
	o.param2.Read(reader)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Old
// ----------------------------------------------------------------------------------------------------------------------------

func newOp2_old(name string, param1, param2 opPart) *op2_old {
	return &op2_old{*newOp1_old(name, param1), param2}
}

type op2_old struct {
	op1_old
	param2 opPart
}

// TODO - remove this (all ops should have their own type)
func (o *op2_old) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

func (o *op2_old) ModifyParameter(z80.NumberFormatType, int) {
}

func (o *op2_old) ReferencesAddress() (refs []z80.Address) {
	refs = append(refs, o.op1_old.ReferencesAddress()...)
	refs = append(refs, o.param2.ReferencesAddress()...)
	return
}

func (o *op2_old) Display(formatter format.Formatter) string {
	text := strings.Builder{}
	text.WriteString(o.op1_old.Display(formatter))
	sep := ","
	if o.param1.Display(formatter) == "" {
		sep = " "
	}
	text.WriteString(sep)
	text.WriteString(o.param2.Display(formatter))
	return text.String()
}

func (o *op2_old) Read(reader z80.MemoryReaderCount) {
	o.op1_old.Read(reader)
	o.param2.Read(reader)
}
