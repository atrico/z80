package disassembler

import (
	"errors"
	"fmt"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type DisassemblerOpRead interface {
	DisassemblerOp
	// Read in op from memory
	Read(reader z80.MemoryReaderCount)
}
type DisassemblerInstructionOp interface {
	DisassemblerOpRead
	// Execute this instruction on using the given snapshot.  Snapshot is modified accordingly
	Execute(snap snapshot.Snapshot)
}

type opPart interface {
	DisassemblerOpRead
}

type ParameterModifier byte

const (
	ParameterModifier_Offset        ParameterModifier = iota
	ParameterModifier_WordAsAddress ParameterModifier = iota
	ParameterModifier_ByteAsChar    ParameterModifier = iota
	ParameterModifier_ByteAsMask    ParameterModifier = iota
)

type opPartT[T any] interface {
	DisassemblerOpRead
	snapshot.SnapshotRef[T]
	ModifyParameter(format z80.NumberFormatType, offset int)
}
type opPartTF[T any] func() opPartT[T]

type opPartBit interface {
	opPart
	snapshot.SnapshotRef[bool]
}

type opPartByte interface {
	opPart
	snapshot.SnapshotRef[z80.Byte]
}
type opPartByteF func() opPartByte

type opPartWord interface {
	opPart
	snapshot.SnapshotRef[z80.Word]
	// Convert op to use word as address
	FormatWordAsAddress()
}
type opPartWordF func() opPartWord

type opPartAddr interface {
	opPart
	snapshot.SnapshotRef[z80.Address]
}

type opPartDisplace interface {
	opPartByte
}

type ioByte struct {
	byte z80.Byte
}

func (p *ioByte) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (p *ioByte) Display(formatter format.Formatter) string {
	return fmt.Sprintf("(%s)", formatter.Highlighter().Number(p.byte.String()))
}

var IoByte = &ioByte{}

func (p *ioByte) Read(reader z80.MemoryReaderCount) {
	p.byte = reader.ReadByte()
}

func (p *ioByte) Get(snap snapshot.Snapshot) z80.Byte {
	panic("NYI: func (p indirectReg) Get")
}

func (p *ioByte) Set(val z80.Byte, snap snapshot.Snapshot) {

	panic(errors.New("cannot change io data byte"))
}

type bitVal int

func (p bitVal) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (p bitVal) Display(formatter format.Formatter) string {
	return formatter.Highlighter().Number(fmt.Sprintf("%v", p))
}

func (p bitVal) Read(reader z80.MemoryReaderCount) {
	// Nothing to do
}

func (p bitVal) Get(snap snapshot.Snapshot) z80.Byte {
	panic("NYI: func (p bitVal) Get")
}

func (p bitVal) Set(val z80.Byte, snap snapshot.Snapshot) {
	panic(errors.New("cannot change bit index"))
}
