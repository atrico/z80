package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type numAddr struct {
	addr z80.Address
}

func (p *numAddr) FormatWordAsAddress() {
	// Nothing to convert
}

func (p *numAddr) ReferencesAddress() []z80.Address {
	return []z80.Address{p.addr}
}

func (p *numAddr) Display(formatter format.Formatter) string {
	return formatter.AddressRef(p.addr)
}

var literal_Address = &numAddr{}

func (p *numAddr) Read(reader z80.MemoryReaderCount) {
	p.addr = reader.ReadAddress()
}

func (p *numAddr) Get(snapshot.Snapshot) z80.Address {
	return p.addr
}

func (p *numAddr) Set(z80.Address, snapshot.Snapshot) {
	panic(changeLiteralError)
}
