package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/snapshot"
)

func Sub8(param1 opPartTF[z80.Byte]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &sub8{newOp1("sub", param1)} }
}
func Sbc8(param1 opPartTF[z80.Byte]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &sbc8{newOp1("sbc", param1)} }
}

func Sbc16(param1, param2 opPartWord) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &op2_old{op1_old{"sbc", param1}, param2} }
}

// ----------------------------------------------------------------------------------------------------------------------------
// sub
// ----------------------------------------------------------------------------------------------------------------------------
type sub8 struct {
	op1[z80.Byte]
}

func (s sub8) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// sbc
// ----------------------------------------------------------------------------------------------------------------------------
type sbc8 struct {
	op1[z80.Byte]
}

func (s sbc8) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}
