package disassembler

import (
	"errors"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type condition string

func (p condition) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (p condition) Display(formatter format.Formatter) string {
	return formatter.Highlighter().Condition(string(p))
}

var condition_None = no_condition{}
var condition_C = condition("c")
var condition_NC = condition("nc")
var condition_Z = condition("z")
var condition_NZ = condition("nz")
var condition_PO = condition("po")
var condition_PE = condition("pe")
var condition_P = condition("p")
var condition_M = condition("m")

func (p condition) Read(_ z80.MemoryReaderCount) {
	// Nothing to do
}

func (p condition) Get(_snap snapshot.Snapshot) bool {
	panic("NYI: func (p condition) Get")
}

func (p condition) Set(bool, snapshot.Snapshot) {
	panic(errors.New("cannot change a condition"))
}

// ----------------------------------------------------------------------------------------------------------------------------
// No condition
// ----------------------------------------------------------------------------------------------------------------------------

type no_condition struct{}

func (c no_condition) Read(reader z80.MemoryReaderCount) {
	// Nothing to read
}

func (c no_condition) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (c no_condition) Display(formatter format.Formatter) string {
	// Nothing to show
	return ""
}

func (p no_condition) Get(_snap snapshot.Snapshot) bool {
	panic("NYI: func (p no_condition) Get")
}

func (p no_condition) Set(bool, snapshot.Snapshot) {
	panic(errors.New("cannot change a condition"))
}
