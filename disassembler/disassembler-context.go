package disassembler

import "gitlab.com/atrico/z80"

type DisassembleContext struct {
	instrSet        map[z80.Byte]func() DisassemblerInstructionOpCode
	indexed, cb, ed bool
	skip            int // Skip byte (used in indexing offsets
}

func (c DisassembleContext) setIx() DisassembleContext {
	return DisassembleContext{instrSet: ddInstructions, indexed: true, cb: c.cb, ed: c.ed}
}
func (c DisassembleContext) setIy() DisassembleContext {
	return DisassembleContext{instrSet: fdInstructions, indexed: true, cb: c.cb, ed: c.ed}
}
func (c DisassembleContext) setCb(bytes []z80.Byte) DisassembleContext {
	switch {
	case len(bytes) == 1:
		return DisassembleContext{instrSet: cbInstructions, indexed: c.indexed, cb: true, ed: c.ed}
	case bytes[0] == 0xdd:
		return DisassembleContext{instrSet: ddcbInstructions, indexed: c.indexed, cb: true, ed: c.ed}
	case bytes[0] == 0xfd:
		return DisassembleContext{instrSet: fdcbInstructions, indexed: c.indexed, cb: true, ed: c.ed}
	}
	panic("invalid prefix")
}
func (c DisassembleContext) setEd() DisassembleContext {
	return DisassembleContext{instrSet: edInstructions, indexed: c.indexed, ed: true}
}
