package disassembler

func Sla(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("sla", param1) }
}
func Sra(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("sra", param1) }
}
func Sll(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("sll", param1) }
}
func Srl(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("srl", param1) }
}
