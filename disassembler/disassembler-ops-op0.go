package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
)

func newOp0(name string) op0 {
	return op0{name}
}

type op0 struct {
	name string
}

func (o op0) ModifyParameter(z80.NumberFormatType, int) {
	// No change
}

func (o op0) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}
func (o op0) Display(formatter format.Formatter) string {
	return formatter.Highlighter().Keyword(o.name)
}

func (o op0) Read(reader z80.MemoryReaderCount) {
	// Nothing to read
}
