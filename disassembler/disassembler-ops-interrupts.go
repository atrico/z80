package disassembler

import (
	"fmt"
	"gitlab.com/atrico/z80/snapshot"
)

func Di() DisassemblerInstructionOpCode {
	return di{newOp0("di")}
}
func Ei() DisassemblerInstructionOpCode {
	return ei{newOp0("ei")}
}

func Im(mode int) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return im{newOp0(fmt.Sprintf("im %d", mode))} }
}

func Retn() DisassemblerInstructionOpCode {
	return retn{newOp0("retn")}
}
func Reti() DisassemblerInstructionOpCode {
	return reti{newOp0("reti")}
}

// ----------------------------------------------------------------------------------------------------------------------------
// di
// ----------------------------------------------------------------------------------------------------------------------------

type di struct {
	op0
}

func (o di) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// ei
// ----------------------------------------------------------------------------------------------------------------------------

type ei struct {
	op0
}

func (o ei) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// Interrupt mode
// ----------------------------------------------------------------------------------------------------------------------------

type im struct {
	op0
}

func (o im) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// retn
// ----------------------------------------------------------------------------------------------------------------------------

type retn struct {
	op0
}

func (o retn) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// reti
// ----------------------------------------------------------------------------------------------------------------------------

type reti struct {
	op0
}

func (o reti) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}
