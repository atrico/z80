package disassembler

import (
	"gitlab.com/atrico/z80"
)

type DisassemblyScanner interface {
	Scan(details z80.MemoryAnnotation, symbolsOnly bool) bool
	Current() DisassemblerLine
}

type DisassemblyScannerImplFactory func() DisassemblyScannerImpl

var NOP = stdInstructions[0x00]()

// ----------------------------------------------------------------------------------------------------------------------------
// Scanners
// ----------------------------------------------------------------------------------------------------------------------------

type DisassemblerScannerByName func(name string) (impl DisassemblyScannerImpl, ok bool)

var Z80DataScanners = map[string]DisassemblyScannerImplFactory{
	// Parse data as bytes
	"bytes": func() DisassemblyScannerImpl {
		return &disassemblerScannerBytes{asWords: false}
	},
	// Parse data as words
	"words": func() DisassemblyScannerImpl {
		return &disassemblerScannerBytes{asWords: true}
	},
	// Parse data as bytes but mark as unused
	"unused": func() DisassemblyScannerImpl {
		return &disassemblerScannerBytes{asWords: false, unused: true}
	},
}

// ----------------------------------------------------------------------------------------------------------------------------
// Common Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type DisassemblyScannerImpl interface {
	// Should we create a new child to use this time(based on details)
	ChildPreCheck(reader z80.MemoryReaderCount, details z80.MemoryAnnotation) (child DisassemblyScanner)
	// Perform scan on current scanner
	ScanImpl(reader z80.MemoryReaderCount, details z80.MemoryAnnotation, symbolsOnly bool) (op DisassemblerOp, nextChild DisassemblyScanner, ok bool)
}

func NewDisassemblyScanner(reader z80.MemoryReaderCount, impl DisassemblyScannerImpl) DisassemblyScanner {
	return &DisassemblyScannerCommon{reader: reader, impl: impl}
}

type DisassemblyScannerCommon struct {
	nextChild, child DisassemblyScanner
	reader           z80.MemoryReaderCount
	impl             DisassemblyScannerImpl
	current          DisassemblerLine
}

func (d *DisassemblyScannerCommon) Scan(details z80.MemoryAnnotation, symbolsOnly bool) (more bool) {
	// Swap next child on first scan
	if d.nextChild != nil {
		d.child = d.nextChild
		d.nextChild = nil
	}
	// Child?
	if d.child != nil {
		if more = d.child.Scan(details, symbolsOnly); more {
			return
		}
		// Child finished
		d.child = nil
	}
	// Pre check for new child
	if d.child = d.impl.ChildPreCheck(d.reader, details); d.child != nil {
		if more = d.child.Scan(details, symbolsOnly); more {
			return
		}
		// Child finished
		d.child = nil
	}
	// This scanner
	if more = d.reader.More(); more {
		// Set address and clear byte buffer
		d.current.Address = d.reader.CurrentAddress()
		d.reader.ResetByteBuffer()
		// Call implementation for op
		if d.current.Op, d.nextChild, more = d.impl.ScanImpl(d.reader, details, symbolsOnly); more {
			// Set Metadata
			d.current.NewSection = details.NewSection
			d.current.HeaderComments = details.Header
			d.current.Comments = nil
			if details.Comment != "" {
				d.current.Comments = []string{details.Comment}
			}
			for _, cmt := range details.CommentContinue {
				d.current.Comments = append(d.current.Comments, cmt)
			}
			// Bytes
			d.current.Bytes = d.reader.ByteBuffer()
		}
	}
	return
}

func (d *DisassemblyScannerCommon) Current() DisassemblerLine {
	// Child?
	if d.child != nil {
		return d.child.Current()
	}
	return d.current
}
