package disassembler

import (
	"gitlab.com/atrico/z80"
)

var fdcbInstructions = map[z80.Byte]func() DisassemblerInstructionOpCode{
	0x06: Rlc(IndirectIY()),
	0x0e: Rrc(IndirectIY()),
	0x16: Rl(IndirectIY()),
	0x1e: Rr(IndirectIY()),
	0x26: Sla(IndirectIY()),
	0x2e: Sra(IndirectIY()),
	0x36: Sll(IndirectIY()),
	0x3e: Srl(IndirectIY()),
	0x46: Bit(0, IndirectIY()),
	0x4e: Bit(1, IndirectIY()),
	0x56: Bit(2, IndirectIY()),
	0x5e: Bit(3, IndirectIY()),
	0x66: Bit(4, IndirectIY()),
	0x6e: Bit(5, IndirectIY()),
	0x76: Bit(6, IndirectIY()),
	0x7e: Bit(7, IndirectIY()),
	0x86: Res(0, IndirectIY()),
	0x8e: Res(1, IndirectIY()),
	0x96: Res(2, IndirectIY()),
	0x9e: Res(3, IndirectIY()),
	0xa6: Res(4, IndirectIY()),
	0xae: Res(5, IndirectIY()),
	0xb6: Res(6, IndirectIY()),
	0xbe: Res(7, IndirectIY()),
	0xc6: Set(0, IndirectIY()),
	0xce: Set(1, IndirectIY()),
	0xd6: Set(2, IndirectIY()),
	0xde: Set(3, IndirectIY()),
	0xe6: Set(4, IndirectIY()),
	0xee: Set(5, IndirectIY()),
	0xf6: Set(6, IndirectIY()),
	0xfe: Set(7, IndirectIY()),
}
