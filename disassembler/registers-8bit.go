package disassembler

import (
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type reg8 string

func (p reg8) FormatWordAsAddress() {
	// Nothing to convert

}

func (p reg8) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (p reg8) Display(formatter format.Formatter) string {
	return formatter.Highlighter().Variable(string(p))
}

func B() opPartT[z80.Byte]   { return reg8("b") }
func C() opPartT[z80.Byte]   { return reg8("c") }
func D() opPartT[z80.Byte]   { return reg8("d") }
func E() opPartT[z80.Byte]   { return reg8("e") }
func H() opPartT[z80.Byte]   { return reg8("h") }
func L() opPartT[z80.Byte]   { return reg8("l") }
func A() opPartT[z80.Byte]   { return reg8("a") }
func I() opPartT[z80.Byte]   { return reg8("i") }
func R() opPartT[z80.Byte]   { return reg8("r") }
func IXh() opPartT[z80.Byte] { return reg8("ixh") }
func IXl() opPartT[z80.Byte] { return reg8("ixl") }
func IYh() opPartT[z80.Byte] { return reg8("iyh") }
func IYl() opPartT[z80.Byte] { return reg8("iyl") }

func B_old() opPartByte   { return reg8("b") }
func C_old() opPartByte   { return reg8("c") }
func D_old() opPartByte   { return reg8("d") }
func E_old() opPartByte   { return reg8("e") }
func H_old() opPartByte   { return reg8("h") }
func L_old() opPartByte   { return reg8("l") }
func A_old() opPartByte   { return reg8("a") }
func I_old() opPartByte   { return reg8("i") }
func R_old() opPartByte   { return reg8("r") }
func IXh_old() opPartByte { return reg8("ixh") }
func IXl_old() opPartByte { return reg8("ixl") }
func IYh_old() opPartByte { return reg8("iyh") }
func IYl_old() opPartByte { return reg8("iyl") }

var _b = reg8("b")
var _c = reg8("c")
var _d = reg8("d")
var _e = reg8("e")
var _h = reg8("h")
var _l = reg8("l")
var _a = reg8("a")
var _i = reg8("i")
var _r = reg8("r")
var _ixh = reg8("ixh")
var _ixl = reg8("ixl")
var _iyh = reg8("iyh")
var _iyl = reg8("iyl")

func (p reg8) Read(_ z80.MemoryReaderCount) {
	// Nothing to do
}

func (p reg8) Get(snap snapshot.Snapshot) z80.Byte {
	ref := core.Must1(snap.Registers8Bit().Get, string(p))
	return ref.Get()
}

func (p reg8) Set(val z80.Byte, snap snapshot.Snapshot) {
	ref := core.Must1(snap.Registers8Bit().Get, string(p))
	ref.Set(val)
}

func (p reg8) ModifyParameter(z80.NumberFormatType, int) {
	// Nothing to do
}
