package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/snapshot"
)

func Ld8(param1, param2 opPartTF[z80.Byte]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &ld8{newOp2("ld", param1, param2)} }
}
func Ld8Flags(param1, param2 opPartTF[z80.Byte]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &ld8flags{ld8{newOp2("ld", param1, param2)}} }
}

func Ld16(param1, param2 opPartTF[z80.Word]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &ld16{newOp2("ld", param1, param2)} }
}

// ----------------------------------------------------------------------------------------------------------------------------
// ld 8 bit
// ----------------------------------------------------------------------------------------------------------------------------

type ld8 struct {
	op2[z80.Byte]
}

func (o *ld8) Execute(snap snapshot.Snapshot) {
	val := o.param2.Get(snap)
	o.param1.Set(val, snap)
}

// ----------------------------------------------------------------------------------------------------------------------------
// ld 8 bit with flags (for ld a,i and ld a,r)
// ----------------------------------------------------------------------------------------------------------------------------

type ld8flags struct {
	ld8
}

func (o *ld8flags) Execute(snap snapshot.Snapshot) {
	o.ld8.Execute(snap)
	// Set the flags
	val := o.param2.Get(snap)
	// Ignores C
	snap.Flags().N.Set(false)
	snap.Flags().PV.Set(snap.InterruptsEnabled().Get())
	snap.Flags().B3.Set(val&0x08 != 0)
	snap.Flags().H.Set(false)
	snap.Flags().B5.Set(val&0x20 != 0)
	snap.Flags().Z.Set(val == 0)
	snap.Flags().S.Set(val&0x80 != 0)
}

// ----------------------------------------------------------------------------------------------------------------------------
// ld 16 bit
// ----------------------------------------------------------------------------------------------------------------------------

type ld16 struct {
	op2[z80.Word]
}

func (o *ld16) Execute(snap snapshot.Snapshot) {
	val := o.param2.Get(snap)
	o.param1.Set(val, snap)
}
