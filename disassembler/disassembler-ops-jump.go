package disassembler

func Jp(param1 opPartAddr) func() DisassemblerInstructionOpCode {
	return JpC(condition_None, param1)
}
func JpC(cond opPartBit, param2 opPartAddr) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("jp", cond, param2) }
}
func JpReg(param1 opPartWord) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("jp", param1) }
}
func Jr(param1 opPartDisplace) func() DisassemblerInstructionOpCode {
	return JrC(condition_None, param1)
}
func JrC(cond opPartBit, param2 opPartDisplace) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("jr", cond, param2) }
}

func Djnz(param1 opPartDisplace) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("djnz", param1) }
}
