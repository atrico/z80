package disassembler

import (
	"fmt"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

func CreateIndirectLiteralAddressByte(val z80.Address) opPartByte {
	return &indirectAddrByte{val, 0}
}

// ----------------------------------------------------------------------------------------------------------------------------
// 8 bit
// ----------------------------------------------------------------------------------------------------------------------------

type indirectAddrByte struct {
	address z80.Address
	offset  int
}

func (p *indirectAddrByte) FormatWordAsAddress() {
	// Nothing to convert
	// (already an address)
}

func (p *indirectAddrByte) ReferencesAddress() (refs []z80.Address) {
	refs = append(refs, p.address.Offset(p.offset))
	return
}

func (p *indirectAddrByte) Display(formatter format.Formatter) string {
	return fmt.Sprintf("(%s)", formatter.AddressRef(p.address, format.Offset(p.offset)))
}

func IndirectAddressByte() opPartT[z80.Byte] { return &indirectAddrByte{} }
func IndirectAddressByte_old() opPartByte    { return &indirectAddrByte{} }

func (p *indirectAddrByte) Read(reader z80.MemoryReaderCount) {
	p.address = reader.ReadAddress()
}

func (p *indirectAddrByte) Get(snap snapshot.Snapshot) z80.Byte {
	// Get byte at this address
	return snap.ReadByte(p.address)
}

func (p *indirectAddrByte) Set(val z80.Byte, snap snapshot.Snapshot) {
	// Write byte to this address
	snap.WriteByte(p.address, val)
}

func (p *indirectAddrByte) ModifyParameter(format z80.NumberFormatType, offset int) {
	p.offset = offset
}

// ----------------------------------------------------------------------------------------------------------------------------
// 16 bit
// ----------------------------------------------------------------------------------------------------------------------------

type indirectAddrWord struct {
	indirectAddrByte
}

func (p *indirectAddrWord) FormatWordAsAddress() {
	// Nothing to convert
	// (already an address)
}

func IndirectAddressWord() opPartT[z80.Word] { return &indirectAddrWord{indirectAddrByte{}} }
func IndirectAddressWord_old() opPartWord    { return &indirectAddrWord{indirectAddrByte{}} }

func (p *indirectAddrWord) Read(reader z80.MemoryReaderCount) {
	p.indirectAddrByte.Read(reader)
}

func (p *indirectAddrWord) Get(snap snapshot.Snapshot) z80.Word {
	return snap.ReadWord(p.address)
}

func (p *indirectAddrWord) Set(val z80.Word, snap snapshot.Snapshot) {
	// Write word to this address
	snap.WriteWord(p.address, val)
}

func (p *indirectAddrWord) ModifyParameter(format z80.NumberFormatType, offset int) {
	p.offset = offset
}
