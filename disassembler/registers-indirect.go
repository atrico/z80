package disassembler

import (
	"fmt"
	"gitlab.com/atrico/core"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type indirectReg string

func (p indirectReg) FormatWordAsAddress() {
	// Nothing to convert
}

func (p indirectReg) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (p indirectReg) Display(formatter format.Formatter) string {
	return fmt.Sprintf("(%s)", formatter.Highlighter().Variable(string(p)))
}

type indirectReg16 indirectReg

func (p indirectReg16) FormatWordAsAddress() {
	// Nothing to convert
}

func (p indirectReg16) ReferencesAddress() (refs []z80.Address) {
	// No refs
	return
}

func (p indirectReg16) Display(formatter format.Formatter) string {
	return fmt.Sprintf("(%s)", formatter.Highlighter().Variable(string(p)))
}

func IndirectC() opPartT[z80.Byte]    { return _indirectC }
func IndirectBC() opPartT[z80.Byte]   { return _indirectBC }
func IndirectDE() opPartT[z80.Byte]   { return _indirectDE }
func IndirectHL() opPartT[z80.Byte]   { return _indirectHL }
func IndirectSP16() opPartT[z80.Word] { return _indirectSP16 }

func IndirectC_old() opPartByte  { return _indirectC }
func IndirectBC_old() opPartByte { return _indirectBC }
func IndirectDE_old() opPartByte { return _indirectDE }
func IndirectHL_old() opPartByte { return _indirectHL }

var _indirectC = indirectReg("c")
var _indirectBC = indirectReg("bc")
var _indirectDE = indirectReg("de")
var _indirectHL = indirectReg("hl")
var _indirectSP = indirectReg("sp")
var _indirectSP16 = indirectReg16(_indirectSP)

func (p indirectReg) Read(_ z80.MemoryReaderCount) {
	// Nothing to do
}

func (p indirectReg) Get(snap snapshot.Snapshot) z80.Byte {
	// Get address
	addr := core.Must1(snap.Registers16Bit().Get, string(p)).Get().AsAddress()
	// Get byte at this address
	return snap.ReadByte(addr)
}

func (p indirectReg) Set(val z80.Byte, snap snapshot.Snapshot) {
	// Get address
	addr := core.Must1(snap.Registers16Bit().Get, string(p)).Get().AsAddress()
	// Write byte to this address
	snap.WriteByte(addr, val)
}

func (p indirectReg) ModifyParameter(z80.NumberFormatType, int) {
	// Nothing to do
}

func (p indirectReg16) Read(reader z80.MemoryReaderCount) {
	indirectReg(p).Read(reader)
}

func (p indirectReg16) Get(snap snapshot.Snapshot) z80.Word {
	// Get address
	addr := core.Must1(snap.Registers16Bit().Get, string(p)).Get().AsAddress()
	// Get word at this address
	return snap.ReadWord(addr)
}

func (p indirectReg16) Set(val z80.Word, snap snapshot.Snapshot) {
	// Get address
	addr := core.Must1(snap.Registers16Bit().Get, string(p)).Get().AsAddress()
	// Write word to this address
	snap.WriteWord(addr, val)
}

func (p indirectReg16) ModifyParameter(z80.NumberFormatType, int) {
	// Nothing to do
}
