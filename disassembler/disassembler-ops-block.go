package disassembler

import (
	"gitlab.com/atrico/z80/snapshot"
)

func Ldi() DisassemblerInstructionOpCode {
	return ldi{newOp0("ldi")}
}
func Ldir() DisassemblerInstructionOpCode {
	return ldir{newOp0("ldir")}
}

func Ldd() DisassemblerInstructionOpCode {
	return ldd{newOp0("ldd")}
}
func Lddr() DisassemblerInstructionOpCode {
	return lddr{newOp0("lddr")}
}

func Cpi() DisassemblerInstructionOpCode {
	return cpi{newOp0("cpi")}
}
func Cpir() DisassemblerInstructionOpCode {
	return cpir{newOp0("cpir")}
}

func Cpd() DisassemblerInstructionOpCode {
	return cpd{newOp0("cpd")}
}
func Cpdr() DisassemblerInstructionOpCode {
	return cpdr{newOp0("cpdr")}
}

// ----------------------------------------------------------------------------------------------------------------------------
// ldi / ldd
// ----------------------------------------------------------------------------------------------------------------------------

type ldi struct {
	op0
}

type ldd struct {
	op0
}

func (o ldi) Execute(snap snapshot.Snapshot) { executeLdiLdd(snap, 1) }
func (o ldd) Execute(snap snapshot.Snapshot) { executeLdiLdd(snap, -1) }

func executeLdiLdd(snap snapshot.Snapshot, delta int) {
	// Copy byte
	source := snap.Registers16Bit().HL.Get().AsAddress()
	dest := snap.Registers16Bit().DE.Get().AsAddress()
	val := snap.ReadByte(source)
	snap.WriteByte(dest, val)
	// Increment/Decrement pointers
	modifyRef16(snap.Registers16Bit().HL, delta)
	modifyRef16(snap.Registers16Bit().DE, delta)
	// Decrement count
	decrementRef16(snap.Registers16Bit().BC)
	// reset N & H flags
	snap.Flags().N.Set(false)
	snap.Flags().H.Set(false)
	// PV
	snap.Flags().PV.Set(snap.Registers16Bit().BC.Get() != 0)
	// B3,5
	a := snap.Registers8Bit().A.Get()
	tmp := a + val
	snap.Flags().B3.Set(tmp&0x8 != 0)
	snap.Flags().B5.Set(tmp&0x2 != 0)
}

// ----------------------------------------------------------------------------------------------------------------------------
// ldir/lddr
// ----------------------------------------------------------------------------------------------------------------------------

type ldir struct {
	op0
}

type lddr struct {
	op0
}

func (o ldir) Execute(snap snapshot.Snapshot) {
	// Repeat until BC == 0
	for snap.Registers16Bit().BC.Get() > 0 {
		executeLdiLdd(snap, 1)
	}
}
func (o lddr) Execute(snap snapshot.Snapshot) {
	// Repeat until BC == 0
	for snap.Registers16Bit().BC.Get() > 0 {
		executeLdiLdd(snap, -1)
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// cpi
// ----------------------------------------------------------------------------------------------------------------------------

type cpi struct {
	op0
}

func (o cpi) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// cpir
// ----------------------------------------------------------------------------------------------------------------------------

type cpir struct {
	op0
}

func (o cpir) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// cpd
// ----------------------------------------------------------------------------------------------------------------------------

type cpd struct {
	op0
}

func (o cpd) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// cpdr
// ----------------------------------------------------------------------------------------------------------------------------

type cpdr struct {
	op0
}

func (o cpdr) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}
