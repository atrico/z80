package disassembler

import (
	"gitlab.com/atrico/z80"
)

type disassemblerScannerInstruction struct {
	dataScanners   DisassemblerScannerByName
	postFormatters []PostOpChildCheck
}

func (d *disassemblerScannerInstruction) ChildPreCheck(reader z80.MemoryReaderCount, details z80.MemoryAnnotation) (child DisassemblyScanner) {
	// Start new data scanner?
	if details.Data > 0 || details.DataFormat != "" {
		if dis, ok := d.dataScanners(details.DataFormat); ok {
			count := details.Data
			if count == 0 {
				count = 0x10000
			}
			child = NewDisassemblyScanner(reader.ChildReader(count), dis)
		}
	}
	return
}

func (d *disassemblerScannerInstruction) ScanImpl(reader z80.MemoryReaderCount, details z80.MemoryAnnotation, symbolsOnly bool) (op DisassemblerOp, child DisassemblyScanner, ok bool) {
	// Get instruction
	instruction, context := disassembleInstructionImpl(reader, DisassembleContext{instrSet: stdInstructions})
	// Modify parameters
	instruction.ModifyParameter(details.NumberFormat, details.NumberOffset)
	// Read the parameters
	instruction.Read(reader)
	// TODO - nasty hack to swap bytes
	if context.indexed && context.cb {
		tmp := reader.ByteBuffer()[2]
		reader.ByteBuffer()[2] = reader.ByteBuffer()[3]
		reader.ByteBuffer()[3] = tmp
	}
	// TODO - nasty hack for index offset order swap
	reader.SkipBytes(context.skip)
	// Run post formatters
	for _, post := range d.postFormatters {
		child = post(instruction, reader, details)
		if child != nil {
			break
		}
	}
	op = instruction
	ok = true
	return
}

func disassembleInstructionImpl(reader z80.MemoryReaderCount, context DisassembleContext) (instruction DisassemblerInstructionOpCode, endContext DisassembleContext) {
	// ddcb/fdcb = offset is first (unlike std instruction set)
	if context.indexed && context.cb {
		reader.SkipBytes(1) // skip the offset
	}
	by := reader.ReadByte()
	if context.indexed && context.cb {
		reader.SkipBytes(-2) // Skip back to offset
		context.skip = 1
	}
	switch {
	// IX,IY
	case (by == 0xdd || by == 0xfd) && len(reader.ByteBuffer()) == 1:
		var ctx DisassembleContext
		if by == 0xdd {
			ctx = context.setIx()
		} else {
			ctx = context.setIy()
		}
		instruction, endContext = disassembleInstructionImpl(reader, ctx)
	// CB
	case by == 0xcb && (len(reader.ByteBuffer()) == 1 || (len(reader.ByteBuffer()) == 2 && (reader.ByteBuffer()[0] == 0xdd || reader.ByteBuffer()[0] == 0xfd))): // TODO - WTF?
		instruction, endContext = disassembleInstructionImpl(reader, context.setCb(reader.ByteBuffer()))
	// ED
	case by == 0xed && len(reader.ByteBuffer()) == 1:
		instruction, endContext = disassembleInstructionImpl(reader, context.setEd())
	default:
		if instr, ok := context.instrSet[by]; ok {
			instruction = instr()
		} else {
			instruction = NOP
		}
		endContext = context
	}
	return
}
