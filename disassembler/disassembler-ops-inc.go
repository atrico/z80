package disassembler

func Inc8(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("inc", param1) }
}

func Inc16(param1 opPartWord) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("inc", param1) }
}
