package disassembler

func And(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("and", param1) }
}

func Or(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("or", param1) }
}

func Xor(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp1_old("xor", param1) }
}
