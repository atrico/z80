package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/snapshot"
)

func Cp(param1 opPartTF[z80.Byte]) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return &cp{newOp1[z80.Byte]("cp", param1)} }
}

type cp struct {
	op1[z80.Byte]
}

func (c *cp) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}
