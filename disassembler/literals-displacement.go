package disassembler

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/format"
	"gitlab.com/atrico/z80/snapshot"
)

type displace struct {
	addr   z80.Address
	offset int
}

func (p *displace) FormatWordAsAddress() {
	// Nothing to convert
}

func (p *displace) ReferencesAddress() (refs []z80.Address) {
	refs = append(refs, p.addr)
	return
}

func (p *displace) Display(formatter format.Formatter) string {
	return formatter.AddressRef(p.addr)
}

var literal_Displace = &displace{}

func (p *displace) Read(reader z80.MemoryReaderCount) {
	p.offset = reader.ReadByte().TwosComplement()
	// Calculate address
	p.addr = z80.Address(int(reader.CurrentAddress()) + p.offset)
}

func (p *displace) Get(snap snapshot.Snapshot) z80.Byte {
	panic("NYI: func (p displace) Get")
}

func (p *displace) Set(val z80.Byte, snap snapshot.Snapshot) {
	panic(changeLiteralError)
}
