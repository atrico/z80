package disassembler

import (
	"gitlab.com/atrico/z80/snapshot"
)

func In(param1 opPartByte) func() DisassemblerInstructionOpCode {
	return In2(param1, IndirectC())
}
func In2(param1, param2 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("in", param1, param2) }
}
func Out(param2 opPartByte) func() DisassemblerInstructionOpCode {
	return Out2(IndirectC(), param2)
}
func Out2(param1, param2 opPartByte) func() DisassemblerInstructionOpCode {
	return func() DisassemblerInstructionOpCode { return newOp2_old("out", param1, param2) }
}

func Ini() DisassemblerInstructionOpCode {
	return ini{newOp0("ini")}
}
func Inir() DisassemblerInstructionOpCode {
	return inir{newOp0("inir")}
}
func Ind() DisassemblerInstructionOpCode {
	return ind{newOp0("ind")}
}
func Indr() DisassemblerInstructionOpCode {
	return indr{newOp0("indr")}
}

func Outi() DisassemblerInstructionOpCode {
	return outi{newOp0("outi")}
}
func Otir() DisassemblerInstructionOpCode {
	return otir{newOp0("otir")}
}
func Outd() DisassemblerInstructionOpCode {
	return outd{newOp0("outd")}
}
func Otdr() DisassemblerInstructionOpCode {
	return otdr{newOp0("otdr")}
}

// ----------------------------------------------------------------------------------------------------------------------------
// ini
// ----------------------------------------------------------------------------------------------------------------------------

type ini struct {
	op0
}

func (o ini) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// inir
// ----------------------------------------------------------------------------------------------------------------------------

type inir struct {
	op0
}

func (o inir) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// ind
// ----------------------------------------------------------------------------------------------------------------------------

type ind struct {
	op0
}

func (o ind) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// indr
// ----------------------------------------------------------------------------------------------------------------------------

type indr struct {
	op0
}

func (o indr) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// outi
// ----------------------------------------------------------------------------------------------------------------------------

type outi struct {
	op0
}

func (o outi) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// otir
// ----------------------------------------------------------------------------------------------------------------------------

type otir struct {
	op0
}

func (o otir) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// outd
// ----------------------------------------------------------------------------------------------------------------------------

type outd struct {
	op0
}

func (o outd) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}

// ----------------------------------------------------------------------------------------------------------------------------
// otdr
// ----------------------------------------------------------------------------------------------------------------------------

type otdr struct {
	op0
}

func (o otdr) Execute(snap snapshot.Snapshot) {
	//TODO implement me
	panic("implement me")
}
