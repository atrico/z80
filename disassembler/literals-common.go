package disassembler

import "errors"

var changeLiteralError = errors.New("cannot change a literal")
