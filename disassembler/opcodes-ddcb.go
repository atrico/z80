package disassembler

import (
	"gitlab.com/atrico/z80"
)

var ddcbInstructions = map[z80.Byte]func() DisassemblerInstructionOpCode{
	0x06: Rlc(IndirectIX()),
	0x0e: Rrc(IndirectIX()),
	0x16: Rl(IndirectIX()),
	0x1e: Rr(IndirectIX()),
	0x26: Sla(IndirectIX()),
	0x2e: Sra(IndirectIX()),
	0x36: Sll(IndirectIX()),
	0x3e: Srl(IndirectIX()),
	0x46: Bit(0, IndirectIX()),
	0x4e: Bit(1, IndirectIX()),
	0x56: Bit(2, IndirectIX()),
	0x5e: Bit(3, IndirectIX()),
	0x66: Bit(4, IndirectIX()),
	0x6e: Bit(5, IndirectIX()),
	0x76: Bit(6, IndirectIX()),
	0x7e: Bit(7, IndirectIX()),
	0x86: Res(0, IndirectIX()),
	0x8e: Res(1, IndirectIX()),
	0x96: Res(2, IndirectIX()),
	0x9e: Res(3, IndirectIX()),
	0xa6: Res(4, IndirectIX()),
	0xae: Res(5, IndirectIX()),
	0xb6: Res(6, IndirectIX()),
	0xbe: Res(7, IndirectIX()),
	0xc6: Set(0, IndirectIX()),
	0xce: Set(1, IndirectIX()),
	0xd6: Set(2, IndirectIX()),
	0xde: Set(3, IndirectIX()),
	0xe6: Set(4, IndirectIX()),
	0xee: Set(5, IndirectIX()),
	0xf6: Set(6, IndirectIX()),
	0xfe: Set(7, IndirectIX()),
}
