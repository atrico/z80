package format

type HighlightFunc func(original string) string

type SyntaxHighlighter interface {
	Address(original string) string
	Word(original string) string
	Char(original string) string
	Byte(original string) string
	String(original string) string
	Number(original string) string
	Variable(original string) string
	Keyword(original string) string
	Comment(original string) string
	Condition(original string) string
	Label(original string) string
	Data(original string) string
	Error(original string) string
	CustomN(original string, num int) string
	Custom(original string, formatter HighlightFunc) string
}
