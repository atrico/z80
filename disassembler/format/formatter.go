package format

import (
	"fmt"
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler/symbols"
	"strings"
)

type Formatter interface {
	SymbolTable() symbols.SymbolTableBuilder
	Highlighter() SyntaxHighlighter
	Char(val z80.Byte, configurators ...Configurator) string
	Byte(val z80.Byte, configurators ...Configurator) string
	Word(val z80.Word, configurators ...Configurator) string
	Address(val z80.Address, configurators ...Configurator) string
	AddressRef(val z80.Address, configurators ...Configurator) string
}

type CharFormatter func(by z80.Byte) (ch string, ok bool)

type TypeDefaultConfig struct {
	typeName      string
	configurators []Configurator
}

func AddDefaultConfig(name string, configurators ...Configurator) TypeDefaultConfig {
	return TypeDefaultConfig{name, configurators}
}

func NewFormatter(highlight SyntaxHighlighter, charFormatter CharFormatter, symbolBuilder func(formatter symbols.LabelFormatter) symbols.SymbolTableBuilder, defaultConfigs ...TypeDefaultConfig) Formatter {
	// start with blank config
	var defaultConfig Config
	// Find default settings
	for _, cfg := range defaultConfigs {
		// Modify the overall default
		if cfg.typeName == "" {
			defaultConfig = createConfig(Config{}, cfg.configurators)
			break
		}
	}
	configs := make(map[string]Config)
	configs[""] = defaultConfig
	// Add all others
	for _, cfg := range defaultConfigs {
		if cfg.typeName != "" {
			configs[strings.ToLower(cfg.typeName)] = createConfig(defaultConfig, cfg.configurators)
		}
	}

	formatter := formatterService{defaultConfigs: configs, highlight: highlight, charFormatter: charFormatter}
	formatter.symbols = symbolBuilder(func(addr z80.Address) string { return formatter.Address(addr, AsLabel) })
	return formatter
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type formatterService struct {
	defaultConfigs map[string]Config
	symbols        symbols.SymbolTableBuilder
	highlight      SyntaxHighlighter
	charFormatter  CharFormatter
}

func (f formatterService) SymbolTable() symbols.SymbolTableBuilder {
	return f.symbols
}

func (f formatterService) Highlighter() SyntaxHighlighter {
	return f.highlight
}

func (f formatterService) Char(value z80.Byte, configurators ...Configurator) string {
	config := f.getConfig("char", configurators)
	value = z80.Byte(int(value) + config.Offset)
	var ch string
	var ok bool
	if ch, ok = f.charFormatter(value); ok {
		ch = f.highlight.Char(ch)
	} else {
		ch = f.highlight.Error("?")
	}
	return fmt.Sprintf(`"%s"%s`, ch, f.formatOffset(config.Offset, config))
}

func (f formatterService) Byte(val z80.Byte, configurators ...Configurator) string {
	return f.value(int(val), bite, configurators)
}

func (f formatterService) Word(val z80.Word, configurators ...Configurator) string {
	return f.value(int(val), word, configurators)
}

func (f formatterService) Address(val z80.Address, configurators ...Configurator) string {
	return f.value(int(val), addr, configurators)
}

func (f formatterService) AddressRef(val z80.Address, configurators ...Configurator) string {
	return f.value(int(val), addrRef, configurators)
}

type valueType string

const (
	bite    valueType = "byte"
	word    valueType = "word"
	addr    valueType = "address"
	addrRef valueType = "addressref"
)

func (f formatterService) value(value int, theType valueType, configurators []Configurator) (text string) {
	config := f.getConfig(string(theType), configurators)
	value += config.Offset
	// If address reference...
	if theType == addrRef {
		if entry, ok := f.symbols.Lookup(z80.Address(value)); ok {
			text = f.highlight.Label(entry.Label)
		} else {
			AsLabel(&config)
		}
	}
	if text == "" {
		var prefix, lead, width, base string
		prefix = config.Prefix
		switch config.Base {
		case Base_Hex:
			base = "x"
			if config.FixedWidth == FixedWidth_Hex || config.FixedWidth == FixedWidth_All {
				width = "4"
				if theType == bite {
					width = "2"
				}
			}
			if config.LeadingZero == LeadingZero_Hex || config.LeadingZero == LeadingZero_All {
				lead = "0"
			}
			break
		case Base_Dec:
			base = "d"
			if config.FixedWidth == FixedWidth_Dec || config.FixedWidth == FixedWidth_All {
				width = "5"
				if theType == bite {
					width = "3"
				}
			}
			if config.LeadingZero == LeadingZero_Dec || config.LeadingZero == LeadingZero_All {
				lead = "0"
			}
		}
		fs := fmt.Sprintf("%s%%%s%s%s", prefix, lead, width, base)
		text = fmt.Sprintf(fs, int(value))
		// Highlight
		switch theType {
		case bite:
			text = f.highlight.Byte(text)
		case word:
			text = f.highlight.Word(text)
		case addr:
			text = f.highlight.Address(text)
		case addrRef:
			text = f.highlight.Label(text)
		}
	}
	// Offset
	text += f.formatOffset(config.Offset, config)
	return
}

func (f formatterService) formatOffset(offset int, config Config) (text string) {
	if offset != 0 {
		build := strings.Builder{}
		build.WriteString(text)
		// Reverse offset in label (offset is added to address, formatting adds delta)
		if offset > 0 {
			build.WriteString("-")
		} else {
			build.WriteString("+")
		}
		offsetAbs := absInt(offset)
		if offsetAbs > 255 {
			build.WriteString(f.Word(z80.Word(offsetAbs), SubWordConfig(config)))
		} else {
			build.WriteString(f.Byte(z80.Byte(offsetAbs), SubWordConfig(config)))
		}
		text = build.String()
	}
	return
}

func createConfig(defaultConfig Config, configurators []Configurator) (config Config) {
	config = defaultConfig.Copy()
	for _, cfg := range configurators {
		cfg(&config)
	}
	return config
}

func (f formatterService) getConfig(name string, configurators []Configurator) (config Config) {
	var ok bool
	// Get default
	if config, ok = f.defaultConfigs[name]; !ok {
		config = f.defaultConfigs[""]
	}
	// Modify
	return createConfig(config, configurators)
}

func absInt(val int) int {
	if val < 0 {
		return -val
	}
	return val
}
