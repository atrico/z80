package format

type Config struct {
	Base        BaseType
	Prefix      string
	FixedWidth  FixedWidthType
	LeadingZero LeadingZeroType
	AsAddress   bool
	Offset      int
}

func (c Config) Copy() (cpy Config) {
	cpy.Base = c.Base
	cpy.Prefix = c.Prefix
	cpy.FixedWidth = c.FixedWidth
	cpy.LeadingZero = c.LeadingZero
	cpy.AsAddress = c.AsAddress
	cpy.Offset = c.Offset
	return
}

type BaseType int

const (
	Base_Hex BaseType = iota
	Base_Dec BaseType = iota
)

type FixedWidthType int

const (
	FixedWidth_Hex  FixedWidthType = iota
	FixedWidth_Dec  FixedWidthType = iota
	FixedWidth_None FixedWidthType = iota
	FixedWidth_All  FixedWidthType = iota
)

type LeadingZeroType int

const (
	LeadingZero_Hex  LeadingZeroType = iota
	LeadingZero_Dec  LeadingZeroType = iota
	LeadingZero_None LeadingZeroType = iota
	LeadingZero_All  LeadingZeroType = iota
)

type Configurator func(config *Config)

// Hexadecimal
var Hex = NumberBase(Base_Hex)

// Decimal
var Dec = NumberBase(Base_Dec)

// Number base
func NumberBase(b BaseType) Configurator {
	return func(config *Config) {
		(*config).Base = b
	}
}

// Fixed width values
var FixedWidth = FixedWidthCustom(FixedWidth_All)

// Not fixed width values
var NoFixedWidth = FixedWidthCustom(FixedWidth_None)

// Fixed width values of this type
func FixedWidthCustom(t FixedWidthType) Configurator {
	return func(config *Config) {
		(*config).FixedWidth = t
	}
}

var LeadingZero = LeadingZeroCustom(LeadingZero_All)
var NoLeadingZero = LeadingZeroCustom(LeadingZero_None)

// Leading zero
func LeadingZeroCustom(t LeadingZeroType) Configurator {
	return func(config *Config) {
		(*config).LeadingZero = t
	}
}

// Format as label
var AsLabel = AsLabelCustom("L_")

// Format as custom label
func AsLabelCustom(prefix string) Configurator {
	return func(config *Config) {
		FixedWidth(config)
		LeadingZero(config)
		Prefix(prefix)(config)
	}
}

// Prefix
func Prefix(prefix string) Configurator {
	return func(config *Config) {
		(*config).Prefix = prefix
	}
}

// Format (word) as an address
func AsAddress() Configurator {
	return func(config *Config) {
		(*config).AsAddress = true
	}
}

// Offset
func Offset(offset int) Configurator {
	return func(config *Config) {
		(*config).Offset = offset
	}
}

// Pull out bits of config for a sub word
func SubWordConfig(current Config) Configurator {
	return func(config *Config) {
		*config = Config{
			Base:        current.Base,
			FixedWidth:  current.FixedWidth,
			LeadingZero: current.LeadingZero,
		}
	}
}
