package z80

import (
	"errors"
	"fmt"
	"slices"
)

//goland:noinspection GoStandardMethods
type MemoryRead interface {
	ReadByte(addr Address) Byte
	ReadWord(addr Address) Word
	ReadWordBigEndian(addr Address) Word
	// Get range of bytes as slice
	ReadBytes(start, end Address) []Byte
	// Get a reader a
	Reader(start Address) MemoryReader
	// Get a reader with limited data
	ReaderCount(start, end Address) MemoryReaderCount
	// Get the annotations
	Annotations() MemoryAnnotations
}

//goland:noinspection GoStandardMethods
type MemoryWrite interface {
	WriteByte(addr Address, val Byte)
	WriteWord(addr Address, val Word)
	WriteWordBigEndian(addr Address, val Word)
	WriteBytes(start Address, by []Byte)
	// Get references
	GetByteRef(addr Address) ByteRef
	GetWordRef(addr Address) WordRef
	GetAddressRef(addr Address) AddressRef
	GetBlockRef(addr Address, len int) ByteBlockRef
}

//goland:noinspection GoStandardMethods
type Memory interface {
	MemoryRead
	MemoryWrite
	// Clone entire memory
	Clone() Memory
}

func NewMemory(rom []Byte, ram []Byte, annotationConfigs ...AnnotationsLoader) (mem Memory, err error) {
	var annot MemoryAnnotations
	if annot, err = NewMemoryAnnotations(annotationConfigs...); err == nil {
		mem, err = NewMemoryA(rom, ram, annot)
	}
	return
}
func NewMemoryA(rom []Byte, ram []Byte, annotations MemoryAnnotations) (mem Memory, err error) {
	if len(rom)+len(ram) != 0x10000 {
		err = errors.New(fmt.Sprintf("Data wrong size: %d != %d", len(rom)+len(ram), 0x10000))
	} else {
		m := memory{
			rom:         make([]Byte, len(rom)),
			ram:         make([]Byte, len(ram)),
			annotations: annotations,
		}
		for i := range rom {
			m.rom[i] = rom[i]
			i++
		}
		for i := range ram {
			m.ram[i] = ram[i]
			i++
		}
		mem = &m
	}
	return
}

type memory struct {
	rom         []Byte
	ram         []Byte
	annotations MemoryAnnotations
}

//goland:noinspection GoStandardMethods
func (m *memory) ReadByte(addr Address) (by Byte) {
	if int(addr) < len(m.rom) {
		by = m.rom[addr]
	} else {
		by = m.ram[addr.Offset(-len(m.rom))]
	}
	return
}

//goland:noinspection GoStandardMethods
func (m *memory) WriteByte(addr Address, val Byte) {
	// Can't write to ROM
	if int(addr) >= len(m.rom) {
		m.ram[addr.Offset(-len(m.rom))] = val
	}
}

func (m *memory) ReadWord(addr Address) Word {
	return Word(m.ReadByte(addr)) + 0x100*Word(m.ReadByte(addr+1))
}
func (m *memory) WriteWord(addr Address, val Word) {
	m.WriteByte(addr, val.Lo())
	m.WriteByte(addr.Offset(1), val.Hi())
}

func (m *memory) ReadWordBigEndian(addr Address) Word {
	return 0x100*Word(m.ReadByte(addr)) + Word(m.ReadByte(addr+1))
}
func (m *memory) WriteWordBigEndian(addr Address, val Word) {
	m.WriteByte(addr, val.Hi())
	m.WriteByte(addr.Offset(1), val.Lo())
}

func (m *memory) ReadBytes(start, end Address) (by []Byte) {
	count := int(end - start)
	if count <= 0 {
		count += 0x10000
	}
	newEnd := int(start) + count
	// Shortcut all in ROM
	if newEnd < len(m.rom) {
		return slices.Clone(m.rom[start:newEnd])
	}
	// Shortcut all in RAM
	if int(start) >= len(m.rom) {
		return slices.Clone(m.ram[start.Offset(-len(m.rom)) : newEnd-len(m.rom)])
	}
	// Have to copy
	by = append(m.rom[start:], m.ram[:newEnd-len(m.rom)]...)
	return
}

func (m *memory) WriteBytes(start Address, bytes []Byte) {
	for i, by := range bytes {
		m.WriteByte(start.Offset(i), by)
	}
}

func (m *memory) GetByteRef(addr Address) ByteRef {
	return NewByteRefMem(m, addr)
}

func (m *memory) GetWordRef(addr Address) WordRef {
	return NewWordRefMem(m, addr)
}

func (m *memory) GetAddressRef(addr Address) AddressRef {
	return NewAddressRefMem(m, addr)
}

func (m *memory) GetBlockRef(addr Address, len int) ByteBlockRef {
	return NewByteBlockRefMem(m, addr, len)
}

func (m *memory) Clone() Memory {
	return &memory{
		rom: m.rom,
		ram: slices.Clone(m.ram[:]),
	}
}

func (m *memory) Annotations() MemoryAnnotations {
	return m.annotations
}
