package z80

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type memoryReaderChild struct {
	MemoryReaderCount
	count      int
	startAddr  Address
	startCount int
}

func (m *memoryReaderChild) More() bool {
	return m.count > 0
}

//goland:noinspection GoStandardMethods
func (m *memoryReaderChild) ReadByte() Byte {
	m.count--
	return m.MemoryReaderCount.ReadByte()
}

func (m *memoryReaderChild) ReadWord() Word {
	m.count -= 2
	return m.MemoryReaderCount.ReadWord()
}
func (m *memoryReaderChild) ReadWordBigEndian() Word {
	m.count -= 2
	return m.MemoryReaderCount.ReadWordBigEndian()
}

func (m *memoryReaderChild) ReadAddress() Address {
	m.count -= 2
	return m.MemoryReaderCount.ReadAddress()
}
func (m *memoryReaderChild) ReadAddressBigEndian() Address {
	m.count -= 2
	return m.MemoryReaderCount.ReadAddressBigEndian()
}

func (m *memoryReaderChild) ReadBytes(count int) []Byte {
	m.count -= count
	return m.MemoryReaderCount.ReadBytes(count)
}

func (m *memoryReaderChild) ReadWords(count int) []Word {
	m.count -= count * 2
	return m.MemoryReaderCount.ReadWords(count)
}

func (m *memoryReaderChild) ReadAllBytes() []Byte {
	return m.ReadBytes(m.count)
}

func (m *memoryReaderChild) ReadAllWords() []Word {
	return m.ReadWords(m.count / 2)
}

func (m *memoryReaderChild) ReadBlockWithCount() []Byte {
	count := int(m.ReadWord())
	return m.ReadBytes(count)
}

func (m *memoryReaderChild) SkipBytes(delta int) {
	m.count -= delta
	m.MemoryReaderCount.SkipBytes(delta)
}

func (m *memoryReaderChild) Reset() {
	m.Rewind(m.startCount - m.count)
}

func (m *memoryReaderChild) ChildReader(count int) (reader MemoryReaderCount) {
	// Truncate count if not enough
	if count > m.count {
		count = m.count
	}
	reader = &memoryReaderChild{
		MemoryReaderCount: m,
		count:             count,
		startAddr:         m.CurrentAddress(),
		startCount:        count,
	}
	return
}

func (m *memoryReaderChild) Rewind(count int) {
	m.count += count
	m.MemoryReaderCount.Rewind(count)
}
