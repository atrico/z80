package executor

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/disassembler"
	"gitlab.com/atrico/z80/snapshot"
	"golang.org/x/exp/slices"
)

// Execute code (at the PC)
// TODO - context to determine when to stop, currently just single instruction
func ExecuteCode(snap snapshot.Snapshot) {
	instr, bytes := GetNextInstruction(snap)
	// Increase the program counter
	snap.ProgramCounter().Set(snap.ProgramCounter().Get().Offset(len(bytes)))
	// Increase R
	// Inc 1 each instruction
	// Prefix counts as 1 instruction (dd,fd,ed,cb
	newR := snap.Registers8Bit().R.Get()
	newR++
	prefix := []z80.Byte{0xdd, 0xfd, 0xcb, 0xed}
	if slices.Contains(prefix, bytes[0]) {
		newR++
	}
	snap.Registers8Bit().R.Set(newR)
	// Execute
	instr.Execute(snap)
}

func GetNextInstruction(snap snapshot.Snapshot) (disassembler.DisassemblerInstructionOp, []z80.Byte) {
	reader := snap.ReaderCount(snap.ProgramCounter().Get(), 16) // 16 long enough for single instruction
	noPostOpChildCheck := make([]disassembler.PostOpChildCheck, 0)
	noDataScanners := func(name string) (impl disassembler.DisassemblyScannerImpl, ok bool) { return }
	scanner := disassembler.NewDisassemblyInstructionScanner(reader, noDataScanners, noPostOpChildCheck)
	if scanner.Scan(z80.MemoryAnnotation{}, false) {
		if inst, ok := scanner.Current().Op.(disassembler.DisassemblerInstructionOp); ok {
			return inst, scanner.Current().Bytes
		}
	}
	panic("not an instruction")
}
