package interrupt

type Mode byte

const (
	IM0 Mode = 0
	IM1 Mode = 1
	IM2 Mode = 2
)

func (m Mode) String() string {
	switch m {
	case IM0:
		return "IM0"
	case IM1:
		return "IM1"
	case IM2:
		return "IM2"
	default:
		return "INVALID"
	}
}
