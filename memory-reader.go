package z80

//goland:noinspection GoStandardMethods
type MemoryReader interface {
	CurrentAddress() Address
	// Get the annotations
	Annotations() MemoryAnnotations
	PeekByte() Byte
	PeekWord() Word
	PeekWordBigEndian() Word
	PeekAddress() Address
	PeekAddressBigEndian() Address
	ReadByte() Byte
	ReadWord() Word
	ReadWordBigEndian() Word
	ReadAddress() Address
	ReadAddressBigEndian() Address
	ReadBytes(count int) []Byte
	ReadWords(count int) []Word
	ReadBlockWithCount() []Byte
	ByteBuffer() []Byte
	ResetByteBuffer()
	SkipBytes(delta int)
	// Absolute peek
	PeekByteAt(address Address) Byte
	PeekWordAt(address Address) Word
	PeekWordBigEndianAt(address Address) Word
	PeekAddressAt(address Address) Address
	PeekAddressBigEndianAt(address Address) Address
	// Create child reader to read a subset of that remaining
	ChildReader(count int) (reader MemoryReaderCount)
	// Rewind the location
	Rewind(count int)
	// Reset the address to the specified address
	ResetAddress(addr Address)
}

func (m *memory) Reader(start Address) MemoryReader {
	return &memoryReader{
		memory: m,
		addr:   start,
		buffer: nil,
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type memoryReader struct {
	memory Memory
	addr   Address
	buffer []Byte
}

func (r *memoryReader) Annotations() MemoryAnnotations {
	return r.memory.Annotations()
}

func (r *memoryReader) CurrentAddress() Address {
	return r.addr
}

func (r *memoryReader) PeekByte() Byte {
	return r.memory.ReadByte(r.addr)
}

func (r *memoryReader) PeekWord() Word {
	return r.memory.ReadWord(r.addr)
}
func (r *memoryReader) PeekWordBigEndian() Word {
	return r.memory.ReadWordBigEndian(r.addr)
}

func (r *memoryReader) PeekAddress() Address {
	return r.PeekWord().AsAddress()
}
func (r *memoryReader) PeekAddressBigEndian() Address {
	return r.PeekWordBigEndian().AsAddress()
}

//goland:noinspection GoStandardMethods
func (r *memoryReader) ReadByte() (by Byte) {
	by = r.memory.ReadByte(r.addr)
	r.buffer = append(r.buffer, by)
	r.addr++
	return
}

func (r *memoryReader) ReadWords(count int) (buffer []Word) {
	buffer = make([]Word, count)
	for i := range buffer {
		buffer[i] = r.ReadWord()
	}
	return buffer
}

func (r *memoryReader) ReadWord() (wd Word) {
	wd = r.memory.ReadWord(r.addr)
	r.buffer = append(r.buffer, wd.Lo(), wd.Hi())
	r.addr += 2
	return
}
func (r *memoryReader) ReadWordBigEndian() (wd Word) {
	wd = r.memory.ReadWordBigEndian(r.addr)
	r.buffer = append(r.buffer, wd.Hi(), wd.Lo())
	r.addr += 2
	return
}

func (r *memoryReader) ReadAddress() Address {
	return Address(r.ReadWord())
}
func (r *memoryReader) ReadAddressBigEndian() Address {
	return Address(r.ReadWordBigEndian())
}

func (r *memoryReader) ReadBytes(count int) (buffer []Byte) {
	buffer = make([]Byte, count)
	for i := range buffer {
		buffer[i] = r.ReadByte()
	}
	return buffer
}
func (r *memoryReader) ReadBlockWithCount() []Byte {
	count := int(r.ReadWord())
	return r.ReadBytes(count)
}

func (r *memoryReader) ByteBuffer() []Byte {
	return r.buffer
}

func (r *memoryReader) ResetByteBuffer() {
	r.buffer = nil
}

func (r *memoryReader) PeekByteAt(address Address) Byte {
	return r.memory.ReadByte(address)
}

func (r *memoryReader) PeekWordAt(address Address) Word {
	return Word(int(r.PeekByteAt(address)) + 0x100*int(r.PeekByteAt(address+1)))
}
func (r *memoryReader) PeekWordBigEndianAt(address Address) Word {
	return Word(0x100*int(r.PeekByteAt(address)) + int(r.PeekByteAt(address+1)))
}
func (r *memoryReader) PeekAddressAt(address Address) Address {
	return r.PeekWordAt(address).AsAddress()
}
func (r *memoryReader) PeekAddressBigEndianAt(address Address) Address {
	return r.PeekWordBigEndianAt(address).AsAddress()
}

func (r *memoryReader) SkipBytes(delta int) {
	r.addr = r.addr.Offset(delta)
}

func (r *memoryReader) ChildReader(count int) (reader MemoryReaderCount) {
	reader = &memoryReaderChild{
		MemoryReaderCount: &memoryReaderCount{
			base:       r,
			count:      count,
			startAddr:  r.CurrentAddress(),
			startCount: count,
		},
		count:      count,
		startAddr:  r.CurrentAddress(),
		startCount: count,
	}
	return
}

func (r *memoryReader) Rewind(count int) {
	// Partial reset
	r.addr = r.addr.Offset(-count)
	if count < len(r.buffer) {
		r.buffer = r.buffer[:len(r.buffer)-count]
	} else {
		r.ResetByteBuffer()
	}
}

func (r *memoryReader) ResetAddress(addr Address) {
	// Full reset
	r.addr = addr
	r.ResetByteBuffer()
}
