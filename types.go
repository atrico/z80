package z80

import (
	"fmt"
	"unsafe"
)

// ----------------------------------------------------------------------------------------------------------------------------
// Byte
// ----------------------------------------------------------------------------------------------------------------------------

type Byte byte

func (b Byte) String() string {
	return fmt.Sprintf("%02x", int(b))
}

func (b Byte) Display(name string) string {
	return fmt.Sprintf("%s = %02X (%03d)", name, b, b)
}

func (b Byte) TwosComplement() int {
	return int(b&0x7f) - int(b&0x80)
}

func (b Byte) And(rhs Byte) Byte {
	return Byte(byte(b) & byte(rhs))
}

func (b Byte) Or(rhs Byte) Byte {
	return Byte(byte(b) | byte(rhs))
}

func (b Byte) RotateLeft(carry bool) (result Byte, newCarry bool) {
	val := byte(b)
	newCarry = val&0x80 > 0
	val <<= 1
	if carry {
		val |= 0x01
	}
	result = Byte(val)
	return
}

func (b Byte) RotateRight(carry bool) (result Byte, newCarry bool) {
	val := byte(b)
	newCarry = val&0x01 > 0
	val >>= 1
	if carry {
		val |= 0x80
	}
	result = Byte(val)
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Slices
// ----------------------------------------------------------------------------------------------------------------------------

func ToZ80(b []byte) []Byte {
	return *(*[]Byte)(unsafe.Pointer(&b))
}

func ToSystem(b []Byte) []byte {
	return *(*[]byte)(unsafe.Pointer(&b))
}

// ----------------------------------------------------------------------------------------------------------------------------
// Word
// ----------------------------------------------------------------------------------------------------------------------------

type Word uint16

func (w Word) Hi() Byte {
	return Byte(w >> 8)
}
func (w Word) Lo() Byte {
	return Byte(w & 0x00ff)
}

func (w Word) String() string {
	return fmt.Sprintf("%04x", int(w))
}

func (w Word) Display(name string) string {
	return fmt.Sprintf("%s = %s (%d)", name, w, int(w))
}

func (w Word) AsAddress() Address {
	return Address(w)
}

func (w Word) TwosComplement() int {
	return int(w&0x7fff) - int(w&0x8000)
}

func NewWord(lo, hi Byte) Word {
	return Word(int(lo) + 0x100*int(hi))
}

// ----------------------------------------------------------------------------------------------------------------------------
// Address
// ----------------------------------------------------------------------------------------------------------------------------

type Address Word

func (a Address) String() string {
	return Word(a).String()
}

func (a Address) Display(name string) string {
	return Word(a).Display(name)
}

func (a Address) Offset(offset int) Address {
	return Address((int(a) + offset) & 0xffff)
}
func (a Address) OffsetB(offset Byte) Address {
	return a.Offset(int(offset))
}

func (a Address) AsWord() Word {
	return Word(a)
}

var MaxAddress = Address(0xffff)
