package z80

type GeneralRegisters struct {
	HL, DE, BC, AF Word
}

type IndexRegisters struct {
	IX, IY Word
}

type SpecialRegisters struct {
	I, R Byte
}
