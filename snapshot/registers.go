package snapshot

import (
	"errors"
	"fmt"
	"gitlab.com/atrico/z80"
	"strings"
)

type Registers16Bit struct {
	HL  z80.WordRef
	DE  z80.WordRef
	BC  z80.WordRef
	AF  z80.WordRef
	HLp z80.WordRef
	DEp z80.WordRef
	BCp z80.WordRef
	AFp z80.WordRef
	IX  z80.WordRef
	IY  z80.WordRef
	SP  z80.WordRef
}

func (r Registers16Bit) Get(name string) (reg z80.WordRef, err error) {
	switch strings.ToUpper(name) {
	case "HL":
		reg = r.HL
	case "DE":
		reg = r.DE
	case "BC":
		reg = r.BC
	case "AF":
		reg = r.AF
	case "HL'":
		reg = r.HLp
	case "DE'":
		reg = r.DEp
	case "BC'":
		reg = r.BCp
	case "AF'":
		reg = r.AFp
	case "IX":
		reg = r.IX
	case "IY":
		reg = r.IY
	case "SP":
		reg = r.SP
	default:
		err = errors.New(fmt.Sprintf("Invalid 16bit register: %s", name))
	}
	return
}

type Registers8Bit struct {
	H   z80.ByteRef
	L   z80.ByteRef
	D   z80.ByteRef
	E   z80.ByteRef
	B   z80.ByteRef
	C   z80.ByteRef
	A   z80.ByteRef
	F   z80.ByteRef
	Hp  z80.ByteRef
	Lp  z80.ByteRef
	Dp  z80.ByteRef
	Ep  z80.ByteRef
	Bp  z80.ByteRef
	Cp  z80.ByteRef
	Ap  z80.ByteRef
	Fp  z80.ByteRef
	IXh z80.ByteRef
	IXl z80.ByteRef
	IYh z80.ByteRef
	IYl z80.ByteRef
	I   z80.ByteRef
	R   z80.ByteRef
}

func (r Registers8Bit) Get(name string) (reg z80.ByteRef, err error) {
	switch strings.ToUpper(name) {
	case "H":
		reg = r.H
	case "L":
		reg = r.L
	case "D":
		reg = r.D
	case "E":
		reg = r.E
	case "B":
		reg = r.B
	case "C":
		reg = r.C
	case "A":
		reg = r.A
	case "F":
		reg = r.F
	case "H'":
		reg = r.Hp
	case "L'":
		reg = r.Lp
	case "D'":
		reg = r.Dp
	case "E'":
		reg = r.Ep
	case "B'":
		reg = r.Bp
	case "C'":
		reg = r.Cp
	case "A'":
		reg = r.Ap
	case "F'":
		reg = r.Fp
	case "IXH":
		reg = r.IXh
	case "IXL":
		reg = r.IXl
	case "IYH":
		reg = r.IYh
	case "IYL":
		reg = r.IYl
	case "I":
		reg = r.I
	case "R":
		reg = r.R
	default:
		err = errors.New(fmt.Sprintf("Invalid 8bit register: %s", name))
	}
	return
}

type Flags struct {
	Value z80.ByteRef
	// Flags
	C  z80.BitRef // Carry (bit 0)
	N  z80.BitRef // Add/Subtract (bit 1)
	PV z80.BitRef // Parity/Overflow (bit 2)
	B3 z80.BitRef // bit 3 of the result (bit 3)
	H  z80.BitRef // Half carry (bit 4)
	B5 z80.BitRef // bit 5 of the result (bit 5)
	Z  z80.BitRef // Zero (bit 6)
	S  z80.BitRef // Sign (bit 7)
	// Conditions
	// Carry = C
	NC z80.BitRef // Not Carry = !C
	//N z80.BitRef
	PO z80.BitRef // Parity edd = PV
	PE z80.BitRef // Parity even = !PV
	//H z80.BitRef
	// Zero = Z
	NZ z80.BitRef // Not Zero = !Z
	M  z80.BitRef // Sign negative = S
	P  z80.BitRef // Sign positive = !S
}

func (r Flags) Get(name string) (res z80.BitRef, err error) {
	switch strings.ToUpper(name) {
	case "C": // Flags
		res = r.C
	case "N":
		res = r.N
	case "PV", "P/V":
		res = r.PV
	case "B3":
		res = r.B3
	case "H":
		res = r.H
	case "B5":
		res = r.B5
	case "Z":
		res = r.Z
	case "S":
		res = r.S
	case "NC": // Conditions
		res = r.NC
	case "PO":
		res = r.PO
	case "PE":
		res = r.PE
	case "NZ":
		res = r.NZ
	case "M":
		res = r.M
	case "P":
		res = r.P

	default:
		err = errors.New(fmt.Sprintf("Invalid flag/condition: %s", name))
	}
	return
}
