package snapshot

type SnapshotRef[T any] interface {
	Get(snap Snapshot) T
	Set(val T, snap Snapshot)
}
