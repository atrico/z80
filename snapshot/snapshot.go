package snapshot

import (
	"gitlab.com/atrico/z80"
	"gitlab.com/atrico/z80/interrupt"
)

type Snapshot interface {
	z80.MemoryRead
	z80.MemoryWrite
	// Info
	Registers16Bit() Registers16Bit
	Registers8Bit() Registers8Bit
	Flags() Flags
	AltFlags() Flags
	StackPointer() z80.AddressRef
	ProgramCounter() z80.AddressRef
	InterruptMode() z80.InterruptModeRef
	InterruptsEnabled() z80.BitRef
	// Clone this snapshot
	Clone() Snapshot
}

func NewSnapshot(memory z80.Memory, PC z80.Address, SP z80.Address, registers z80.GeneralRegisters, altRegisters z80.GeneralRegisters, indexRegisters z80.IndexRegisters, specialRegisters z80.SpecialRegisters, interruptMode interrupt.Mode, interruptsEnabled bool) Snapshot {
	iff2 := 0
	if interruptsEnabled {
		iff2 = 0x4
	}
	return &snapshot{
		registers:        registers,
		altRegisters:     altRegisters,
		indexRegisters:   indexRegisters,
		specialRegisters: specialRegisters,
		interruptMode:    interruptMode,
		IFF2:             z80.Byte(iff2),
		pc:               PC,
		sp:               SP,
		Memory:           memory,
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type snapshot struct {
	registers        z80.GeneralRegisters
	altRegisters     z80.GeneralRegisters
	indexRegisters   z80.IndexRegisters
	specialRegisters z80.SpecialRegisters
	interruptMode    interrupt.Mode
	IFF2             z80.Byte // bit 2 is interrupts enabled
	pc               z80.Address
	sp               z80.Address
	z80.Memory
}

func (s *snapshot) GetBytes(start, end z80.Address) []z80.Byte {
	return s.GetBytes(start, end)
}

func (s *snapshot) Registers16Bit() Registers16Bit {
	return Registers16Bit{
		HL:  z80.NewWordRef(&s.registers.HL),
		DE:  z80.NewWordRef(&s.registers.DE),
		BC:  z80.NewWordRef(&s.registers.BC),
		AF:  z80.NewWordRef(&s.registers.AF),
		HLp: z80.NewWordRef(&s.altRegisters.HL),
		DEp: z80.NewWordRef(&s.altRegisters.DE),
		BCp: z80.NewWordRef(&s.altRegisters.BC),
		AFp: z80.NewWordRef(&s.altRegisters.AF),
		IX:  z80.NewWordRef(&s.indexRegisters.IX),
		IY:  z80.NewWordRef(&s.indexRegisters.IY),
		SP:  s.StackPointer().AsWord(),
	}
}

func (s *snapshot) Registers8Bit() Registers8Bit {
	return Registers8Bit{
		H:   z80.NewByteRef16(&s.registers.HL, z80.HiByte),
		L:   z80.NewByteRef16(&s.registers.HL, z80.LoByte),
		D:   z80.NewByteRef16(&s.registers.DE, z80.HiByte),
		E:   z80.NewByteRef16(&s.registers.DE, z80.LoByte),
		B:   z80.NewByteRef16(&s.registers.BC, z80.HiByte),
		C:   z80.NewByteRef16(&s.registers.BC, z80.LoByte),
		A:   z80.NewByteRef16(&s.registers.AF, z80.HiByte),
		F:   z80.NewByteRef16(&s.registers.AF, z80.LoByte),
		Hp:  z80.NewByteRef16(&s.altRegisters.HL, z80.HiByte),
		Lp:  z80.NewByteRef16(&s.altRegisters.HL, z80.LoByte),
		Dp:  z80.NewByteRef16(&s.altRegisters.DE, z80.HiByte),
		Ep:  z80.NewByteRef16(&s.altRegisters.DE, z80.LoByte),
		Bp:  z80.NewByteRef16(&s.altRegisters.BC, z80.HiByte),
		Cp:  z80.NewByteRef16(&s.altRegisters.BC, z80.LoByte),
		Ap:  z80.NewByteRef16(&s.altRegisters.AF, z80.HiByte),
		Fp:  z80.NewByteRef16(&s.altRegisters.AF, z80.LoByte),
		IXh: z80.NewByteRef16(&s.indexRegisters.IX, z80.HiByte),
		IXl: z80.NewByteRef16(&s.indexRegisters.IX, z80.LoByte),
		IYh: z80.NewByteRef16(&s.indexRegisters.IY, z80.HiByte),
		IYl: z80.NewByteRef16(&s.indexRegisters.IY, z80.LoByte),
		I:   z80.NewByteRef(&s.specialRegisters.I),
		R:   z80.NewByteRef(&s.specialRegisters.R),
	}
}

func (s *snapshot) Flags() Flags {
	return createFlags(s.Registers8Bit().F)
}
func (s *snapshot) AltFlags() Flags {
	return createFlags(s.Registers8Bit().Fp)
}

func createFlags(f z80.ByteRef) (flags Flags) {
	flags = Flags{
		Value: f,
		C:     z80.NewBitRefFromRef(f, 0),
		N:     z80.NewBitRefFromRef(f, 1),
		PV:    z80.NewBitRefFromRef(f, 2),
		B3:    z80.NewBitRefFromRef(f, 3),
		H:     z80.NewBitRefFromRef(f, 4),
		B5:    z80.NewBitRefFromRef(f, 5),
		Z:     z80.NewBitRefFromRef(f, 6),
		S:     z80.NewBitRefFromRef(f, 7),
	}
	flags.NC = z80.NegateBitRef(flags.C)
	flags.PO = flags.PV
	flags.PE = z80.NegateBitRef(flags.PV)
	flags.NZ = z80.NegateBitRef(flags.Z)
	flags.M = flags.S
	flags.P = z80.NegateBitRef(flags.S)

	return
}

func (s *snapshot) StackPointer() z80.AddressRef {
	return z80.NewAddressRef(&s.sp)
}

func (s *snapshot) ProgramCounter() z80.AddressRef {
	return z80.NewAddressRef(&s.pc)
}

func (s *snapshot) InterruptMode() z80.InterruptModeRef {
	return z80.NewInterruptModeRef(&s.interruptMode)
}

func (s *snapshot) InterruptsEnabled() z80.BitRef {
	return z80.NewBitRef(&s.IFF2, 2)
}

func (s *snapshot) Clone() Snapshot {
	return &snapshot{
		s.registers,
		s.altRegisters,
		s.indexRegisters,
		s.specialRegisters,
		s.interruptMode,
		s.IFF2,
		s.pc,
		s.sp,
		s.Memory.Clone(),
	}
}
