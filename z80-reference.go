package z80

import (
	"fmt"
	"gitlab.com/atrico/z80/interrupt"
)

type Ref[T any] interface {
	fmt.Stringer
	Get() T
	Set(val T)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Bit
// ----------------------------------------------------------------------------------------------------------------------------

type BitRef interface {
	Ref[bool]
}

func NewBitRef(by *Byte, offset int) BitRef {
	return bitRef{&byteRef{by}, offset}
}
func NewBitRefFromRef(by ByteRef, offset int) BitRef {
	return bitRef{by, offset}
}
func NegateBitRef(r BitRef) BitRef {
	return bitRefNegate{r}
}

type bitRef struct {
	by     ByteRef
	offset int // 0-7
}

func (r bitRef) Get() bool {
	mask := 0x01 << r.offset
	return (int(r.by.Get()) & mask) != 0
}

func (r bitRef) Set(val bool) {
	mask := 0x01 << r.offset
	res := int(r.by.Get())
	if val {
		// Set bit (OR with single bit set)
		res |= mask
	} else {
		// Reset bit (AND with single bit reset)
		res &= mask ^ 0xff
	}
	r.by.Set(Byte(res & 0xff))
}

func (r bitRef) String() string {
	if r.Get() {
		return "1"
	}
	return "0"
}

type bitRefNegate struct {
	BitRef
}

func (r bitRefNegate) Get() bool {
	return !r.BitRef.Get()
}

func (r bitRefNegate) Set(val bool) {
	r.BitRef.Set(!val)
}

func (r bitRefNegate) String() string {
	if r.BitRef.Get() {
		return "0"
	}
	return "1"
}

// ----------------------------------------------------------------------------------------------------------------------------
// Interrupt mode
// ----------------------------------------------------------------------------------------------------------------------------

type InterruptModeRef interface {
	Ref[interrupt.Mode]
}

func NewInterruptModeRef(mode *interrupt.Mode) InterruptModeRef {
	return interruptModeRef{mode}
}

type interruptModeRef struct {
	mode *interrupt.Mode
}

func (r interruptModeRef) Get() interrupt.Mode {
	return *r.mode
}

func (r interruptModeRef) Set(val interrupt.Mode) {
	*r.mode = val
}

func (r interruptModeRef) String() string {
	return r.Get().String()
}

// ----------------------------------------------------------------------------------------------------------------------------
// Buffer (block)
// ----------------------------------------------------------------------------------------------------------------------------

type ByteBlockRef interface {
	Ref[[]Byte]
}

func NewByteBlockRefMem(mem Memory, addr Address, len int) ByteBlockRef {
	return byteBlockRefMem{
		mem:  mem,
		addr: addr,
		len:  len,
	}
}

type byteBlockRefMem struct {
	mem  Memory
	addr Address
	len  int
}

func (r byteBlockRefMem) Get() []Byte {
	return r.mem.ReadBytes(r.addr, r.addr.Offset(r.len))
}

func (r byteBlockRefMem) Set(val []Byte) {
	for i, by := range val {
		if i == r.len {
			return
		}
		r.mem.WriteByte(r.addr.Offset(i), by)
	}
}

func (r byteBlockRefMem) String() string {
	return fmt.Sprintf("%v", r.Get())
}
