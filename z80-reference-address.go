package z80

type AddressRef interface {
	Ref[Address]
	AsWord() WordRef
}

func NewAddressRef(ad *Address) AddressRef {
	return addressRef{NewWordRef((*Word)(ad))}
}
func NewAddressRefMem(mem Memory, addr Address) AddressRef {
	return addressRef{NewWordRefMem(mem, addr)}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Address
// ----------------------------------------------------------------------------------------------------------------------------

type addressRef struct {
	WordRef
}

func (r addressRef) AsWord() WordRef {
	return r.WordRef
}

func (r addressRef) Get() Address {
	return Address(r.WordRef.Get())
}

func (r addressRef) Set(val Address) {
	r.WordRef.Set(Word(val))
}

func (r addressRef) String() string {
	return r.Get().String()
}
